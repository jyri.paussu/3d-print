/**
 * @author Sergey Burkov, http://www.wp3dprinting.com
 * @copyright 2015
 */
jQuery(document).ready(function() {
	jQuery('.p3d-expand').accordion({collapsible:true, active:false});
	jQuery( "#3dp_tabs" ).tabs();
	jQuery( ".3dp_color_picker" ).wpColorPicker();
	jQuery('select.select_material').change()
	jQuery('select.select_printer').change()
	jQuery('.sumoselect').SumoSelect({ okCancelInMulti: true, selectAll: true });
	jQuery('.tooltip').tooltipster({ contentAsHTML: true, multiple: true });
	/*p3dCalculateKwh();
	p3dCalculateDepreciation();
	p3dCalculateRepair();*/

	

});

function p3dAddPrinter(clone_id, max_index) {

	jQuery('input[name^=3dp_printer_name]').each(function(){
		var start = jQuery(this).attr('name').indexOf('[')+1;
		var end = jQuery(this).attr('name').indexOf(']');
		var index = parseInt(jQuery(this).attr('name').substring(start, end));
		if (index > max_index) max_index = index;
	});

	if (clone_id) object_to_clone=jQuery('#printer-'+clone_id);
	else object_to_clone=jQuery('table.printer').first();

	jQuery(object_to_clone).clone().insertBefore('#add_printer_button');
	if (!clone_id) jQuery('table.printer').last().find('input').val('');

	jQuery('table.printer').last().find('.remove_printer').remove();
	jQuery('table.printer').last().find('.item_id').remove();
	jQuery('table.printer').last().find('input[name^=3dp_printer], select[name^=3dp_printer], textarea[name^=3dp_printer]').each(function(){
		var start = jQuery(this).attr('name').indexOf('[')+1;
		var end = jQuery(this).attr('name').indexOf(']');
		var index = parseInt(jQuery(this).attr('name').substring(start, end));
		var new_name = jQuery(this).attr('name').replace('['+index+']', '['+(max_index+1)+']');
		jQuery(this).attr('name', new_name);

		if (!clone_id) jQuery(this).val(jQuery(this).find("option:first").val());
//console.log(jQuery(this).val())
	});
	jQuery('table.printer').last().find('tr, option').show();
//	jQuery('table.printer').last().find('tr.printer_materials').remove();
	jQuery('table.printer').last().find('.optWrapper, .CaptionCont').remove();
	jQuery('table.printer').last().find('.sumoselect').unwrap().show();
	if (!clone_id) jQuery('table.printer').last().find('.sumoselect option:selected').prop('selected', false);
	jQuery('table.printer').last().find('.sumoselect').SumoSelect({ okCancelInMulti: true, selectAll: true });
	jQuery('table.printer').last().find('.tooltipstered').removeClass('tooltipstered');
	jQuery('table.printer').last().find('.tooltip').tooltipster({ contentAsHTML: true, multiple: true });
	jQuery('table.printer').last().find('.p3d-advanced').hide()



}

function p3dAddMaterial(clone_id, max_index) {

	jQuery('input[name^=3dp_material_name]').each(function(){
		var start = jQuery(this).attr('name').indexOf('[')+1;
		var end = jQuery(this).attr('name').indexOf(']');
		var index = parseInt(jQuery(this).attr('name').substring(start, end));
		if (index > max_index) max_index = index;
	});

	if (clone_id) object_to_clone=jQuery('#material-'+clone_id);
	else object_to_clone=jQuery('table.material').first();

	jQuery(object_to_clone).clone().insertBefore('#add_material_button');
	if (!clone_id) jQuery('table.material').last().find('input').val('');


	jQuery('table.material').last().find('.wp-picker-container').remove();
	jQuery('table.material').last().find('td.color_td').html('<input type="text" class="3dp_color_picker" name="3dp_material_color['+(max_index+1)+']" value="" />');
	jQuery('table.material').last().find( ".3dp_color_picker" ).wpColorPicker();
	jQuery('table.material').last().find('.remove_material').remove();
	jQuery('table.material').last().find('.item_id').remove();

	jQuery('table.material').last().find('input[name^=3dp_material], select[name^=3dp_material], textarea[name^=3dp_material]').each(function(){
		var start = jQuery(this).attr('name').indexOf('[')+1;
		var end = jQuery(this).attr('name').indexOf(']');
		var index = parseInt(jQuery(this).attr('name').substring(start, end));
		var new_name = jQuery(this).attr('name').replace('['+index+']', '['+(max_index+1)+']');
		jQuery(this).attr('name', new_name);
	});
	jQuery('table.material').last().find('.tooltipstered').removeClass('tooltipstered');
	jQuery('table.material').last().find('.tooltip').tooltipster({ contentAsHTML: true, multiple: true });
}

function p3dAddCoating(clone_id, max_index) {

	jQuery('input[name^=3dp_coating_name]').each(function(){
		var start = jQuery(this).attr('name').indexOf('[')+1;
		var end = jQuery(this).attr('name').indexOf(']');
		var index = parseInt(jQuery(this).attr('name').substring(start, end));
		if (index > max_index) max_index = index;
	});

	if (clone_id) object_to_clone=jQuery('#coating-'+clone_id);
	else object_to_clone=jQuery('table.coating').first();

	jQuery(object_to_clone).clone().insertBefore('#add_coating_button');
	if (!clone_id) jQuery('table.coating').last().find('input').val('');

	jQuery('table.coating').last().find('.wp-picker-container').remove();
	jQuery('table.coating').last().find('td.color_td').html('<input type="text" class="3dp_color_picker" name="3dp_coating_color['+(max_index+1)+']" value="" />');
	jQuery('table.coating').last().find( ".3dp_color_picker" ).wpColorPicker();
	jQuery('table.coating').last().find('.remove_coating').remove();
	jQuery('table.coating').last().find('.item_id').remove();

	jQuery('table.coating').last().find('input[name^=3dp_coating], select[name^=3dp_coating], textarea[name^=3dp_coating]').each(function(){
		var start = jQuery(this).attr('name').indexOf('[')+1;
		var end = jQuery(this).attr('name').indexOf(']');
		var index = parseInt(jQuery(this).attr('name').substring(start, end));
		var new_name = jQuery(this).attr('name').replace('['+index+']', '['+(max_index+1)+']');
		jQuery(this).attr('name', new_name);
	});

	jQuery('table.coating').last().find('.optWrapper, .CaptionCont').remove();
	jQuery('table.coating').last().find('.sumoselect').unwrap().show();
	if (!clone_id) jQuery('table.coating').last().find('.sumoselect option:selected').prop('selected', false);
	jQuery('table.coating').last().find('.sumoselect').SumoSelect({ okCancelInMulti: true, selectAll: true });
	jQuery('table.coating').last().find('.tooltipstered').removeClass('tooltipstered');
	jQuery('table.coating').last().find('.tooltip').tooltipster({ contentAsHTML: true, multiple: true });
}

function p3dRemovePrinter(id) {
	jQuery( '<form action="admin.php?page=3dprint#3dp_tabs-1" method="post"><input type="hidden" name="action" value="remove_printer"><input type="hidden" name="printer_id" value="'+id+'"></form>' ).appendTo('body').submit()
}
function p3dRemoveMaterial(id) {
	jQuery( '<form action="admin.php?page=3dprint#3dp_tabs-2" method="post"><input type="hidden" name="action" value="remove_material"><input type="hidden" name="material_id" value="'+id+'"></form>' ).appendTo('body').submit()
}

function p3dRemoveCoating(id) {
	jQuery( '<form action="admin.php?page=3dprint#3dp_tabs-3" method="post"><input type="hidden" name="action" value="remove_coating"><input type="hidden" name="coating_id" value="'+id+'"></form>' ).appendTo('body').submit()
}

function p3dRemoveRequest(id) {
	jQuery( '<form action="admin.php?page=3dprint#3dp_tabs-4" method="post"><input type="hidden" name="action" value="remove_request"><input type="hidden" name="request_id" value="'+id+'"></form>' ).appendTo('body').submit()
}

function p3dSetMaterialType(obj)  {
        var material_type = obj.value;
	jQuery(obj).closest('table.form-table.material').find('tr, a').each(function(i, el){
		var className = jQuery(el).attr('class');
		if (typeof(className)!=='undefined') {
			if (className.indexOf('material')==0) {

				if (className=='material_'+material_type) jQuery(el).show();
				else jQuery(el).hide();
			}
		}
	});
}

function p3dSetPrinterType(printer_type, obj)  {

	jQuery(obj).closest('table.form-table.printer').find('tr, option').each(function(i, el){

		var className = jQuery(el).attr('class');
		if (typeof(className)!=='undefined') {
			if (className.indexOf('printer')==0) {
				if (className.indexOf('p3d-hidden')!=-1) jQuery(el).hide();
				else if (className.indexOf('printer_'+printer_type)!=-1) jQuery(el).toggle();
				else jQuery(el).hide();
			}
		}
	});
}



function p3dSelectPlatformShape(obj) {
	var platform_shape = obj.value;
	jQuery(obj).closest('table.form-table.printer').find('tr').each(function(i, el){

		var className = jQuery(el).attr('class');
		if (typeof(className)!=='undefined') {
			if (className.indexOf('platform_shape')==0) {
				if (className.indexOf('platform_shape_'+platform_shape)!=-1) jQuery(el).show();
				else jQuery(el).hide();
			}
		}
	});
}

function p3dCheckPricing(pricing) {
	if (pricing=='checkout') {
		jQuery('#extra_pricing_button').show();
	}
	else {
		jQuery('#extra_pricing_button').hide();
	}
}

function p3dCalculateKwh() {
	jQuery('input.printer_kwh').each(function(i, el){
		var printer_id = jQuery(el).closest('table.printer').data('id');
		var power_tariff = jQuery('input[name=3dp_printer_power_tariff\\['+printer_id+'\\]]').val();
		var printer_power = jQuery('input[name=3dp_printer_power\\['+printer_id+'\\]]').val();
		var hourly_price = (printer_power/1000)*power_tariff;
		if (!isNaN(hourly_price)) jQuery(el).val(hourly_price.toFixed(2));
	});
}

function p3dCalculateDepreciation() {
	jQuery('input.printer_depreciation').each(function(i, el){
		var printer_id = jQuery(el).closest('table.printer').data('id');
		var printer_purchase_price = jQuery('input[name=3dp_printer_purchase_price\\['+printer_id+'\\]]').val();
		var printer_lifetime = jQuery('input[name=3dp_printer_lifetime\\['+printer_id+'\\]]').val();
		var printer_daily_usage = jQuery('input[name=3dp_printer_daily_usage\\['+printer_id+'\\]]').val();
//		if (printer_id==4) console.log(printer_purchase_price, printer_lifetime, printer_daily_usage)
		var printer_hours_in_life = 365*printer_lifetime*printer_daily_usage;
		var hourly_price = printer_purchase_price / printer_hours_in_life;
//		if (printer_id==4) console.log(printer_hours_in_life, hourly_price);
		jQuery(el).closest('table.printer').find('span.printer_hours_in_life').val(printer_hours_in_life);
		if (!isNaN(hourly_price)) jQuery(el).val(hourly_price.toFixed(2));
	});
}

function p3dCalculateRepair() {
	jQuery('input.printer_repair').each(function(i, el){
		var printer_id = jQuery(el).closest('table.printer').data('id');
		var printer_repair_cost = jQuery('input[name=3dp_printer_repair_cost\\['+printer_id+'\\]]').val();
		var printer_purchase_price = jQuery('input[name=3dp_printer_purchase_price\\['+printer_id+'\\]]').val();
		var printer_lifetime = jQuery('input[name=3dp_printer_lifetime\\['+printer_id+'\\]]').val();
		var printer_daily_usage = jQuery('input[name=3dp_printer_daily_usage\\['+printer_id+'\\]]').val();
		var printer_hours_in_life = 365*printer_lifetime*printer_daily_usage;
		var hourly_price = (printer_purchase_price/100*printer_repair_cost)/printer_hours_in_life;
		if (!isNaN(hourly_price)) jQuery(el).val(hourly_price.toFixed(2));
	});
}