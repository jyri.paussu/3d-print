<?php


/* This plugin has been modified to meet the needs of SAMK 3D-Printing Webshop */
/* This code also includes 3d-party plugin Gcode-viewer */
/* Made by Jyri Paussu */
/* 21.09.2018 */

/* SAMK 3D-Webshop includes */

function get_3dprint_url()
{
    return "/wp-content/plugins/3dprint";
}

function wbs_adding_scripts() {

    wp_register_script( 'jquery-1.8.2', get_3dprint_url() . '/assets/lib/jquery-1.8.2.js' , array(), '', false );
    wp_register_script( 'jquery-ui-1.9.0.custom', get_3dprint_url() . '/assets/lib/jquery-ui-1.9.0.custom.js' , array(), '', false );
    //wp_register_script( 'codemirror', get_3dprint_url() . '/assets/lib/codemirror.js' , array(), '', false );
    //wp_register_script( 'mode_gcode/gcode_mode', get_3dprint_url() . '/assets/lib/mode_gcode/gcode_mode.js' , array(), '', false );
    wp_register_script( 'three', get_3dprint_url() . '/assets/lib/three.js' , array(), '', false );
    wp_register_script( 'bootstrap', get_3dprint_url() . '/assets/lib/bootstrap.js' , array(), '', false );
    wp_register_script( 'modernizr.custom.09684', get_3dprint_url() . '/assets/lib/modernizr.custom.09684.js' , array(), '', false );
    wp_register_script( 'jquery-ui-1.9.0.custom', get_3dprint_url() . '/assets/lib/jquery-ui-1.9.0.custom.js' , array(), '', false );
    wp_register_script( 'TrackballControls', get_3dprint_url() . '/assets/lib/TrackballControls.js' , array(), '', false );
    wp_register_script( 'zlib.min', get_3dprint_url() . '/assets/lib/zlib.min.js' , array(), '', false );


    wp_register_script( 'ui', get_3dprint_url() . '/assets/js/ui.js' , array(), '', false );
    wp_register_script( 'gCodeReader', get_3dprint_url() . '/assets/js/gCodeReader.js' , array(), '', false );
    wp_register_script( 'renderer', get_3dprint_url() . '/assets/js/renderer.js' , array(), '', false );
    wp_register_script( 'analyzer', get_3dprint_url() . '/assets/js/analyzer.js', array(), '', false );
    wp_register_script( 'renderer3d', get_3dprint_url() . '/assets/js/renderer3d.js' , array(), '', false );
    //wp_register_script( 'Worker', get_3dprint_url() . '/assets/js/Worker.js' , array(), '', false );

    wp_enqueue_script( 'jquery-1.8.2');
    wp_enqueue_script('jquery-ui-1.9.0.custom');
    //wp_enqueue_script( 'codemirror');
    //wp_enqueue_script( 'mode_gcode/gcode_mode');
    wp_enqueue_script( 'three');
    wp_enqueue_script( 'bootstrap');
    wp_enqueue_script( 'modernizr.custom.09684');
    wp_enqueue_script( 'TrackballControls');
    wp_enqueue_script( 'zlib.min');

    wp_enqueue_script( 'ui');
    wp_enqueue_script( 'gCodeReader');
    wp_enqueue_script( 'renderer');
    wp_enqueue_script( 'analyzer');
    wp_enqueue_script( 'renderer3d');
    //wp_enqueue_script( 'Worker');

}
function wbs_adding_styles()
{
    wp_enqueue_style('style-gcode', get_3dprint_url() . '/assets/css/style-gcode.css', '', 1);
    wp_enqueue_style('bootstrap', get_3dprint_url() . '/assets/css/bootstrap.css', '', 1);
    wp_enqueue_style('bootstrap.min', get_3dprint_url() . '/assets/css/bootstrap.min.css', '', 1);
    wp_enqueue_style('bootstrap-responsive', get_3dprint_url() . '/assets/css/bootstrap-responsive.css', '', 1);
    wp_enqueue_style('bootstrap-responsive.min', get_3dprint_url() . '/assets/css/bootstrap-responsive.min.css', '', 1);
}
add_action( 'wp_enqueue_style', 'wbs_adding_styles' );
add_action( 'wp_enqueue_scripts', 'wbs_adding_scripts' );

/* End of includes */


/* Global variables */

$uploadedfile;

/**
 *
 *
 * @author Sergey Burkov, http://www.wp3dprinting.com
 * @copyright 2015
 */

function p3d_activate() {
	global $wpdb;

	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if( !is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
		wp_die ('Woocommerce is not installed!');
	}

	$current_version = get_option( '3dp_version');

	$check_query = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}woocommerce_attribute_taxonomies" );

	$attr = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_printer' ) );
	if ( strlen( $attr->attribute_id )==0 ) {

		$attribute=array( 'attribute_name'=>'p3d_printer',
			'attribute_label'=>__( 'Printer', '3dprint' ),
			'attribute_type'=>'text',
			'attribute_orderby'=>'menu_order',
			'attribute_public'=>'0' );
		if ( !isset( $check_query->attribute_public ) ) unset( $attribute['attribute_public'] );
		$wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $attribute );

		do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $attribute );
		delete_transient( 'wc_attribute_taxonomies' );


	}
	$attr = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_material' ) );
	if ( strlen( $attr->attribute_id )==0 ) {
		$attribute=array( 'attribute_name'=>'p3d_material',
			'attribute_label'=>__( 'Material', '3dprint' ),
			'attribute_type'=>'text',
			'attribute_orderby'=>'menu_order',
			'attribute_public'=>'0' );
		if ( !isset( $check_query->attribute_public ) ) unset( $attribute['attribute_public'] );
		$wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $attribute );

		do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $attribute );
		delete_transient( 'wc_attribute_taxonomies' );

	}

	$attr = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_coating' ) );
	if ( strlen( $attr->attribute_id )==0 ) {
		$attribute=array( 'attribute_name'=>'p3d_coating',
			'attribute_label'=>__( 'Coating', '3dprint' ),
			'attribute_type'=>'text',
			'attribute_orderby'=>'menu_order',
			'attribute_public'=>'0' );
		if ( !isset( $check_query->attribute_public ) ) unset( $attribute['attribute_public'] );
		$wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $attribute );

		do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $attribute );
		delete_transient( 'wc_attribute_taxonomies' );

	}

	$attr = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_model' ) );
	if ( strlen( $attr->attribute_id )==0 ) {
		$attribute=array( 'attribute_name'=>'p3d_model',
			'attribute_label'=>__( 'Model', '3dprint' ),
			'attribute_type'=>'text',
			'attribute_orderby'=>'menu_order',
			'attribute_public'=>'0' );
		if ( !isset( $check_query->attribute_public ) ) unset( $attribute['attribute_public'] );
		$wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $attribute );

		do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $attribute );
		delete_transient( 'wc_attribute_taxonomies' );

	}

	$attr = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_unit' ) );
	if ( strlen( $attr->attribute_id )==0 ) {
		$attribute=array( 'attribute_name'=>'p3d_unit',
			'attribute_label'=>__( 'Unit', '3dprint' ),
			'attribute_type'=>'text',
			'attribute_orderby'=>'menu_order',
			'attribute_public'=>'0' );
		if ( !isset( $check_query->attribute_public ) ) unset( $attribute['attribute_public'] );
		$wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $attribute );

		do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $attribute );
		delete_transient( 'wc_attribute_taxonomies' );

	}

	$attr = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_scale' ) );
	if ( strlen( $attr->attribute_id )==0 ) {
		$attribute=array( 'attribute_name'=>'p3d_scale',
			'attribute_label'=>__( 'Scale', '3dprint' ),
			'attribute_type'=>'text',
			'attribute_orderby'=>'menu_order',
			'attribute_public'=>'0' );
		if ( !isset( $check_query->attribute_public ) ) unset( $attribute['attribute_public'] );
		$wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $attribute );

		do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $attribute );
		delete_transient( 'wc_attribute_taxonomies' );
	}


	$attr = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_infill' ) );
	if ( strlen( $attr->attribute_id )==0 ) {
		$attribute=array( 'attribute_name'=>'p3d_infill',
			'attribute_label'=>__( 'Infill', '3dprint' ),
			'attribute_type'=>'text',
			'attribute_orderby'=>'menu_order',
			'attribute_public'=>'0' );
		if ( !isset( $check_query->attribute_public ) ) unset( $attribute['attribute_public'] );
		$wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $attribute );

		do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $attribute );
		delete_transient( 'wc_attribute_taxonomies' );

	}



	p3d_check_install();

	p3d_update_option('3dp_cache',array());
	p3d_update_option('3dp_triangulation_cache',array());
	$upload_dir = wp_upload_dir();
	if ( !is_dir( $upload_dir['basedir'].'/p3d/' ) ) {
		mkdir( $upload_dir['basedir'].'/p3d/' );
	}

	if ( !file_exists( $upload_dir['basedir'].'/p3d/index.html' ) ) {
		$fp = fopen( $upload_dir['basedir'].'/p3d/index.html', "w" );
		fclose( $fp );
	}

	$htaccess_contents='
<FilesMatch "\.(php([0-9]|s)?|s?p?html|cgi|py|pl|exe)$">
	Order Deny,Allow
	Deny from all
</FilesMatch>
AddType application/octet-stream obj
AddType application/octet-stream stl
<ifmodule mod_deflate.c>
	AddOutputFilterByType DEFLATE application/octet-stream
</ifmodule>
<ifmodule mod_expires.c>
	ExpiresActive on
	ExpiresDefault "access plus 365 days"
</ifmodule>
<ifmodule mod_headers.c>
	Header set Cache-Control "max-age=31536050"
</ifmodule>
	';
	
	if ( !file_exists( $upload_dir['basedir'].'/p3d/.htaccess' ) || version_compare($current_version, '1.5.0', '<')) {
		file_put_contents( $upload_dir['basedir'].'/p3d/.htaccess', $htaccess_contents );
	}

	add_option( 'p3d_do_activation_redirect', true );

	update_option( '3dp_version', P3D_VERSION );

	do_action( '3dprint_activate' );
}


function p3d_check_install() {
	global $wpdb;

	$current_version = get_option( '3dp_version');
	$charset_collate = $wpdb->get_charset_collate();

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	if (!empty($current_version) && version_compare($current_version, '2.0.7.5', '<=')) {

		$attr = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_scale' ) );
		if ( strlen( $attr->attribute_id )==0 ) {
			$attribute=array( 'attribute_name'=>'p3d_scale',
				'attribute_label'=>__( 'Scale', '3dprint' ),
				'attribute_type'=>'text',
				'attribute_orderby'=>'menu_order',
				'attribute_public'=>'0' );
			if ( !isset( $check_query->attribute_public ) ) unset( $attribute['attribute_public'] );
			$wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $attribute );
	
			do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $attribute );
			delete_transient( 'wc_attribute_taxonomies' );
		}


		$postlist = get_posts(array(
			'posts_per_page'=> -1,
			'post_type'  => 'product'));


		foreach ( $postlist as $post ) {

			if ( p3d_is_p3d( $post->ID ) ) {

				p3d_delete_p3d( $post->ID );
				$_POST['post_ID']=$post->ID;
				$_POST['_3dprinting']='on';
				p3d_save_post($post->ID);

			}
		}
	}


	$default_image_url = str_replace('http:','',plugins_url()).'/3dprint/images/';

	$sql = "CREATE TABLE ".$wpdb->prefix."p3d_printers (
		  id mediumint(9) NOT NULL AUTO_INCREMENT,
		  status tinyint(1) DEFAULT '1' NOT NULL,
		  name varchar(64) DEFAULT '' NOT NULL,
		  description text DEFAULT '' NOT NULL,
		  photo varchar(2048) DEFAULT '' NOT NULL,
		  type varchar(64) DEFAULT 'fff' NOT NULL,
		  full_color tinyint(1) DEFAULT '1' NOT NULL,
		  platform_shape varchar(64) DEFAULT 'rectangle' NOT NULL,
		  width smallint(6) DEFAULT 0 NOT NULL,
		  length smallint(6) DEFAULT 0 NOT NULL,
		  height smallint(6) DEFAULT 0 NOT NULL,
		  diameter smallint(6) DEFAULT 0 NOT NULL,
		  min_side float DEFAULT 1 NOT NULL,
		  price varchar(128) DEFAULT '0' NOT NULL,
		  price_type varchar(32) DEFAULT 'box_volume' NOT NULL,
		  price1 varchar(128) DEFAULT '0' NOT NULL,
		  price_type1 varchar(32) DEFAULT 'box_volume' NOT NULL,
		  price2 varchar(128) DEFAULT '0' NOT NULL,
		  price_type2 varchar(32) DEFAULT 'box_volume' NOT NULL,
		  price3 varchar(128) DEFAULT '0' NOT NULL,
		  price_type3 varchar(32) DEFAULT 'box_volume' NOT NULL,
		  price4 varchar(128) DEFAULT '0' NOT NULL,
		  price_type4 varchar(32) DEFAULT 'box_volume' NOT NULL,
		  power_tariff float DEFAULT '0' NOT NULL,
		  printer_power float DEFAULT '0' NOT NULL,
		  printer_purchase_price float DEFAULT '0' NOT NULL,
		  printer_lifetime float DEFAULT '5' NOT NULL,
		  printer_daily_usage float DEFAULT '0' NOT NULL,
		  printer_repair_cost float DEFAULT '0' NOT NULL,
		  printer_failure_rate float DEFAULT '0' NOT NULL,
		  printer_energy_hourly_cost float DEFAULT '0' NOT NULL,
		  printer_depreciation_hourly_cost float DEFAULT '0' NOT NULL,
		  printer_repair_hourly_cost float DEFAULT '0' NOT NULL,
		  speed float DEFAULT '150' NOT NULL,
		  speed_type varchar(8) DEFAULT 'mms' NOT NULL,
		  travel_speed float DEFAULT '150' NOT NULL,
		  infill_speed float DEFAULT '0' NOT NULL,
		  bottom_layer_speed float DEFAULT '20' NOT NULL,
		  solidarea_speed float DEFAULT '0' NOT NULL,
		  outer_shell_speed float DEFAULT '0' NOT NULL,
		  inner_shell_speed float DEFAULT '0' NOT NULL,
		  cool_min_layer_time smallint DEFAULT '5' NOT NULL,
		  support tinyint(1) DEFAULT '2' NOT NULL,
		  support_type tinyint(1) DEFAULT '0' NOT NULL,
		  support_angle smallint DEFAULT '45' NOT NULL,
		  support_pattern_spacing FLOAT DEFAULT '2.5' NOT NULL,
		  support_bridges tinyint(1) DEFAULT '0' NOT NULL,
		  support_z_distance FLOAT DEFAULT '0' NOT NULL,
		  support_raft_layers smallint DEFAULT '0' NOT NULL,
		  brim_width FLOAT DEFAULT '0' NOT NULL,
		  materials varchar(8192) DEFAULT '' NOT NULL,
		  infills varchar(256) DEFAULT '0,10,20,30,40,50,60,70,80,90,100' NOT NULL,
		  default_infill varchar(3) DEFAULT '20' NOT NULL,
		  infill_pattern varchar(64) DEFAULT 'honeycomb' NOT NULL,
		  infill_pattern_top_bottom varchar(64) DEFAULT 'rectilinear' NOT NULL,
		  layer_height float DEFAULT 0.1 NOT NULL,
		  perimeters smallint DEFAULT 3 NOT NULL,
		  solid_layers_top smallint DEFAULT 3 NOT NULL,
		  solid_layers_bottom smallint DEFAULT 3 NOT NULL,
		  time_per_layer float DEFAULT 17 NOT NULL,
		  wall_thickness float DEFAULT 0.8 NOT NULL,
		  bottom_top_thickness float DEFAULT 0.6 NOT NULL,
		  nozzle_size float DEFAULT 0.4 NOT NULL,
		  sort_order smallint(6) DEFAULT 0 NOT NULL,
		  group_name varchar(64) DEFAULT '' NOT NULL,
		  UNIQUE KEY id (id)
		) $charset_collate;";


	dbDelta( $sql );

	$cols = $wpdb->get_col("SELECT * FROM ".$wpdb->prefix."p3d_printers LIMIT 1" );

	if ( empty($cols) ){
		if (!empty($current_version) && version_compare($current_version, '1.5.0', '<')) {
			$current_printers=get_option('3dp_printers');
			foreach ($current_printers as $printer) {
				$printer['materials'] = '';
				$wpdb->insert( $wpdb->prefix."p3d_printers", $printer );
			}
		}
		else {
			$default_printers[]=array(
				'status' => '1',
				'name' => 'Default Printer',
				'description' => '',
				'photo' => '',
				'type' => 'fff',
				'full_color' => '1',
				'platform_shape' => 'rectangle',
				'width' => '300',
				'length' => '400',
				'height' => '300',
				'diameter' => '300',
				'min_side' => '1',
				'price' => '0.02',
				'price_type' => 'box_volume',
				'speed' => '50',
				'speed_type' => 'mms',
				'travel_speed' => '150',
				'support' => '0',
				'layer_height' => '0.1',
				'time_per_layer' => '10',
				'wall_thickness' => '0.8',
				'nozzle_size' => '0.4',
				'infills' => '0,10,20,30,40,50,60,70,80,90,100',
				'default_infill' => '20',
				'materials' => "",
				'group_name' => '',
				'sort_order' => '10'
			);
			foreach ($default_printers as $printer) {
				$wpdb->insert( $wpdb->prefix."p3d_printers", $printer );
			}
	
		}
	
	}


	$sql = "CREATE TABLE ".$wpdb->prefix."p3d_materials (
		  id mediumint(9) NOT NULL AUTO_INCREMENT,
		  status tinyint(1) DEFAULT '1' NOT NULL,
		  name varchar(64) DEFAULT '' NOT NULL,
		  description text DEFAULT '' NOT NULL,
		  photo varchar(2048) DEFAULT '' NOT NULL,
		  type varchar(64) DEFAULT 'filament' NOT NULL,
		  length smallint(6) DEFAULT 0 NOT NULL,
		  density float DEFAULT 0 NOT NULL,
		  diameter float DEFAULT 0 NOT NULL,
		  weight float DEFAULT 0 NOT NULL,
		  price varchar(128) DEFAULT '0' NOT NULL,
		  price_type varchar(32) DEFAULT 'cm3' NOT NULL,
		  price1 varchar(128) DEFAULT '0' NOT NULL,
		  price_type1 varchar(32) DEFAULT 'cm3' NOT NULL,
		  price2 varchar(128) DEFAULT '0' NOT NULL,
		  price_type2 varchar(32) DEFAULT 'cm3' NOT NULL,
		  roll_price float DEFAULT 0 NOT NULL,
		  color varchar(7) DEFAULT '' NOT NULL,
		  shininess varchar(32) DEFAULT 'plastic' NOT NULL,
		  transparency varchar(32) DEFAULT 'opaque' NOT NULL,
		  glow tinyint(1) DEFAULT '0' NOT NULL,
		  sort_order smallint(6) DEFAULT 0 NOT NULL,
		  group_name varchar(64) DEFAULT '' NOT NULL,
		  UNIQUE KEY id (id)
		) $charset_collate;";


	dbDelta( $sql );


	$cols = $wpdb->get_col("SELECT * FROM ".$wpdb->prefix."p3d_materials LIMIT 1" );

	if ( empty($cols) ){
		if (!empty($current_version) && version_compare($current_version, '1.5.0', '<')) {
			$current_materials=get_option('3dp_materials');
			foreach ($current_materials as $material) {
				$wpdb->insert( $wpdb->prefix."p3d_materials", $material );
			}
		}
		else {
			$default_materials[]=array(
				'status' => '1',
				'name' => 'PLA - Green',
				'description' => '',
				'photo' => '',
				'type' => 'filament',
				'density' => '1.26',
				'length' => '330',
				'diameter' => '1.75',
				'weight' => '1',
				'price' => '0.03',
				'price_type' => 'gram',
				'roll_price' => '20',
				'group_name' => 'PLA',
				'color' => '#08c101',
				'shininess' => 'plastic',
				'glow' => '0',
				'transparency' => 'opaque'
			);
			$default_materials[]=array(
				'status' => '1',
				'name' => 'ABS - Red',
				'description' => '',
				'photo' => '',
				'type' => 'filament',
				'density' => '1.41',
				'length' => '100',
				'diameter' => '3',
				'weight' => '1',
				'price' => '0.04',
				'price_type' => 'gram',
				'roll_price' => '25',
				'group_name' => 'ABS',
				'color' => '#dd3333',
				'shininess' => 'plastic',
				'glow' => '0',
				'transparency' => 'opaque'
			);

			foreach ($default_materials as $material) {
				$wpdb->insert( $wpdb->prefix."p3d_materials", $material );
			}
	
		}
	
	}

	$sql = "CREATE TABLE ".$wpdb->prefix."p3d_coatings (
		  id mediumint(9) NOT NULL AUTO_INCREMENT,
		  status tinyint(1) DEFAULT '1' NOT NULL,
		  name varchar(64) DEFAULT '' NOT NULL,
		  description text DEFAULT '' NOT NULL,
		  photo varchar(2048) DEFAULT '' NOT NULL,
		  price varchar(128) DEFAULT '0' NOT NULL,
		  price_type varchar(32) DEFAULT 'cm2' NOT NULL,
		  price1 varchar(128) DEFAULT '0' NOT NULL,
		  price_type1 varchar(32) DEFAULT 'cm2' NOT NULL,
		  color varchar(7) DEFAULT '' NOT NULL,
		  shininess varchar(32) DEFAULT 'plastic' NOT NULL,
		  glow tinyint(1) DEFAULT '0' NOT NULL,
		  transparency varchar(32) DEFAULT 'opaque' NOT NULL,
		  materials varchar(8192) DEFAULT '' NOT NULL,
		  sort_order smallint(6) DEFAULT 0 NOT NULL,
		  group_name varchar(64) DEFAULT '' NOT NULL,
		  UNIQUE KEY id (id)
		) $charset_collate;";


	dbDelta( $sql );

	$cols = $wpdb->get_col("SELECT * FROM ".$wpdb->prefix."p3d_coatings LIMIT 1");

	if ( empty($cols) ){
		if (!empty($current_version) && version_compare($current_version, '1.5.0', '<')) {
			$current_coatings=get_option('3dp_coatings');
			if (count($current_coatings)>0) {
				foreach ($current_coatings as $coating) {
					$wpdb->insert( $wpdb->prefix."p3d_coatings", $coating );
				}
			}
		}

	}



	$current_settings = get_option( '3dp_settings' );

	$settings=array(
		'pricing' => (isset($current_settings['pricing']) ? $current_settings['pricing'] : 'checkout'),
		'pricing_irrepairable' => (isset($current_settings['pricing_irrepairable']) ? $current_settings['pricing_irrepairable'] : 'checkout'),
		'pricing_too_large' => (isset($current_settings['pricing_too_large']) ? $current_settings['pricing_too_large'] : 'checkout'),
		'pricing_arrange' => (isset($current_settings['pricing_arrange']) ? $current_settings['pricing_arrange'] : 'checkout'),
		'pricing_api_expired' => (isset($current_settings['pricing_api_expired']) ? $current_settings['pricing_api_expired'] : 'request'),
		'price_num_decimals' => (isset($current_settings['price_num_decimals']) ? $current_settings['price_num_decimals'] : wc_get_price_decimals()),
		'use_ninjaforms' => (isset($current_settings['use_ninjaforms']) ? $current_settings['use_ninjaforms'] : ''),
		'ninjaforms_shortcode' => (isset($current_settings['ninjaforms_shortcode']) ? $current_settings['ninjaforms_shortcode'] : ''),
		'startup_price' => (isset($current_settings['startup_price']) ? $current_settings['startup_price'] : '0'),
		'startup_price_taxable' => (isset($current_settings['startup_price_taxable']) ? $current_settings['startup_price_taxable'] : ''),
		'slicer' => (isset($current_settings['slicer']) ? $current_settings['slicer'] : 'cura'),
		'default_unit' => (isset($current_settings['default_unit']) ? $current_settings['default_unit'] : 'mm'),
		'minimum_price_type' => (isset($current_settings['minimum_price_type']) ? $current_settings['minimum_price_type'] : 'minimum_price'),
		'email_address' => (isset($current_settings['email_address']) ? $current_settings['email_address'] : get_option( 'admin_email' )),
		'wizard' => (isset($current_settings['wizard']) ? $current_settings['wizard'] : '0'),
		'default_model' => (isset($current_settings['default_model']) ? $current_settings['default_model'] : ''),
		'canvas_width' => (isset($current_settings['canvas_width']) ? $current_settings['canvas_width'] : '1024'),
		'canvas_height' => (isset($current_settings['canvas_height']) ? $current_settings['canvas_height'] : '768'),
		'display_mode' => (isset($current_settings['display_mode']) ? $current_settings['display_mode'] : 'on_page'),
		'shading' => (isset($current_settings['shading']) ? $current_settings['shading'] : 'flat'),
		'auto_rotation' => (isset($current_settings['auto_rotation']) ? $current_settings['auto_rotation'] : 'on'),
		'resize_on_scale' => (isset($current_settings['resize_on_scale']) ? $current_settings['resize_on_scale'] : 'on'),
		'fit_on_resize' => (isset($current_settings['fit_on_resize']) ? $current_settings['fit_on_resize'] : 'on'),
		'background1' => (isset($current_settings['background1']) ? $current_settings['background1'] : '#FFFFFF'),
		'background2' => (isset($current_settings['background2']) ? $current_settings['background2'] : '#1e73be'),
		'plane_color' => (isset($current_settings['plane_color']) ? $current_settings['plane_color'] : '#898989'),
		'ground_color' => (isset($current_settings['ground_color']) ? $current_settings['ground_color'] : '#c1c1c1'),
		'ground_mirror' => (isset($current_settings['ground_mirror']) ? $current_settings['ground_mirror'] : ''),
		'show_shadow' => (isset($current_settings['show_shadow']) ? $current_settings['show_shadow'] : ''),
		'printer_color' => (isset($current_settings['printer_color']) ? $current_settings['printer_color'] : '#1e73be'),
		'button_color1' => (isset($current_settings['button_color1']) ? $current_settings['button_color1'] : '#1d9650'),
		'button_color2' => (isset($current_settings['button_color2']) ? $current_settings['button_color2'] : '#148544'),
		'button_color3' => (isset($current_settings['button_color3']) ? $current_settings['button_color3'] : '#0e7138'),
		'button_color4' => (isset($current_settings['button_color4']) ? $current_settings['button_color4'] : '#fff'),
		'button_color5' => (isset($current_settings['button_color5']) ? $current_settings['button_color5'] : '#fff'),
		'ajax_loader' => (isset($current_settings['ajax_loader']) ? $current_settings['ajax_loader'] : $default_image_url.'ajax-loader.gif'),
		'show_printer_box' => (isset($current_settings['show_printer_box']) ? $current_settings['show_printer_box'] : 'on'),
		'show_grid' => (isset($current_settings['show_grid']) ? $current_settings['show_grid'] : 'on'),
		'show_axis' => (isset($current_settings['show_axis']) ? $current_settings['show_axis'] : 'on'),
		'show_canvas_stats' => (isset($current_settings['show_canvas_stats']) ? $current_settings['show_canvas_stats'] : 'on'),
		'show_upload_button' => (isset($current_settings['show_upload_button']) ? $current_settings['show_upload_button'] : 'on'),
		'show_model_stats_material_volume' => (isset($current_settings['show_model_stats_material_volume']) ? $current_settings['show_model_stats_material_volume'] : 'on'),
		'show_model_stats_box_volume' => (isset($current_settings['show_model_stats_box_volume']) ? $current_settings['show_model_stats_box_volume'] : 'on'),
		'show_model_stats_surface_area' => (isset($current_settings['show_model_stats_surface_area']) ? $current_settings['show_model_stats_surface_area'] : 'on'),
		'show_model_stats_model_weight' => (isset($current_settings['show_model_stats_model_weight']) ? $current_settings['show_model_stats_model_weight'] : 'on'),
		'show_model_stats_model_dimensions' => (isset($current_settings['show_model_stats_model_dimensions']) ? $current_settings['show_model_stats_model_dimensions'] : 'on'),
		'show_model_stats_model_hours' => (isset($current_settings['show_model_stats_model_hours']) ? $current_settings['show_model_stats_model_hours'] : ''),
		'show_model_stats' => (isset($current_settings['show_model_stats']) ? $current_settings['show_model_stats'] : 'on'),
		'show_printers' => (isset($current_settings['show_printers']) ? $current_settings['show_printers'] : 'on'),
		'show_materials' => (isset($current_settings['show_materials']) ? $current_settings['show_materials'] : 'on'),
		'show_coatings' => (isset($current_settings['show_coatings']) ? $current_settings['show_coatings'] : 'on'),
		'show_infills' => (isset($current_settings['show_infills']) ? $current_settings['show_infills'] : ''),
		'show_scale' => (isset($current_settings['show_scale']) ? $current_settings['show_scale'] : 'on'),
		'scale_xyz' => (isset($current_settings['scale_xyz']) ? $current_settings['scale_xyz'] : ''),
		'show_rotation' => (isset($current_settings['show_rotation']) ? $current_settings['show_rotation'] : ''),
		'hide_plane_on_rotation' => (isset($current_settings['hide_plane_on_rotation']) ? $current_settings['hide_plane_on_rotation'] : 'on'),
		'show_unit' => (isset($current_settings['show_unit']) ? $current_settings['show_unit'] : 'on'),
		'show_filename' => (isset($current_settings['show_filename']) ? $current_settings['show_filename'] : 'on'),
		'file_chunk_size' => (isset($current_settings['file_chunk_size']) ? $current_settings['file_chunk_size'] : wp_max_upload_size()/1048576),
		'file_extensions' => (isset($current_settings['file_extensions']) ? $current_settings['file_extensions'] : 'stl,obj,stp,step,igs,iges,zip,png,jpg,jpeg,gif,bmp'),
		'file_max_size' => (isset($current_settings['file_max_size']) ? $current_settings['file_max_size'] : '20'),
		'file_max_days' => (isset($current_settings['file_max_days']) ? $current_settings['file_max_days'] : ''),
		'server_triangulation' => (isset($current_settings['server_triangulation']) ? $current_settings['server_triangulation'] : 'on'),
		'daily_hours' => (isset($current_settings['daily_hours']) ? $current_settings['daily_hours'] : '0'),
		'api_repair' => (isset($current_settings['api_repair']) ? $current_settings['api_repair'] : ''),
		'api_optimize' => (isset($current_settings['api_optimize']) ? $current_settings['api_optimize'] : ''),
		'api_pack' => (isset($current_settings['api_pack']) ? $current_settings['api_pack'] : 'on'),
		'api_pack_spacing' => (isset($current_settings['api_pack_spacing']) ? $current_settings['api_pack_spacing'] : '1'),
		'api_analyse' => (isset($current_settings['api_analyse']) ? $current_settings['api_analyse'] : ''),
		'api_login' => (isset($current_settings['api_login']) ? $current_settings['api_login'] : ''),
		'api_subscription_login' => (isset($current_settings['api_subscription_login']) ? $current_settings['api_subscription_login'] : ''),
		'api_subscription_key' => (isset($current_settings['api_subscription_key']) ? $current_settings['api_subscription_key'] : ''), 
		'cookie_expire' => (isset($current_settings['cookie_expire']) ? $current_settings['cookie_expire'] : '0'),
		'printers_layout' => (isset($current_settings['printers_layout']) ? $current_settings['printers_layout'] : 'dropdowns'),
		'materials_layout' => (isset($current_settings['materials_layout']) ? $current_settings['materials_layout'] : 'colors'),
		'coatings_slider_num' => (isset($current_settings['coatings_slider_num']) ? $current_settings['coatings_slider_num'] : '5'),
		'printers_slider_num' => (isset($current_settings['printers_slider_num']) ? $current_settings['printers_slider_num'] : '5'),
		'materials_slider_num' => (isset($current_settings['materials_slider_num']) ? $current_settings['materials_slider_num'] : '5'),
		'coatings_layout' => (isset($current_settings['coatings_layout']) ? $current_settings['coatings_layout'] : 'colors'),
		'infills_layout' => (isset($current_settings['infills_layout']) ? $current_settings['infills_layout'] : 'lists'),
		'load_everywhere' => (isset($current_settings['load_everywhere']) ? $current_settings['load_everywhere'] : ''),
		'mobile_no_animation' => (isset($current_settings['mobile_no_animation']) ? $current_settings['mobile_no_animation'] : ''),
		'items_per_page' => (isset($current_settings['items_per_page']) ? $current_settings['items_per_page'] : '10')

	);

	if (!empty($current_version) && version_compare($current_version, '1.7.6', '<')) {
		$settings['file_extensions'].=',step,stp,iges,igs';
	}

	if (!empty($current_version) && version_compare($current_version, '2.0.5.1', '<')) {
		$settings['file_extensions'].=',png,jpg,jpeg,gif,bmp';
	}

	if (!empty($current_version) && version_compare($current_version, '2.0.5.2', '<')) {
		$settings['file_extensions']=str_replace('igspng', 'igs,png', $settings['file_extensions']);
	}


	update_option( '3dp_settings', $settings );

	$sitename = strtolower( $_SERVER['SERVER_NAME'] );
	if ( substr( $sitename, 0, 4 ) == 'www.' ) {
		$sitename = substr( $sitename, 4 );
	}
	$default_from_email = 'wordpress@' . $sitename; //taken from wp_mail code


	$current_templates = get_option( '3dp_email_templates' );
	if (!isset($current_templates['admin_email_body'])) {
		$current_templates['admin_email_body'] = '
'.__('E-mail','3dprint').': [customer_email] <br>
'.__('Product ID','3dprint').': [product_id] <br>
'.__('Printer','3dprint').': [printer_name] <br>
'.__('Infill','3dprint').': [infill] <br>
'.__('Material','3dprint').': [material_name] <br>
'.__('Coating','3dprint').': [coating_name] <br>
'.__('Model','3dprint').': [model_file]  <br>
'.__('Original file','3dprint').': [original_model_file] <br>
'.__('Unit','3dprint').': [unit] <br>
'.__('Resize Scale','3dprint').': [resize_scale] <br>
'.__('Dimensions','3dprint').': [dimensions] <br>
'.__('Estimated Price','3dprint').': [estimated_price] <br>
[custom_attributes] <br>
'.__('Comments','3dprint').': [customer_comments] <br>
'.__('Manage Price Requests','3dprint').': [price_requests_link] <br>
';
	}

	if (!isset($current_templates['admin_email_from'])) {
		$current_templates['admin_email_from'] = $default_from_email;
	}

	if (!isset($current_templates['admin_email_subject'])) {
		$current_templates['admin_email_subject'] = __('Price enquiry from','3dprint').' [customer_email]';
	}

	if (!isset($current_templates['client_email_body'])) {
		$current_templates['client_email_body'] = '
'.__('Printer','3dprint').': [printer_name] <br>
'.__('Infill','3dprint').': [infill] <br>
'.__('Material','3dprint').': [material_name] <br>
'.__('Coating','3dprint').': [coating_name] <br>
'.__('Model','3dprint').': [model_file]  <br>
'.__('Dimensions','3dprint').': [dimensions] <br>
[custom_attributes] <br>
'.__('Price','3dprint').': [price] <br>
'.__('Buy Now!','3dprint').': [buy_link] <br>
'.__('Comments','3dprint').': [admin_comments] <br>
';
	}


	if (!isset($current_templates['client_email_from'])) {
		$current_templates['client_email_from'] = $default_from_email;
	}

	if (!isset($current_templates['client_email_subject'])) {
		$current_templates['client_email_subject'] = __('Your model price','3dprint');
	}


	update_option( '3dp_email_templates', $current_templates );


	add_option( '3dp_price_requests', '' );
	update_option( '3dp_servers',  array(0=>'http://srv1.wp3dprinting.com', 1=>'http://srv2.wp3dprinting.com') );



	$p3d_attr_prices=get_option( '3dp_attr_prices' );

	if (is_array($p3d_attr_prices) && count($p3d_attr_prices)) {
		foreach ($p3d_attr_prices as $key=>$value) {
			foreach ($value as $key1=>$info) {
				if ($info['price_type']=='pct' && $info['pct_type']=='') {
					$p3d_attr_prices[$key][$key1]['pct_type']='total';
				}
			}
		}
		update_option('3dp_attr_prices', $p3d_attr_prices);
	}
	update_option( '3dp_version', P3D_VERSION );

}

function p3d_get_max_id ($option) {
	global $wpdb;

	if ($option == '3dp_printers') $option = 'p3d_printers';
	if ($option == '3dp_materials') $option = 'p3d_materials';
	if ($option == '3dp_coatings') $option = 'p3d_coatings';

	$query = "SELECT max(id) as maxid FROM ".$wpdb->prefix.$option;

	$total = $wpdb->get_row( $query );

	return $total->maxid;


}

function p3d_get_option_total ($option) {
	global $wpdb;

	if ($option == '3dp_printers') $option = 'p3d_printers';
	if ($option == '3dp_materials') $option = 'p3d_materials';
	if ($option == '3dp_coatings') $option = 'p3d_coatings';

	$query = "SELECT * FROM ".$wpdb->prefix.$option;
	$total_query = "SELECT COUNT(1) FROM (${query}) AS combined_table";
	$total = $wpdb->get_var( $total_query );

	return $total;


}

function p3d_get_option ($option, $pagination=false) {
	global $wpdb;
	$output=array();

	if ($option == '3dp_printers') $option = 'p3d_printers';
	if ($option == '3dp_materials') $option = 'p3d_materials';
	if ($option == '3dp_coatings') $option = 'p3d_coatings';

	if($wpdb->get_var("SHOW TABLES LIKE '".$wpdb->prefix."$option'") == $wpdb->prefix.$option) {

		$query = "SELECT * FROM ".$wpdb->prefix.$option;
		$settings = get_option( '3dp_settings' );
		$items_per_page = (int)$settings['items_per_page'];
		$page = (isset( $_GET['cpage'] ) && is_numeric( $_GET['cpage'] )) ? abs( (int) $_GET['cpage'] ) : 1;
		if (isset($_GET['p3d_section']) && $_GET['p3d_section']!=$option) $page=1;

		$offset = ( $page * $items_per_page ) - $items_per_page;
		if ($pagination) {
			$results = $wpdb->get_results( "$query ORDER BY id desc LIMIT ${offset}, ${items_per_page}", ARRAY_A );
		}
		else {
			$results = $wpdb->get_results( "$query", ARRAY_A );
		}
		
		foreach ($results as $result) {
			//$result['_option']=$option;
			$output[$result['id']]=$result;
		}
		return $output;
	}
	else {
		return get_option($option);
	}
}

function p3d_add_option ($option, $data) {
	global $wpdb;
	switch ($option) {
		case '3dp_printers' :
			$wpdb->insert( $wpdb->prefix . 'p3d_printers', $data );
		break;

		case '3dp_materials' :
			$wpdb->insert( $wpdb->prefix . 'p3d_materials', $data );
		break;

		case '3dp_coatings' :
			$wpdb->insert( $wpdb->prefix . 'p3d_coatings', $data );
		break;

		default :
			add_option($data);
		break;
	
	}
}

function p3d_update_option ($option, $data) {
	global $wpdb;

	switch ($option) {
		case '3dp_printers' :
			$wpdb->replace( $wpdb->prefix . 'p3d_printers', $data );

		break;

		case '3dp_materials' :
			$wpdb->replace( $wpdb->prefix . 'p3d_materials', $data );
		break;

		case '3dp_coatings' :
			$wpdb->replace( $wpdb->prefix . 'p3d_coatings', $data );
		break;

		default :
			update_option($option, $data);
		break;
	
	}

}

add_action( 'plugins_loaded', 'p3d_load_textdomain' );
function p3d_load_textdomain() {
	load_plugin_textdomain( '3dprint', false, dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/' );
}


function p3d_attribute_slug_to_title( $attribute ,$slug ) {
	global $woocommerce;
	$value = "";
	if ( taxonomy_exists( esc_attr( str_replace( 'attribute_', '', $attribute ) ) ) ) {
		$term = get_term_by( 'slug', $slug, esc_attr( str_replace( 'attribute_', '', $attribute ) ) );
		if (!is_object($term)) return '';
		if ( ! is_wp_error( $term ) && $term->name ) 
			$value = $term->name;

	} else {
		$value = apply_filters( 'woocommerce_variation_option_name', $value );
	}
	return $value;
}




function p3d_filter_update_checks($queryArgs) {
	$settings = get_option('3dp_settings');
	if ( !empty($settings['api_login']) ) {
		$queryArgs['login'] = $settings['api_login'];
	}
	return $queryArgs;
}


function p3d_enqueue_scripts_backend() {
	global $wp_scripts;
	$p3d_current_version = get_option('3dp_version');

	if (isset($_GET['page']) && $_GET['page']=='3dprint') {
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'jquery-ui' );
		wp_enqueue_script( 'jquery-ui-dialog' );
		wp_enqueue_script( 'jquery-ui-tabs' );
		wp_enqueue_script( 'js/3dprint-backend.js', plugin_dir_url( __FILE__ ).'js/3dprint-backend.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'jquery.sumoselect.min.js',  plugin_dir_url( __FILE__ ).'ext/sumoselect/jquery.sumoselect.min.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'jquery-ui.min.js',  plugin_dir_url( __FILE__ ).'ext/jquery-ui/jquery-ui.min.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_style( 'sumoselect.css', plugin_dir_url( __FILE__ ).'ext/sumoselect/sumoselect.css', array(), $p3d_current_version );
		wp_enqueue_script( 'tooltipster.js',  plugin_dir_url( __FILE__ ).'ext/tooltipster/js/jquery.tooltipster.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_style( 'tooltipster.css', plugin_dir_url( __FILE__ ).'ext/tooltipster/css/tooltipster.css', array(), $p3d_current_version );
		wp_enqueue_style( 'jquery-ui.min.css', plugin_dir_url( __FILE__ ).'ext/jquery-ui/jquery-ui.min.css', array(), $p3d_current_version );
		wp_enqueue_style( '3dprint-backend.css', plugin_dir_url( __FILE__ ).'css/3dprint-backend.css', array(), $p3d_current_version );
	}
}

function p3d_enqueue_scripts_frontend() {
	global $post, $woocommerce;
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if( !is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
		return;
	}
//	if ( is_shop() ) return false;

	$available_variations = array();
	$post_object = get_post(get_the_ID());


	if ($post_object->post_type=='product') {
		$product = new WC_Product_Variable( get_the_ID() );
		if ( !method_exists( $product, 'get_available_variations' ) ) return false;
		$available_variations=$product->get_available_variations();
	}


	$p3d_current_version = get_option('3dp_version');


	$settings = get_option( '3dp_settings' );


	wp_enqueue_style( '3dprint-frontend-global.css', plugin_dir_url( __FILE__ ).'css/3dprint-frontend-global.css', array(), $p3d_current_version );
	$fix_css = "";
	if ($settings['show_printers']!='on') {
		$fix_css .= ".woocommerce td.product-name dl.variation dd.variation-Printer, .woocommerce td.product-name dl.variation dt.variation-Printer {
 				   display: none !important; 
			}";
	}
	if ($settings['show_materials']!='on') {
		$fix_css .= ".woocommerce td.product-name dl.variation dd.variation-Material, .woocommerce td.product-name dl.variation dt.variation-Material {
 				   display: none !important; 
			}";
	}
	if ($settings['show_coatings']!='on') {
		$fix_css .= ".woocommerce td.product-name dl.variation dd.variation-Coating, .woocommerce td.product-name dl.variation dt.variation-Coating {
 				   display: none !important; 
			}";
	}
	if ($settings['show_unit']!='on') {
		$fix_css .= ".woocommerce td.product-name dl.variation dd.variation-Unit, .woocommerce td.product-name dl.variation dt.variation-Unit {
 				   display: none !important; 
			}";
	}
	if ($settings['show_filename']!='on') {
		$fix_css .= ".woocommerce td.product-name dl.variation dd.variation-Model, .woocommerce td.product-name dl.variation dt.variation-Model {
 				   display: none !important; 
			}";
	}


	wp_add_inline_style( '3dprint-frontend-global.css', $fix_css );

	if ($settings['load_everywhere']!='on') $condition = (isset( $available_variations[0]['attributes']['attribute_pa_p3d_printer'] ) && isset( $available_variations[0]['attributes']['attribute_pa_p3d_material'] ) && isset( $available_variations[0]['attributes']['attribute_pa_p3d_model'] ) && isset( $available_variations[0]['attributes']['attribute_pa_p3d_unit'] ));
	else $condition = true;

	if ( $condition ) {
		wp_enqueue_style( '3dprint-frontend.css', plugin_dir_url( __FILE__ ).'css/3dprint-frontend.css', array(), $p3d_current_version );
		wp_enqueue_style( 'component.css', plugin_dir_url( __FILE__ ).'ext/ProgressButtonStyles/css/component.css', array(), $p3d_current_version );
		wp_enqueue_style( 'nouislider.min.css', plugin_dir_url( __FILE__ ).'ext/noUiSlider/nouislider.min.css', array(), $p3d_current_version );
		wp_enqueue_style( 'bxslider.css', plugin_dir_url( __FILE__ ).'ext/bxslider/css/jquery.bxslider.css', array(), $p3d_current_version );
		wp_enqueue_style( 'easyaspie-main.css', plugin_dir_url( __FILE__ ).'ext/easyaspie/assets/css/main.css', array(), $p3d_current_version );
		wp_enqueue_style( 'p3d-extruder-css', plugin_dir_url( __FILE__ ).'ext/mb.extruder/mbExtruder.css', array(), $p3d_current_version );
		wp_enqueue_style( 'tooltipster.bundle.min.css', plugin_dir_url( __FILE__ ).'ext/tooltipster/css/tooltipster.bundle.min.css', array(), $p3d_current_version );
		wp_enqueue_style( 'tooltipster-sideTip-light.min.css ', plugin_dir_url( __FILE__ ).'ext/tooltipster/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-light.min.css', array(), $p3d_current_version );

		if( version_compare( $woocommerce->version, '3.0', ">=" ) ) {
			wp_enqueue_style( 'prettyPhoto.css', plugin_dir_url( __FILE__ ).'ext/prettyPhoto/css/prettyPhoto.css', array(), $p3d_current_version );
		}

		wp_enqueue_script( 'tooltipster.bundle.min.js',  plugin_dir_url( __FILE__ ).'ext/tooltipster/js/tooltipster.bundle.min.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'modernizr.custom.js',  plugin_dir_url( __FILE__ ).'ext/ProgressButtonStyles/js/modernizr.custom.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'jquery.bxslider.js',  plugin_dir_url( __FILE__ ).'ext/bxslider/js/jquery.bxslider.js', array( 'jquery' ), $p3d_current_version );

		wp_enqueue_script( 'p3d-threejs',  plugin_dir_url( __FILE__ ).'ext/threejs/three.min.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'p3d-threejs-detector',  plugin_dir_url( __FILE__ ).'ext/threejs/js/Detector.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'p3d-threejs-mirror',  plugin_dir_url( __FILE__ ).'ext/threejs/js/Mirror.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'p3d-threejs-controls',  plugin_dir_url( __FILE__ ).'ext/threejs/js/controls/OrbitControls.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'p3d-threejs-canvas-renderer',  plugin_dir_url( __FILE__ ).'ext/threejs/js/renderers/CanvasRenderer.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'p3d-threejs-projector-renderer',  plugin_dir_url( __FILE__ ).'ext/threejs/js/renderers/Projector.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'p3d-threejs-stl-loader',  plugin_dir_url( __FILE__ ).'ext/threejs/js/loaders/STLLoader.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'p3d-threejs-obj-loader',  plugin_dir_url( __FILE__ ).'ext/threejs/js/loaders/OBJLoader.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'p3d-threejs-mtl-loader',  plugin_dir_url( __FILE__ ).'ext/threejs/js/loaders/MTLLoader.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'p3d-threex-dilategeometry',  plugin_dir_url( __FILE__ ).'ext/threex/threex.dilategeometry.js', array( 'p3d-threejs', 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'p3d-threex-atmospherematerial',  plugin_dir_url( __FILE__ ).'ext/threex/threex.atmospherematerial.js', array( 'p3d-threejs', 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'p3d-threex-geometricglowmesh',  plugin_dir_url( __FILE__ ).'ext/threex/threex.geometricglowmesh.js', array( 'p3d-threejs', 'jquery' ), $p3d_current_version );

		wp_enqueue_script( 'p3d-extruder-hoverintent',  plugin_dir_url( __FILE__ ).'ext/mb.extruder/jquery.hoverIntent.min.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'p3d-extruder-fliptext',  plugin_dir_url( __FILE__ ).'ext/mb.extruder/jquery.mb.flipText.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'p3d-extruder',  plugin_dir_url( __FILE__ ).'ext/mb.extruder/mbExtruder.js', array( 'jquery', 'p3d-extruder-hoverintent', 'p3d-extruder-fliptext' ), $p3d_current_version );

		if( version_compare( $woocommerce->version, '3.0', ">=" ) ) {
			wp_enqueue_script( 'jquery.prettyPhoto.min.js',  plugin_dir_url( __FILE__ ).'ext/prettyPhoto/js/jquery.prettyPhoto.min.js', array( 'jquery' ), $p3d_current_version );
			wp_enqueue_script( 'jquery.prettyPhoto.init.min.js',  plugin_dir_url( __FILE__ ).'ext/prettyPhoto/js/jquery.prettyPhoto.init.min.js', array( 'jquery' ), $p3d_current_version );
		}
		wp_enqueue_script( 'plupload.full.min.js',  plugin_dir_url( __FILE__ ).'ext/plupload/plupload.full.min.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'classie.js',  plugin_dir_url( __FILE__ ).'ext/ProgressButtonStyles/js/classie.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'progressButton.js',  plugin_dir_url( __FILE__ ).'ext/ProgressButtonStyles/js/progressButton.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'event-manager.js',  plugin_dir_url( __FILE__ ).'ext/event-manager/event-manager.js', array(), $p3d_current_version );
		wp_enqueue_script( 'accounting.js',  plugin_dir_url( __FILE__ ).'ext/accounting/accounting.min.js', array(), $p3d_current_version );
		wp_enqueue_script( 'nouislider.min.js',  plugin_dir_url( __FILE__ ).'ext/noUiSlider/nouislider.min.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'easyaspie.superfish.js',  plugin_dir_url( __FILE__ ).'ext/easyaspie/assets/js/superfish.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( 'easyaspie.js',  plugin_dir_url( __FILE__ ).'ext/easyaspie/assets/js/easyaspie.js', array( 'jquery' ), $p3d_current_version );
		wp_enqueue_script( '3dprint-frontend.js',  plugin_dir_url( __FILE__ ).'js/3dprint-frontend.js', array( 'jquery', 'jquery-cookie' ), $p3d_current_version );

		$plupload_langs=array( 'ku_IQ', 'pt_BR', 'sr_RS', 'th_TH', 'uk_UA', 'zh_CN', 'zh_TW' );
		$current_locale = get_locale() ;
		list ( $lang, $LANG ) = explode( '_', $current_locale );
		if ( in_array( $current_locale, $plupload_langs ) ) $plupload_locale=$current_locale;
		else $plupload_locale=$lang;

		wp_enqueue_script( "$plupload_locale.js",  plugin_dir_url( __FILE__ )."ext/plupload/i18n/$plupload_locale.js" );

		$settings=get_option( '3dp_settings' );
//		$min_price = $product->price;
		$p3d_file_url = get_post_meta(get_the_ID(), 'p3d_file_url', true);
		$p3d_product_price_type = get_post_meta(get_the_ID(), 'p3d_product_price_type', true);
		$p3d_product_pricing = get_post_meta(get_the_ID(), 'p3d_product_pricing', true);
		if ($p3d_product_pricing!='') {
			$settings['pricing']=$p3d_product_pricing;
		}
		if (strlen($p3d_file_url)>0 && $p3d_product_price_type=='fixed') {
			$settings['pricing']='checkout';
		}

		$text_repairing_model='';
		if ($settings['api_repair']=='on') $text_repairing_model = __('Repairing..', '3dprint');
		if ($settings['api_optimize']=='on') $text_repairing_model = __('Optimizing..', '3dprint');

		if ($settings['api_repair']=='on' && $settings['api_optimize']=='on') $text_repairing_model = __('Optimizing and repairing..', '3dprint');
		if ($settings['ninjaforms_shortcode']) {
			$shortcode_atts = shortcode_parse_atts( $settings['ninjaforms_shortcode'] );
			$form_id = (int)$shortcode_atts['id'];
		}


		wp_localize_script( '3dprint-frontend.js', 'p3d',
			array(
				'url' => admin_url( 'admin-ajax.php' ),
				'plugin_url' => plugin_dir_url( dirname( __FILE__ ) ),
				'default_unit' => $settings['default_unit'],
				'error_box_fit' => __( '<span id=\'printer_fit_error\'><b>Error:</b> The model does not fit into the selected printer</span>', '3dprint' ),
				'warning_box_fit' => __( '<span id=\'printer_fit_warning\'><b>Warning:</b> The model might not fit into the selected printer</span>', '3dprint' ),
				'warning_cant_triangulate' => __( '<b>Warning:</b> Can\'t triangulate', '3dprint' ),
				'text_repairing_model' => __( "$text_repairing_model", '3dprint' ),
				'text_model_repaired' => $text_repairing_model . __( "done!", '3dprint' ),
				'text_model_repair_report' => __( 'Error report:', '3dprint' ),
				'text_model_repair_failed' => $text_repairing_model . __( "fail!", '3dprint' ),
				'text_model_no_repair_needed' => __( 'No errors found.', '3dprint' ),
				'text_model_repair_degenerate_facets' => __( 'Degenerate facets', '3dprint' ),
				'text_model_repair_edges_fixed' => __( 'Edges fixed', '3dprint' ),
				'text_model_repair_facets_removed' => __( 'Facets removed', '3dprint' ),
				'text_model_repair_facets_added' => __( 'Facets added', '3dprint' ),
				'text_model_repair_facets_reversed' => __( 'Facets reversed', '3dprint' ),
				'text_model_repair_backwards_edges' => __( 'Backwards edges', '3dprint' ),
				'text_analysing_model' => __( 'Analysing model..', '3dprint' ),
				'text_model_analysed' => __( 'Analysing model.. done!', '3dprint' ),
				'text_model_analyse_failed' => __( 'Analysing model.. fail!', '3dprint' ),
				'text_triangulating_model' => __( 'Triangulating model', '3dprint' ),
				'text_model_triangulated' => __( 'Triangulating model.. done!', '3dprint' ),
				'text_model_triangulate_failed' => __( 'Triangulating model.. fail!', '3dprint' ),
				'text_processing_model' => __( 'Scaling/Rotating..', '3dprint' ),
				'text_processing_model_done' => __( 'Scaling/Rotating.. done!', '3dprint' ),
				'text_processing_model_failed' => __( 'Scaling/Rotating.. fail!', '3dprint' ),
				'text_printer' => __( 'Printer', '3dprint' ),
				'text_material' => __( 'Material', '3dprint' ),
				'text_coating' => __( 'Coating', '3dprint' ),
				'text_infill' => __( 'Infill', '3dprint' ),
				'text_image_height' => __( 'Image height (1-100 mm)', '3dprint' ),
				'text_image_map' => __( 'If brighter is higher press Ok. If darker is higher press Cancel.', '3dprint' ),
				'text_cant_rotate_obj' => __( 'Can not rotate textured OBJ files yet.', '3dprint' ),
				'pricing' => $settings['pricing'],
				'pricing_irrepairable' => $settings['pricing_irrepairable'],
				'pricing_too_large' => $settings['pricing_too_large'],
				'pricing_arrange' => $settings['pricing_arrange'],
				'pricing_api_expired' => $settings['pricing_api_expired'],
				'minimum_price_type' => $settings['minimum_price_type'],
				'use_ninjaforms' => $settings['use_ninjaforms'],
				'ninjaforms_form_id' => (int)$form_id,
				'shading' => $settings['shading'],
				'display_mode' => $settings['display_mode'],
				'show_shadow' => $settings['show_shadow'],
				'ground_mirror' => $settings['ground_mirror'],
				'hide_plane_on_rotation' => $settings['hide_plane_on_rotation'],
				'background1' => str_replace( '#', '0x', $settings['background1']),
#				'background2' => $settings['background2'],
				'plane_color' => str_replace( '#', '0x', $settings['plane_color'] ),
				'ground_color' => str_replace( '#', '0x', $settings['ground_color'] ),
				'printer_color' => str_replace( '#', '0x', $settings['printer_color'] ),
				'scale_xyz' => $settings['scale_xyz'],
				'file_max_size' => $settings['file_max_size'],
				'file_extensions' => $settings['file_extensions'],
				'files_to_convert' => p3d_get_files_to_convert(),
				'file_chunk_size' => $settings['file_chunk_size'],
				'currency_symbol' => get_woocommerce_currency_symbol(),
				'currency_position' => get_option( 'woocommerce_currency_pos' ),
				'thousand_sep' => get_option( 'woocommerce_price_thousand_sep', ',' ),
				'decimal_sep' => get_option( 'woocommerce_price_decimal_sep', '.' ),
				'price_num_decimals' => $settings['price_num_decimals'],
//				'min_price' => $product->price,
				'api_repair' => $settings['api_repair'],
				'api_optimize' => $settings['api_optimize'],
				'api_pack' => $settings['api_pack'],
				'api_pack_spacing' => $settings['api_pack_spacing'],
				'server_triangulation' => $settings['server_triangulation'],
				'api_analyse' => $settings['api_analyse'],
				'cookie_expire' => $settings['cookie_expire'],
				'auto_rotation' => $settings['auto_rotation'],
				'resize_on_scale' => $settings['resize_on_scale'],
				'fit_on_resize' => $settings['fit_on_resize'],
				'show_printer_box' => $settings['show_printer_box'],
				'show_grid' => $settings['show_grid'],
				'show_axis' => $settings['show_axis'],
				'show_model_stats_model_hours' => $settings['show_model_stats_model_hours'],
				'show_upload_button' => $settings['show_upload_button'],
				'show_infills' => $settings['show_infills'],
				'mobile_no_animation' => $settings['mobile_no_animation']
			)
		);


		$custom_css = "
			.progress-button[data-perspective] .content { 
			 	background: ".$settings['button_color1'].";
			}

			.progress-button .progress { 
				background: ".$settings['button_color2']."; 
			}

			.progress-button .progress-inner { 
				background: ".$settings['button_color3']."; 
			}
			.progress-button {
				color: ".$settings['button_color4'].";
			}
			.progress-button .content::before,
			.progress-button .content::after  {
				color: ".$settings['button_color5'].";
			}
		";
		wp_add_inline_style( '3dprint-frontend.css', $custom_css );
		if  ( p3d_is_p3d ( get_the_ID() ) || $condition ) {
			$fix_css = "
				.product.has-default-attributes.has-children > .images {
					opacity:1 !important;
				}
				@media screen and (max-width: 400px) {
				   .product.has-default-attributes.has-children > .images { 
				    float: none;
				    margin-right:0;
				    width:auto;
				    border:0;
				    border-bottom:2px solid #000;    
				  }
				}
				@media screen and (max-width:800px){
					.product.has-default-attributes.has-children > .images  {
						width: auto !important;
					}

				}
			";
			wp_add_inline_style( '3dprint-frontend.css', $fix_css );
		}
	}


	
}


add_action( 'admin_init', 'p3d_plugin_redirect' );
function p3d_plugin_redirect() {
	if ( get_option( 'p3d_do_activation_redirect', false ) ) {
		delete_option( 'p3d_do_activation_redirect' );
		if ( !isset( $_GET['activate-multi'] ) ) {
			wp_redirect( admin_url( 'admin.php?page=3dprint' ) );exit;
		}
	}
}

function p3d_extension($file) {
	$array=explode('.',$file);
	$ext=array_pop($array);
	return strtolower($ext);
} 

function p3d_get_original($file) {

	$uploads = wp_upload_dir( );
	$upload_dir = $uploads['basedir'];

	list ($starting_index,) = explode('_', p3d_basename($file));
	$files = array();
	$original_file = '';

	foreach (glob($uploads['basedir']."/p3d/$starting_index*") as $filename) {
		if (p3d_extension($filename)=='zip') return $filename;
		$mtime = filemtime($filename);
		$files[$mtime] = $filename;
	}

	if (count($files)) {
		ksort($files);
		$original_file = array_shift($files);
	}

	return $original_file;
}

add_action( 'woocommerce_order_item_line_item_html', 'p3d_order_item_html', 10, 2 );
function p3d_order_item_html( $item_id, $item ) {
	global $woocommerce;
	$order = wc_get_order( $_GET['post'] );
        if ( !is_object($order) || (!method_exists($order, 'get_item_meta') && !method_exists($order, 'wc_get_order_item_meta')) || !method_exists($order, 'get_product_from_item') ) return false;
	if ( version_compare( $woocommerce->version, '3.0.0', '<' ) ) {
		$item_meta = $order->get_item_meta( $item_id );
		$product = $order->get_product_from_item( $item );
		$product_id = $product->parent->id;
	}
	else {
		$item_meta = wc_get_order_item_meta( $item_id, false );
		$product = $order->get_product_from_item( $item );
		$product_id = $product->get_parent_id();
	}



	$upload_dir = wp_upload_dir();

	if ( !empty( $item_meta['pa_p3d_model'][0] ) ) {
		$p3dmodel=$item_meta['pa_p3d_model'][0];

		$link = $upload_dir['baseurl']."/p3d/$p3dmodel";
		$image = $link.".png";

		$p3d_file_url_meta = get_post_meta($product_id, 'p3d_file_url'); $p3d_file_url=$p3d_file_url_meta[0];
		$p3d_product_price_type_meta = get_post_meta($product_id, 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];

		if (strlen($p3d_file_url)>0) {
			$upload_dir = wp_upload_dir();
			$uploads = dirname($upload_dir['basedir']).'/'.dirname(substr($p3d_file_url, strpos($p3d_file_url, 'uploads/'))).'/';

			$basename = p3d_basename( $p3d_file_url );
			$p3dmodel_file = $basename;
			$link = $p3d_file_url;
			$image = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
		}

		$p3dmodel_file = urldecode($p3dmodel);
		$original_file = p3d_basename(p3d_get_original($p3dmodel_file));


		$original_link = $upload_dir['baseurl']."/p3d/".rawurlencode(p3d_basename($original_file));
		if (strlen($p3d_file_url)>0) {
			$original_link = $p3d_file_url;
			$p3d_original_file_url_meta = get_post_meta($product_id, 'p3d_original_file_url'); 
			if (strlen($p3d_original_file_url_meta[0])>0) $original_link=$p3d_original_file_url_meta[0];
			$image = $upload_dir['baseurl']."/p3d/".rawurlencode(p3d_basename($original_file));

			echo '<tr><td></td><td><img width="50" src="'.$image.'"></td><td><b>'.__( 'Download final model', '3dprint' ).':</b> <a target="_blank" href="'.$link.'">'.urldecode($p3dmodel_file).'</a></td></tr>';
			echo '<tr><td></td><td><img width="50" src="'.$image.'"></td><td><b>'.__( 'Download original', '3dprint' ).':</b> <a target="_blank" href="'.$original_link.'">'.urldecode(p3d_basename($original_link)).'</a></td></tr>';

		}
		else if (file_exists($upload_dir['basedir']."/p3d/".$original_file) && p3d_basename($original_file) != p3d_basename($p3dmodel_file)) {
			echo '<tr><td></td><td><img width="50" src="'.$image.'"></td><td><b>'.__( 'Download final model', '3dprint' ).':</b> <a target="_blank" href="'.$link.'">'.urldecode($p3dmodel_file).'</a></td></tr>';
			echo '<tr><td></td><td><img width="50" src="'.$image.'"></td><td><b>'.__( 'Download original', '3dprint' ).':</b> <a target="_blank" href="'.$original_link.'">'.urldecode(p3d_basename($original_file)).'</a></td></tr>';
		}

		else {
			echo '<tr><td></td><td><img width="50" src="'.$image.'"></td><td><b>'.__( 'Download', '3dprint' ).':</b> <a target="_blank" href="'.$link.'">'.urldecode($p3dmodel_file).'</a></td></tr>';
		}

	}
}


function p3d_deactivate() {
	global $wpdb;
/*
	$postlist = get_posts(array('post_type'  => 'product'));
	foreach ( $postlist as $post ) {
		if ( p3d_is_p3d( $post->ID ) ) p3d_delete_p3d( $post->ID );
	}
*/

	do_action( '3dprint_deactivate' );
}

function p3d_delete_p3d( $post_id ) {
	$children = get_posts( array(
			'post_parent'  => $post_id,
			'posts_per_page'=> -1,
			'post_type'  => 'product_variation',
			'fields'   => 'ids',
			'post_status' => 'publish'
		) );
	if ( count( $children ) ) {
		foreach ( $children as $child_id ) {
			$child_meta=get_post_meta( $child_id );
			if ( isset( $child_meta['attribute_pa_p3d_printer'] ) && isset( $child_meta['attribute_pa_p3d_material'] ) && isset( $child_meta['attribute_pa_p3d_model'] ) && isset( $child_meta['attribute_pa_p3d_model'] ) && isset( $child_meta['attribute_pa_p3d_unit'] ) ) {
				wp_delete_post( $child_id );
			}
		}
	}
}

function p3d_is_p3d( $post_id ) {
	$children = get_posts( array(
			'post_parent'  => $post_id,
			'posts_per_page'=> -1,
			'post_type'  => 'product_variation',
			'fields'   => 'ids',
			'post_status' => 'any'
		) );
	if ( count( $children ) ) {

		foreach ( $children as $child_id ) {
			$child_meta=get_post_meta( $child_id );
			if ( isset( $child_meta['attribute_pa_p3d_printer'] ) && isset( $child_meta['attribute_pa_p3d_material'] ) && isset( $child_meta['attribute_pa_p3d_model'] ) && isset( $child_meta['attribute_pa_p3d_model'] ) && isset( $child_meta['attribute_pa_p3d_unit'] ) ) {
				return true;
			}
		}
	}
	return false;
}

function p3d_fix_price ($price) {	
	$price =  str_replace(',', '.', $price);
	if (!strstr($price, ':')) $price = (float)$price;
	return $price;
}

function p3d_str_lreplace($search, $replace, $subject) {
	$pos = strrpos($subject, $search);

	if($pos !== false) {
		$subject = substr_replace($subject, $replace, $pos, strlen($search));
	}

	return $subject;
}


add_filter( 'get_pa_p3d_coating', 'p3d_coating_term', 10, 2 );
function p3d_coating_term($_term, $taxonomy) {
	$_term->slug=true;
	return $_term;
}
add_filter( 'get_pa_p3d_material', 'p3d_material_term', 10, 2 );
function p3d_material_term($_term, $taxonomy) {
	$_term->slug=true;
	return $_term;
}
add_filter( 'get_pa_p3d_printer', 'p3d_printer_term', 10, 2 );
function p3d_printer_term($_term, $taxonomy) {
	$_term->slug=true;
	return $_term;
}
add_filter( 'get_pa_p3d_model', 'p3d_model_term', 10, 2 );
function p3d_model_term($_term, $taxonomy) {
	$_term->slug=true;
	return $_term;
}
add_filter( 'get_pa_p3d_infill', 'p3d_infill_term', 10, 2 );
function p3d_infill_term($_term, $taxonomy) {
	$_term->slug=true;
	return $_term;
}
add_filter( 'get_pa_p3d_scale', 'p3d_scale_term', 10, 2 );
function p3d_scale_term($_term, $taxonomy) {
	$_term->slug=true;
	return $_term;
}
add_filter( 'get_pa_p3d_unit', 'p3d_unit_term', 10, 2 );
function p3d_unit_term($_term, $taxonomy) {
	$_term->slug=true;
	return $_term;
}

add_filter( 'product_type_options', 'p3d_product_type_options' );
function p3d_product_type_options( $options ) {

	if ( isset( $_REQUEST['post'] ) ) {
		$post_id=(int)$_REQUEST['post'];
		if ( p3d_is_p3d( $post_id ) ) $default='yes';
		else $default='no';
	}
	else $default='no';


	$new_option=array( '3dprinting' => array(
			'id'            => '_3dprinting',
			'wrapper_class' => 'show_if_variable',
			'label'         => __( '3D Printing Product', '3dprint' ),
			'description'   => __( '3D Printing Product', '3dprint' ),
			'default'       => $default
		) );
	$options['3dprinting']=$new_option['3dprinting'];
	return $options;
}

//3D printing product checked
function p3d_save_post( $post_id ) {

	if ( wp_is_post_revision( $post_id ) )
		return;
	if ( isset( $_POST['post_ID'] ) && $_POST['post_ID']==$post_id ) {
		if (isset($_POST['p3d_file_url'])) {
			if (strtolower(p3d_extension($_POST['p3d_file_url']))=='zip') {
			if (class_exists('ZipArchive')) {
				$allowed_extensions_inside_archive=p3d_get_allowed_extensions_inside_archive();

				$upload_dir = wp_upload_dir();
#				print_r($upload_dir);
				$time=uniqid();
				$targetDir = dirname($upload_dir['basedir']).'/'.dirname(substr($_POST['p3d_file_url'], strpos($_POST['p3d_file_url'], 'uploads/'))).'/';
				$filePath = $targetDir.p3d_basename($_POST['p3d_file_url']);
				if (!file_exists("$targetDir/tmp")) mkdir ("$targetDir/tmp");

				$zip = new ZipArchive;
				$res = $zip->open( $filePath );
				if ( $res === TRUE ) {

					for( $i = 0; $i < $zip->numFiles; $i++ ) {
						$file_to_extract = p3d_basename( $zip->getNameIndex($i) );
						$f2e_path_parts = pathinfo($file_to_extract);
						$f2e_extension = mb_strtolower($f2e_path_parts['extension']);

						if (!in_array($f2e_extension, $allowed_extensions_inside_archive)) continue;
						if ( in_array($f2e_extension, p3d_get_accepted_models()) && !in_array($f2e_extension, p3d_get_support_extensions_inside_archive())) {
							
							$file_found = true;
							$file_to_extract = rawurlencode( p3d_basename( $file_to_extract ) ) ;
							$wp_filename =  $time.'_'.$file_to_extract ;
							$extension = p3d_extension($file_to_extract);
						}
						$zip->extractTo( "$targetDir/tmp", array( $zip->getNameIndex($i) ) );
                                                $files = p3d_find_all_files("$targetDir/tmp");
						foreach ($files as $filename) {

							rename($filename, $targetDir.$time."_".$file_to_extract);
							if (strtolower(p3d_extension($filename))=='mtl') { 
								$material_file = $time."_".$file_to_extract;
								p3d_process_mtl($targetDir.$time."_".$file_to_extract, $time);
							}
						}

					}

					$zip->close();
					if ( !$file_found ) {
						//die( '{"jsonrpc" : "2.0", "error" : {"code": 104, "message": "'.__( 'Model file not found.', '3dprint' ).'"}, "id" : "id"}' );
					}
					//copy($filePath, $targetDir.$wp_filename.'.zip');
					update_post_meta( $post_id, 'p3d_original_file', $targetDir.$wp_filename.'.zip' );
					update_post_meta( $post_id, 'p3d_original_file_url', $_POST['p3d_file_url'] );
				}
				else {
					//die( '{"jsonrpc" : "2.0", "error" : {"code": 105, "message": "'.__( 'Could not extract the file.', '3dprint' ).'"}, "id" : "id"}' );
				}
			}

				
			}
			else {//not zip, no need to keep original url
				update_post_meta( $post_id, 'p3d_original_file', '' );
				update_post_meta( $post_id, 'p3d_original_file_url', '' );

			}

			update_post_meta( $post_id, 'p3d_file_url', $_POST['p3d_file_url'] );
			if (strlen($wp_filename)>0){
				update_post_meta( $post_id, 'p3d_file_url', dirname($_POST['p3d_file_url']).'/'.$wp_filename );
			}
			update_post_meta( $post_id, 'p3d_product_price_type', $_POST['p3d_product_price_type'] );
			update_post_meta( $post_id, 'p3d_product_display_mode', $_POST['p3d_product_display_mode'] );
			update_post_meta( $post_id, 'p3d_product_pricing', $_POST['p3d_product_pricing'] );


		}
		if ( isset( $_POST['_3dprinting'] ) && $_POST['_3dprinting']=='on' ) {

			update_post_meta( $post_id, '_3dprinting', 'yes' );

			if ( p3d_is_p3d( $post_id ) ) return false;

			wp_set_object_terms( $post_id, 'all', 'pa_p3d_printer' , false );
			wp_set_object_terms( $post_id, 'all', 'pa_p3d_material' , false );
			wp_set_object_terms( $post_id, 'all', 'pa_p3d_coating' , false );
			wp_set_object_terms( $post_id, 'all', 'pa_p3d_model' , false );
			wp_set_object_terms( $post_id, 'all', 'pa_p3d_unit' , false );
			wp_set_object_terms( $post_id, 'all', 'pa_p3d_scale' , false );
			wp_set_object_terms( $post_id, 'all', 'pa_p3d_infill' , false );

			$attrs = array(
				'pa_p3d_printer'=>array(
					'name'=>'pa_p3d_printer',
					'value'=>'',
					'is_visible' => '0',
					'is_variation' => '1',
					'is_taxonomy' => '1'
				),
				'pa_p3d_material'=>array(
					'name'=>'pa_p3d_material',
					'value'=>'',
					'is_visible' => '0',
					'is_variation' => '1',
					'is_taxonomy' => '1'
				),
				'pa_p3d_coating'=>array(
					'name'=>'pa_p3d_coating',
					'value'=>'',
					'is_visible' => '0',
					'is_variation' => '1',
					'is_taxonomy' => '1'
				),

				'pa_p3d_model'=>array(
					'name'=>'pa_p3d_model',
					'value'=>'',
					'is_visible' => '0',
					'is_variation' => '1',
					'is_taxonomy' => '1'
				),
				'pa_p3d_unit'=>array(
					'name'=>'pa_p3d_unit',
					'value'=>'',
					'is_visible' => '0',
					'is_variation' => '1',
					'is_taxonomy' => '1'
				),
				'pa_p3d_scale'=>array(
					'name'=>'pa_p3d_scale',
					'value'=>'',
					'is_visible' => '0',
					'is_variation' => '1',
					'is_taxonomy' => '1'
				),

				'pa_p3d_infill'=>array(
					'name'=>'pa_p3d_infill',
					'value'=>'',
					'is_visible' => '0',
					'is_variation' => '1',
					'is_taxonomy' => '1'
				)
			);

			update_post_meta( $post_id, '_product_attributes', $attrs );
			update_post_meta( $post_id, '_price', '1' );
			update_post_meta( $post_id, '_visibility', 'visible' );
			update_post_meta( $post_id, '_stock_status', 'instock' );


			$new_post = array(
				'post_title'=> "Variation #".( $post_id+1 )." of $post_id",
				'post_name' => 'product-' . $post_id . '-variation',
				'post_status' => 'publish',
				'post_parent' => $post_id,
				'post_type' => 'product_variation',
				'guid'=>home_url() . '/?product_variation=product-' . $post_id . '-variation'
			);
			$variation_id = wp_insert_post( $new_post );

			update_post_meta( $post_id, '_min_regular_price_variation_id', $variation_id );
			update_post_meta( $post_id, '_max_regular_price_variation_id', $variation_id );
			update_post_meta( $post_id, '_min_price_variation_id', $variation_id );
			update_post_meta( $post_id, '_max_price_variation_id', $variation_id );

			update_post_meta( $post_id, '_min_variation_price', 1 );
			update_post_meta( $post_id, '_max_variation_price', 1 );
			update_post_meta( $post_id, '_min_variation_regular_price', 1 );
			update_post_meta( $post_id, '_max_variation_regular_price', 1 );

			update_post_meta( $variation_id, '_price', '1' );
			update_post_meta( $variation_id, '_regular_price', '1' );
			update_post_meta( $variation_id, '_stock_status', 'instock' );

			update_post_meta( $variation_id, 'attribute_pa_p3d_printer', '' );
			update_post_meta( $variation_id, 'attribute_pa_p3d_material', '' );
			update_post_meta( $variation_id, 'attribute_pa_p3d_coating', '' );
			update_post_meta( $variation_id, 'attribute_pa_p3d_model', '' );
			update_post_meta( $variation_id, 'attribute_pa_p3d_unit', '' );
			update_post_meta( $variation_id, 'attribute_pa_p3d_scale', '' );
			update_post_meta( $variation_id, 'attribute_pa_p3d_infill', '' );

			wp_set_object_terms( $variation_id, '1', 'pa_p3d_printer' , false );
			wp_set_object_terms( $variation_id, '1', 'pa_p3d_material' , false );
			wp_set_object_terms( $variation_id, '1', 'pa_p3d_coating' , false );
			wp_set_object_terms( $variation_id, '1', 'pa_p3d_model' , false );
			wp_set_object_terms( $variation_id, '1', 'pa_p3d_unit' , false );
			wp_set_object_terms( $variation_id, '1', 'pa_p3d_scale' , false );
			wp_set_object_terms( $variation_id, '1', 'pa_p3d_infill' , false );

			wp_update_post( array( 'ID'=>$post_id, 'post_status'=>'publish' ) );
		}
		else if ( p3d_is_p3d( $post_id ) ) {
				delete_post_meta( $post_id, '_3dprinting' );
				p3d_delete_p3d( $post_id );
			}
	}

}
add_action( 'save_post', 'p3d_save_post' );

add_action('woocommerce_variation_options', 'p3d_variation_options', 10, 3);
function p3d_variation_options($loop, $variation_data, $variation) {

	$product_id = $variation->post_parent;
	if (p3d_is_p3d($product_id)) {
		$p3d_file_url = get_post_meta($product_id, 'p3d_file_url', true); 
		$p3d_product_price_type = get_post_meta($product_id, 'p3d_product_price_type', true);
		$p3d_product_display_mode = get_post_meta($product_id, 'p3d_product_display_mode', true);
		$p3d_product_pricing = get_post_meta($product_id, 'p3d_product_pricing', true);


?>
		<br>
		<table>	
			<tr>
				<td><?php _e( 'Predefined Model', '3dprint' ); ?>:</td>
				<td class="file_url"><input type="text" class="input_text" name="p3d_file_url" id="p3d_file_url" placeholder="<?php esc_attr_e( 'STL, OBJ or ZIP file', '3dprint' ); ?>" value="<?php echo esc_attr( $p3d_file_url ); ?>" /></td>
				<td class="file_url_choose" width="1%"><a href="#" class="button upload_file_button" data-choose="<?php esc_attr_e( 'Choose file', '3dprint' ); ?>" data-update="<?php esc_attr_e( 'Insert file URL', '3dprint' ); ?>"><?php echo str_replace( ' ', '&nbsp;', __( 'Choose file', '3dprint' ) ); ?></a></td>
				<td width="1%"><a href="javascript:void(0);" onclick="jQuery('#p3d_file_url').val('')" class="button"><?php _e( 'Delete', '3dprint' ); ?></a></td>
			</tr>
			<tr>
				<td>
					<?php _e( 'Predefined Model Price', '3dprint' ); ?>: 
				</td>
				<td>
					<select name="p3d_product_price_type" style="width:200px !important;" autocomplete="off">
						<option <?php if ($p3d_product_price_type=='dynamic') echo 'selected'; ?> value="dynamic"><?php _e( 'Dynamic', '3dprint' ); ?></option>
						<option <?php if ($p3d_product_price_type=='fixed') echo 'selected'; ?> value="fixed"><?php _e( 'Fixed', '3dprint' ); ?></option>
					</select>
					<?php 
					$tooltip['desc_tip']=true;
					$tooltip['description']=__('Dynamic price is calculated by 3DPrint plugin. Fixed price is set in the "Regular Price" field.', '3dprint');
					echo WC_Settings_API::get_tooltip_html($tooltip);
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php _e( 'Display Mode', '3dprint' ); ?>: 
				</td>
				<td>
					<select name="p3d_product_display_mode" style="width:200px !important;" autocomplete="off">
								<option <?php if ( $p3d_product_display_mode=='' ) echo 'selected';?> value=""><?php _e( 'Default', '3dprint' );?></option>
								<option <?php if ( $p3d_product_display_mode=='on_page' ) echo 'selected';?> value="on_page"><?php _e( 'On page', '3dprint' );?></option>
								<option <?php if ( $p3d_product_display_mode=='fullscreen' ) echo 'selected';?> value="fullscreen"><?php _e( 'Fullscreen', '3dprint' );?></option>
					</select>
					<?php 
					$tooltip['desc_tip']=true;
					$tooltip['description']=__('Overrides the global setting.', '3dprint');
					echo WC_Settings_API::get_tooltip_html($tooltip);
					?>
				</td>
			</tr>
			<tr>
				<td>
					<?php _e( 'Checkout', '3dprint' ); ?>: 
				</td>

				<td>
					<select name="p3d_product_pricing" style="width:200px !important;" autocomplete="off">
								<option <?php if ( $p3d_product_pricing=='' ) echo 'selected';?> value=""><?php _e( 'Default', '3dprint' );?></option>
								<option <?php if ( $p3d_product_pricing=='checkout' ) echo 'selected';?> value="checkout"><?php _e( 'Calculate price and add to cart', '3dprint' );?></option>
								<option <?php if ( $p3d_product_pricing=='request_estimate' ) echo 'selected';?> value="request_estimate"><?php _e( 'Give an estimate and request price', '3dprint' );?></option>
								<option <?php if ( $p3d_product_pricing=='request' ) echo 'selected';?> value="request"><?php _e( 'Request price', '3dprint' );?></option>

					</select>
					<?php 
					$tooltip['desc_tip']=true;
					$tooltip['description']=__('Overrides the global setting.', '3dprint');
					echo WC_Settings_API::get_tooltip_html($tooltip);
					?>
				</td>

			</tr>
		</table>
<?php
	}
}
add_action('woocommerce_ajax_save_product_variations', 'p3d_save_product_variations');
function p3d_save_product_variations($product_id) {
	update_post_meta( $product_id, 'p3d_file_url', $_POST['p3d_file_url'] );
	update_post_meta( $product_id, 'p3d_product_price_type', $_POST['p3d_product_price_type'] );
}



function p3d_enable_extended_upload ( $mime_types =array() ) {
	$mime_types['stl']  = 'application/octet-stream';
	$mime_types['obj'] = 'application/octet-stream';
	$mime_types['zip']  = 'application/zip';

	return $mime_types;
}
 
add_filter('upload_mimes', 'p3d_enable_extended_upload');



function p3d_unassigned_warning() {
	$class = 'notice notice-error is-dismissible';

	$unassigned_materials = p3d_get_unassigned_materials(p3d_get_option('3dp_printers'), p3d_get_option('3dp_materials'));
	if (count($unassigned_materials) > 0) {
		$message = sprintf(__( 'You have %s unassigned materials. They will not be displayed on the frontend.', '3dprint' ), count($unassigned_materials));
		printf( '<div class="%1$s"><b>3DPrint</b><p>%2$s</p></div>', $class, $message ); 
	}
}
add_action( 'admin_notices', 'p3d_unassigned_warning' );

function p3d_get_unassigned_materials($db_printers, $db_materials) {
	$assigned_materials = array();
	if (count($db_printers)==0) return;
	foreach ($db_printers as $printer) {
		if ($printer['materials']=='') {
			return array(); //all assigned
		}

		$assigned_materials = array_merge($assigned_materials, explode(',', $printer['materials']));
		
	}

	$unassigned_materials = array_diff(array_keys($db_materials), $assigned_materials );
	return $unassigned_materials;
}

function p3d_get_assigned_materials($db_printers, $db_materials) {
	$assigned_materials = array();
	if (count($db_printers)==0) return;
	foreach ($db_printers as $printer) {
		if ($printer['materials']=='') {
			return array_keys($db_materials); //all assigned
		}

		$assigned_materials = array_merge($assigned_materials, explode(',', $printer['materials']));
		
	}

	return $assigned_materials;
}




function p3d_clear_cookies() {
	if ( count( $_COOKIE ) ) {
		foreach ( $_COOKIE as $key=>$value ) {
			if ( strpos( $key, 'p3d' )===0 ) {
				setcookie( $key, "", time()-3600*24*30 );
			}
		}
	}
}


function p3d_init() {
	global $woocommerce_loop, $woocommerce;
	WC()->session->set_customer_session_cookie( true );
	return true;
}

if (isset($_POST['action']) && $_POST['action'] == 'editedtag') {
$p3d_attr_prices = get_option('3dp_attr_prices');
	if (isset($_POST['p3d_attr_prices']) && count($_POST['p3d_attr_prices'])>0) {
		foreach($_POST['p3d_attr_prices'] as $taxonomy_name => $taxonomy_data) {
			$p3d_attr_prices[$taxonomy_name][$_POST['slug']]['price'] = $taxonomy_data['price'];
			$p3d_attr_prices[$taxonomy_name][$_POST['slug']]['price_type'] = $taxonomy_data['price_type'];
			$p3d_attr_prices[$taxonomy_name][$_POST['slug']]['pct_type'] = $taxonomy_data['pct_type'];
		}
		update_option('3dp_attr_prices', $p3d_attr_prices);
	}
}


add_action( 'edit_tag_form_fields', 'p3d_edit_tag_form_fields' );
function p3d_edit_tag_form_fields($tag) {
	if ($_GET['post_type']=='product') {
		$p3d_attr_prices=get_option('3dp_attr_prices');
?>
		<tr class="form-field term-slug-wrap">
			<th scope="row"><label><?php _e('Price mod.' ,'3dprint');?></label></th>
			<td>
				<input type="text" style="width:100px;" size="4" value="<?php echo ( isset($p3d_attr_prices[$_GET['taxonomy']][$tag->slug]) ? $p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['price'] : "" ) ;?>" name="p3d_attr_prices[<?php echo $_GET['taxonomy'];?>][price]">
				<select onchange="if(this.value=='pct') jQuery('#pct_type').css('visibility', 'visible'); else jQuery('#pct_type').css('visibility', 'hidden');" name="p3d_attr_prices[<?php echo $_GET['taxonomy'];?>][price_type]">
					<option <?php if (isset($p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['price_type']) && $p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['price_type'] == 'flat') echo 'selected';?> value="flat"><?php echo get_woocommerce_currency_symbol();?>
					<option <?php if (isset($p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['price_type']) && $p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['price_type'] == 'pct') echo 'selected';?> value="pct">%
				</select> 
				<div id="pct_type" style="margin-left:10px;display:inline;<?php if ($p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['price_type'] != 'pct') echo 'visibility:hidden;';  ?>">
					<?php _e('of', '3dprint');?>
					<select name="p3d_attr_prices[<?php echo $_GET['taxonomy'];?>][pct_type]">
						<option <?php if (isset($p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['pct_type']) && $p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['pct_type'] == 'total') echo 'selected';?> value="total"><?php _e('Total Price');?>
						<option <?php if (isset($p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['pct_type']) && $p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['pct_type'] == 'printer') echo 'selected';?> value="printer"><?php _e('Printer Price');?>
						<option <?php if (isset($p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['pct_type']) && $p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['pct_type'] == 'material') echo 'selected';?> value="material"><?php _e('Material Price');?>
						<option <?php if (isset($p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['pct_type']) && $p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['pct_type'] == 'coating') echo 'selected';?> value="coating"><?php _e('Coating Price');?>
						<option <?php if (isset($p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['pct_type']) && $p3d_attr_prices[$_GET['taxonomy']][$tag->slug]['pct_type'] == 'material_amount') echo 'selected';?> value="material_amount"><?php _e('Material Amount');?>
					</select>
				</div>
			</td>
		</tr>

<?php	
	}
}


add_action( 'admin_enqueue_scripts', 'p3d_add_color_picker' );
function p3d_add_color_picker( $hook ) {

	if ( is_admin() && isset($_GET['page']) && $_GET['page']=='3dprint') {
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'custom-script-handle', plugins_url( 'js/3dprint-backend.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
	}
}

function p3d_sort_by_order($a, $b) {
	return $a['sort_order'] - $b['sort_order'];
}

function p3d_get_enabled ($array) {

	if (count((array)$array)) {

		foreach($array as $key=>$value) {
			if ($value['status']=='0') unset($array[$key]);
		}
	}
	return (array)$array;
}
function p3d_sort_by_group_order ($array) {
	$sort = array();
	foreach($array as $key=>$value) {
		$sort['group_name'][$key] = $value['group_name'];
		$sort['sort_order'][$key] = $value['sort_order'];
	}

	array_multisort($sort['group_name'], SORT_ASC, $sort['sort_order'], SORT_ASC, $array);
	return $array;
}


function p3d_upload_file($name, $index) {
	$uploadedfile = array(
		'name'     => $_FILES[$name]['name'][$index],
		'type'     => $_FILES[$name]['type'][$index],
		'tmp_name' => $_FILES[$name]['tmp_name'][$index],
		'error'    => $_FILES[$name]['error'][$index],
		'size'     => $_FILES[$name]['size'][$index]
	);

	$upload_overrides = array( 'test_form' => false );
	$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
	if (isset( $movefile['error'])) echo $movefile['error'];
	return $movefile;
}



add_filter( 'woocommerce_single_product_image_html', 'p3d_woocommerce_single_product_image_html', 99, 2 );
function p3d_woocommerce_single_product_image_html ($image_url, $post_id) {
	global $post, $woocommerce, $product;

	if (!p3d_is_p3d(get_the_ID())) return $image_url;

	$settings=get_option('3dp_settings');

	$p3d_file_url_meta = get_post_meta($product->get_id(), 'p3d_file_url'); $p3d_file_url = $p3d_file_url_meta[0];
	$p3d_product_price_type_meta = get_post_meta($product->get_id(), 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];

	$fixed_price = false;
	if (strlen($p3d_file_url)>0 && $p3d_product_price_type=='fixed') $fixed_price = true;
	ob_start(); 
    ?>
	<div id="prompt">
	  <!-- if IE without GCF, prompt goes here -->
	</div>

    <div id="gcode-viewer">
        <div id="analysisModal" class="modal hide fade">
            <div class="modal-header">
                <h3>Running analysis....</h3>
            </div>
            <div class="modal-body">
                <div class="progress" >
                    <div id="analysisProgress" class="bar" style="width: 0;"></div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn disabled">Please wait for process to get finished</a>
            </div>
        </div>

        <div class="navbar navbar-static-top">
            <div class="navbar-inner">
                <div class="container">
                    <ul class="nav">
                        <li >
                            <a class="brand" href="#">GCode Viewer</a>
                        </li>
                    </ul>
                    <ul class="nav pull-right">
                        <li class="dropdown">
                            <a href="#"
                               class="dropdown-toggle"
                               data-toggle="dropdown">
                                Spread the word
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="socials">
                                    <div class="g-plusone" data-annotation="inline" data-width="150"></div>
                                </li>
                                <li class="socials"><div class="fb-like" data-href="http://gcode.ws" data-send="false" data-layout="button_count" data-width="150" data-show-faces="true" data-action="recommend"></div></li>
                                <li class="socials"><a href="https://twitter.com/share" class="twitter-share-button" data-url="http://gcode.ws" data-text="Check out a nice online gcode viewer!">Tweet</a>
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>


        <div id="wrap" class="ui-widget ui-widget-content ui-corner-all">
            <div id="control">
                <div id="accordion2" class="accordion">
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#fileAccordionTab">
                                Select GCode file
                            </a>
                        </div>
                        <div id="fileAccordionTab" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <input type="file" id="file" name="files">
                                <!--<form class="add-teacher" id="fAddTeacher" enctype="multipart/form-data" method="post" novalidate="novalidate">-->
                                <!--<button id="selectFileButton">Select GCode File</button>-->
                                <!--</form>-->
                                <div id="drop_zone">Drop file here</div>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#progressAccordionTab">
                                Progress indicators
                            </a>
                        </div>
                        <div id="progressAccordionTab" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <div id="progressBlock">
                                    <div class="progress" >
                                        <div id="loadProgress" class="bar" style="width: 0;"></div>
                                    </div>
                                    <div class="progress" >
                                        <div id="analyzeProgress" class="bar" style="width: 0;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    </div>

                    <div class="accordion-group hide" id="errAnalyseTab">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#errorAnalysisAccordioinTab">
                                Error Analysis
                            </a>
                        </div>
                        <div id="errorAnalysisAccordioinTab" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <button class="btn disabled" id="runAnalysisButton" onclick="GCODE.analyzer.runAnalyze()">Run analysis</button>
                                <div id="analysisOptionsDiv" class="hide">
                                    <label for="renderErrors"><input type="checkbox" id="renderErrors" onclick="GCODE.ui.processOptions()">Render error analysis results</label>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <output id="errorList"></output>


            </div>
            <div id="gcode">
                <div id="tabs-min" class="tabbable">
                    <ul id="myTab" class="nav nav-tabs">
                        <li class=""><a href="#tab2d" data-toggle="tab">2D</a></li>
                        <li class="active"><a href="#tab3d" data-toggle="tab">3D</a></li>
                        <li class=""><a href="#tabGCode" data-toggle="tab">GCode</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab2d">
                            <canvas id="canvas" width="650" height="620"></canvas>
                            <div id="slider-vertical"></div>
                            <div id="slider-horizontal"></div>
                        </div>
                        <div class="tab-pane" id="tab3d">
                            <div id="3d_container"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            GCODE.ui.initHandlers();
        </script>

        <script type="text/javascript">
            (function() {
                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                po.src = 'https://apis.google.com/js/plusone.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();

        </script>
    </div>

	<div id="p3d-viewer" <?php if (strlen($p3d_file_url)>0) echo 'data-fixed="1"'; else echo 'data-fixed="0"'; ?>>

		<canvas id="p3d-cv" width="<?php echo $settings['canvas_width'];?>" height="<?php echo $settings['canvas_height'];?>" style="border: 1px solid;"></canvas>
		<div id="p3d-sidepanel-left" class="{title:'<?php _e('Options', '3dprint');?>'}"></div>
		<div id="p3d-sidepanel-top" class="{title:'<?php _e('Scale', '3dprint');?>'}"></div>
		<div id="p3d-sidepanel-right" class="{title:'<?php _e('Details', '3dprint');?>'}"></div>
		<div id="canvas-stats">
			<div id="p3d-canvas-uploading-status" style="display:none;">
				<div style="<?php if ($fixed_price) echo 'display:none;';?>" id="p3d-filelist"></div>
			</div>
			<p id="p3d-canvas-repair-status" style="display:none;">
				<img id="p3d-canvas-repair-image" alt="Repairing" src="<?php echo plugins_url( '3dprint/images/ajax-loader-small.gif'); ?>">
				<span id="p3d-canvas-repair-message"></span>
			</p>
			<p id="p3d-canvas-process-status" style="display:none;">
				<img id="p3d-canvas-process-image" alt="Repairing" src="<?php echo plugins_url( '3dprint/images/ajax-loader-small.gif'); ?>">
				<span id="p3d-canvas-process-message"></span>
			</p>

			<p id="p3d-canvas-analyse-status" style="display:none;">
				<img id="p3d-canvas-analyse-image" alt="Analysing" src="<?php echo plugins_url( '3dprint/images/ajax-loader-small.gif'); ?>">
			  	<span id="p3d-canvas-analyse-message"></span><span id="p3d-canvas-analyse-percent"></span>
			</p>
			
		</div>
		<div id="p3d-file-loading">
			<img alt="Loading file" src="<?php echo $settings['ajax_loader']; ?>">
		</div>
		<div id="p3d-model-message">
			<p class="p3d-model-message" id="p3d-model-message-upload" style="<?php if (strlen($p3d_file_url)>0) echo 'display:none;';?>">
				<img alt="Upload" id="p3d-model-message-upload-icon" src="<?php echo plugins_url( '3dprint/images/upload45.png'); ?>">
<?php 
				if (preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT']) || preg_match('/Trident/', $_SERVER['HTTP_USER_AGENT'])) { //screw ie
?>
				<?php _e("Click here to upload.", '3dprint');?>
<?php
				} else {
?>
				<?php _e("Click here to upload or drag and drop your model to the canvas.", '3dprint');?>
<?php
				}
?>
			</p>
			<p class="p3d-model-message" id="p3d-model-message-scale"><?php _e("The model is too large and has been resized to fit in the printer's build tray.", '3dprint');?>&nbsp;<span style="cursor:pointer;" onclick='jQuery(this).parent().hide(); return false;'><?php _e('[Hide]', '3dprint');?></span></p>
			<p class="p3d-model-message" id="p3d-model-message-minside"><?php _e("The model is too small and has been upscaled.", '3dprint');?>&nbsp;<span style="cursor:pointer;" onclick='jQuery(this).parent().hide(); return false;'><?php _e('[Hide]', '3dprint');?></span></p>
			<p class="p3d-model-message" id="p3d-model-message-fullcolor"><?php _e( 'Warning: The selected printer can not print in full color', '3dprint' );?>&nbsp;<span style="cursor:pointer;" onclick='jQuery(this).parent().hide(); return false;'><?php _e('[Hide]', '3dprint');?></span></p>
			<p class="p3d-model-message" id="p3d-model-message-multiobj"><?php _e( 'Warning: obj models with multiple meshes are not yet supported', '3dprint' );?>&nbsp;<span style="cursor:pointer;" onclick='jQuery(this).parent().hide(); return false;'><?php _e('[Hide]', '3dprint');?></span></p>
			<p class="p3d-model-message" id="p3d-model-message-arrange"><?php _e( 'Warning: could not arrange <span id="p3d-failed-files-count"></span> models', '3dprint' );?>&nbsp;<span style="cursor:pointer;" onclick='jQuery(this).parent().hide(); return false;'><?php _e('[Hide]', '3dprint');?></span></p>

		</div>
	</div>

	<br style="clear:both;">

	<div style="<?php if ($settings['show_upload_button']!='on') echo 'display:none;';?>" id="p3d-container" onclick="p3dDialogCheck()">
 
<?php

		if (preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT']) || preg_match('/Trident/', $_SERVER['HTTP_USER_AGENT'])) {
?>

		<button id="p3d-pickfiles" style="background-color:<?php echo $settings['button_color1']?>;<?php if (strlen($p3d_file_url)>0) echo 'display:none;';?>" class="progress-button" ><?php _e('Upload Model', '3dprint'); ?></button>
<?php
		}
		else {
?>
		<button id="p3d-pickfiles" style="<?php if (strlen($p3d_file_url)>0) echo 'display:none;';?>" class="progress-button" data-style="rotate-angle-bottom" data-perspective data-horizontal><?php _e('Upload Model', '3dprint'); ?></button>
<?php
		}
?>
	</div>
<?php

	$image_url = ob_get_contents();
	ob_end_clean();

	return $image_url;	
}



add_action('woocommerce_product_thumbnails', 'p3d_product_thumbnails', 99);
function p3d_product_thumbnails() {

	global $post, $woocommerce, $product;
	if (!p3d_is_p3d($product->get_id())) return;
	$settings=get_option('3dp_settings');
	$p3d_file_url_meta = get_post_meta($product->get_id(), 'p3d_file_url'); $p3d_file_url = $p3d_file_url_meta[0];
	$p3d_product_price_type_meta = get_post_meta($product->get_id(), 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];
	$fixed_price = false;
	if (strlen($p3d_file_url)>0 && $p3d_product_price_type=='fixed') $fixed_price = true;


?>
	<div class="p3d-info p3d-panel-top" style="<?php if ($settings['show_unit']!='on' || $fixed_price) echo 'display:none;';?>">
	<?php _e('File Unit:', '3dprint');?>
		&nbsp;&nbsp;
		<input class="p3d-control" autocomplete="off" id="unit_mm" onclick="p3dSelectUnit(this);" type="radio" name="p3d_unit" value="mm">
		<label class="p3d-unit-label" for="unit_mm"><?php _e('mm', '3dprint');?></label>
<!--		<span style="cursor:pointer;" onclick="p3dSelectUnit(jQuery('#unit_mm'));"><?php _e('mm', '3dprint');?></span>-->
		&nbsp;&nbsp;
		<input class="p3d-control" autocomplete="off" id="unit_inch" onclick="p3dSelectUnit(this);" type="radio" name="p3d_unit" value="inch">
<!--		<span style="cursor:pointer;" onclick="p3dSelectUnit(jQuery('#unit_inch'));"><?php _e('inch', '3dprint');?></span>-->
		<label class="p3d-unit-label" for="unit_inch"><?php _e('inch', '3dprint');?></label>
	</div>
	<div class="p3d-info p3d-panel-top" style="white-space:nowrap;<?php if ($settings['show_scale']!='on' || $fixed_price) echo 'display:none;';?>">
		<div id="p3d-scale-text">
			<?php _e("Scale:", "3dprint"); ?>   
		</div>
		<div id="p3d-scale-slider">
			<div id="p3d-scale" class="noUiSlider"></div>
		</div>
		<div id="p3d-scale-input">
			<input id="p3d-slider-range-value" class="p3d-dim-input" type="number" min="0" autocomplete="off" onchange="p3dUpdateSliderValue(this.value)"> %
		</div>
	
	</div>
	<div class="p3d-info p3d-panel-top" style="white-space:nowrap;<?php if ($settings['show_scale']!='on' || $fixed_price) echo 'display:none;';?>">
		<div id="p3d-dimensions-text">
			<?php _e("L &times; W &times; H:", "3dprint"); ?>
		</div>
		<div id="p3d-scale-dimensions">
			<span style="<?php if ($settings['show_axis']!='on') echo 'display:none;';?>" class="p3d-x-axis"><?php _e('X','3dprint');?>:</span>
			<input type="number" autocomplete="off" class="p3d-dim-input" min="0" step="0.1" value="0" id="scale_y" onchange="p3dUpdateDimensions(this);"> <span class="p3d-separator"> &times;</span> 
			<span style="<?php if ($settings['show_axis']!='on') echo 'display:none;';?>" class="p3d-y-axis"><?php _e('Y','3dprint');?>:</span>
			<input type="number" autocomplete="off" class="p3d-dim-input" min="0" step="0.1" value="0" id="scale_x" onchange="p3dUpdateDimensions(this);"> <span class="p3d-separator">&times; </span> 
			<span style="<?php if ($settings['show_axis']!='on') echo 'display:none;';?>" class="p3d-z-axis"><?php _e('Z','3dprint');?>:</span>
			<input type="number" autocomplete="off" class="p3d-dim-input" min="0" step="0.1" value="0" id="scale_z" onchange="p3dUpdateDimensions(this);"><span class="p3d-separator">&nbsp;</span><?php _e("cm", "3dprint"); ?>&nbsp;
			<input type="image" onclick="p3dScaleIndependently(true);" style="<?php if ($settings['scale_xyz']!='on') echo 'display:none;'; ?>" id="p3d-locked-image" title="<?php _e("Keep Proportions", '3dprint'); ?>" alt="<?php _e("Keep Proportions", '3dprint'); ?>"  src="<?php echo plugins_url( '3dprint/images/lock.png'); ?>">
			<input type="image" onclick="p3dScaleIndependently(false);" id="p3d-unlocked-image" title="<?php _e("Scale Independently", '3dprint'); ?>" alt="<?php _e("Scale Independently", '3dprint'); ?>" src="<?php echo plugins_url( '3dprint/images/unlock.png'); ?>">
<!--
			<img onclick="p3dScaleIndependently(true);" style="<?php if ($settings['scale_xyz']!='on') echo 'display:none;'; ?>" id="p3d-locked-image" title="<?php _e("Keep Proportions", '3dprint'); ?>" alt="<?php _e("Keep Proportions", '3dprint'); ?>"  src="<?php echo plugins_url( '3dprint/images/lock.png'); ?>">
			<img onclick="p3dScaleIndependently(false);" id="p3d-unlocked-image" title="<?php _e("Scale Independently", '3dprint'); ?>" alt="<?php _e("Scale Independently", '3dprint'); ?>" src="<?php echo plugins_url( '3dprint/images/unlock.png'); ?>">
-->
			
		</div>
	</div>

	<div class="p3d-info p3d-panel-top" style="white-space:nowrap;<?php if ($settings['show_rotation']!='on' || $fixed_price) echo 'display:none;';?>">
		<div id="p3d-rotation-text">
			<?php _e("Rotation:", "3dprint"); ?>
		</div>
		<div id="p3d-rotation-dimensions">
			<span class="p3d-x-axis"><?php _e('X','3dprint');?>:</span>
			<input type="number" autocomplete="off" class="p3d-dim-input" min="-360" max="360" step="90" value="0" id="rotation_x"> <span class="p3d-separator">&deg; </span> 
			<span class="p3d-y-axis"><?php _e('Y','3dprint');?>:</span>
			<input type="number" autocomplete="off" class="p3d-dim-input" min="-360" max="360" step="90" value="0" id="rotation_y"> <span class="p3d-separator">&deg; </span>
			<span style="visibility:hidden;" class="p3d-z-axis"><?php _e('Z','3dprint');?>:</span>
			<input style="display:none;" type="number" autocomplete="off" class="p3d-dim-input" min="-360" max="360" step="90" value="0" id="rotation_z"><span style="display:none;" class="p3d-separator">&deg; </span>
		<?php if ($settings['show_rotation']=='on') { ?>
			<button onclick="p3dProcessModel(jQuery('#pa_p3d_model').val());" id="p3d-apply-button" type="button"><?php _e('Apply', '3dprint');?></button> &nbsp;
		<?php } ?>
		</div>
	</div>

	<?php if ($settings['show_rotation']!='on' && $settings['show_scale']=='on' && $settings['scale_xyz']=='on') { ?>
	<div class="p3d-info p3d-panel-top">
		<button onclick="p3dProcessModel(jQuery('#pa_p3d_model').val());" id="p3d-apply-button" type="button"><?php _e('Apply', '3dprint');?></button> &nbsp;
	</div>
	<?php } ?>


	<div class="p3d-info" id="p3d-image-form" style="display:none;">
		<input type="radio" name="p3d-image-map" value="1" checked ><?php _e('Dark is higher', '3dprint');?><br>
		<input type="radio" name="p3d-image-map" value="2"><?php _e('Bright is higher', '3dprint');?><br>

		<input type="number" name="p3d-image-height" value="5"><?php _e('Height (mm)', '3dprint');?><br>
		<button onclick="p3d.image_settings_selected=true;">Save</button>

	</div>
	<div class="p3d-info p3d-panel-right">
		<pre id="p3d-console"></pre>
	</div>

	<div class="p3d-info p3d-panel-right">
	  	<span id="p3d-error-message" class="error"></span>
	</div>
	<div class="p3d-info p3d-panel-right" id="p3d-repair-status" style="display:none;">
		<img id="p3d-repair-image" alt="Repairing" src="<?php echo plugins_url( '3dprint/images/ajax-loader-small.gif'); ?>">
	  	<span id="p3d-repair-message"></span>
	</div>
	<div class="p3d-info p3d-panel-right" id="p3d-process-status" style="display:none;">
		<img id="p3d-process-image" alt="Repairing" src="<?php echo plugins_url( '3dprint/images/ajax-loader-small.gif'); ?>">
	  	<span id="p3d-process-message"></span>
	</div>

	<div class="p3d-info p3d-panel-right" id="p3d-analyse-status" style="display:none;">
		<img id="p3d-analyse-image" alt="Analysing" src="<?php echo plugins_url( '3dprint/images/ajax-loader-small.gif'); ?>">
	  	<span id="p3d-analyse-message"></span><span id="p3d-analyse-percent"></span>
	</div>

	<div class="p3d-info p3d-panel-right" style="<?php if ($settings['show_model_stats']!='on') echo 'display:none;';?>">
		<p><b><?php _e('Model Stats', '3dprint');?>:</b></p>
		<table class="p3d-stats">
			<tr style="<?php if ($settings['show_model_stats_material_volume']!='on') echo 'display:none;';?>">
				<td>
					<?php _e('Material Volume', '3dprint');?>:
				</td>
				<td>
					<img id="stats-material-volume-loading" src="<?php echo plugins_url( '3dprint/images/ajax-loader-small.gif'); ?>">
					<span id="stats-material-volume"></span> <?php _e('cm3', '3dprint');?>
				</td>
			</tr>
			<tr style="<?php if ($settings['show_model_stats_box_volume']!='on') echo 'display:none;';?>">
				<td>
					<?php _e('Box Volume', '3dprint');?>:
				</td>
				<td>
					<span id="stats-box-volume"></span> <?php _e('cm3', '3dprint');?>
				</td>
			</tr>
			<tr style="<?php if ($settings['show_model_stats_surface_area']!='on') echo 'display:none;';?>">
				<td>
					<?php _e('Surface Area', '3dprint');?>:
				</td>
				<td>
					<span id="stats-surface-area"></span> <?php _e('cm2', '3dprint');?>
				</td>
			</tr>
			<tr style="<?php if ($settings['show_model_stats_model_weight']!='on') echo 'display:none;';?>">
				<td>
					<?php _e('Model Weight', '3dprint');?>:
				</td>
				<td>
					<img id="stats-material-weight-loading" src="<?php echo plugins_url( '3dprint/images/ajax-loader-small.gif'); ?>">
					<span id="stats-weight"></span> <?php _e('g', '3dprint');?>
				</td>
			</tr>
			<tr style="<?php if ($settings['show_model_stats_model_dimensions']!='on') echo 'display:none;';?>">
				<td>
					<?php _e('Model Dimensions', '3dprint');?>:
				</td>
				<td>
					<span id="stats-length"></span> x <span id="stats-width"></span> x <span id="stats-height"></span>
					<?php _e('cm', '3dprint');?>
				</td>
			</tr>
			<tr id="stats-print-time" style="<?php if ($settings['show_model_stats_model_hours']!='on') echo 'display:none;';?>">
				<td>
					<?php _e('Print Time', '3dprint');?>:
				</td>
				<td>
					<img id="stats-hours-loading" src="<?php echo plugins_url( '3dprint/images/ajax-loader-small.gif'); ?>">
					<span id="stats-hours"></span>

					<?php _e('hours', '3dprint');?>
				</td>
			</tr>

		</table>
	</div>
<?php
}


#return apply_filters( 'woocommerce_get_price_html', $price, $this );
add_filter( 'woocommerce_get_price_html', 'p3d_get_price_html', 10, 2 );
function p3d_get_price_html( $price, $product ) {
	$settings=get_option( '3dp_settings' );
	$orig_price = $price;

	if (p3d_is_p3d($product->get_id())) {
		$p3d_product_pricing = get_post_meta($product->get_id(), 'p3d_product_pricing', true);
		if ($p3d_product_pricing!='') {
			$settings['pricing']=$p3d_product_pricing;
		}
		if ( $settings['pricing']=='request_estimate') $price .= __( '<b>Estimated Price:</b>', '3dprint' );
		if (empty($orig_price)) $price .= '<span class="amount"></span>';
	}

	return $price;
}

add_filter( 'wc_get_template', 'p3d_get_template', 99, 2 );
function p3d_get_template( $located, $template_name ) {
#	$p3d_templates=array( 'single-product/price.php', 'single-product/product-image.php', 'single-product/add-to-cart/variable.php' );
	$p3d_templates=array( 'content-single-product.php', 'single-product/price.php', 'single-product/product-image.php', 'single-product/add-to-cart/variable.php', 'order/order-again.php' );

	if ( p3d_is_p3d( get_queried_object_id() ) || p3d_is_p3d( get_the_ID() ) ) {
		if ( in_array( $template_name, $p3d_templates ) ) {
			$p3d_dir = p3d_plugin_path();
			$located = $p3d_dir."/woocommerce/$template_name";
		}
	}
	return $located;
}
add_filter( 'woocommerce_locate_template', 'p3d_woocommerce_locate_template', 99, 3 );
function p3d_woocommerce_locate_template( $template, $template_name, $template_path ) {
	$_template = $template;

	if ( p3d_is_p3d( get_queried_object_id() ) || p3d_is_p3d( get_the_ID() ) || $template_name == 'order/order-again.php') {

		if ( ! $template_path ) $template_path = $woocommerce->template_url;
		$plugin_path  = p3d_plugin_path() . '/woocommerce/';

		$template = locate_template(
			array(
				$template_path . $template_name,
				$template_name
			)
		);

		if ( ! $template && file_exists( $plugin_path . $template_name ) )
			$template = $plugin_path . $template_name;
	}
	else {

	}

	if ( ! $template )
		$template = $_template;

	return $template;
}

function p3d_plugin_path() {
	return untrailingslashit( dirname( plugin_dir_path( __FILE__ ) ) );
}


add_action( 'template_redirect', 'p3d_redirect' );
function p3d_redirect() {
	p3d_init();
	if ( isset( $_GET['p3d_buynow'] ) && $_GET['p3d_buynow']=='1' ) {
		foreach ( $_GET as $key=>$value ) {
			if ( strstr($key, 'attribute_pa') ) $variation[$key]=$value;
		}

		$product = new WC_Product_Variable( get_the_ID() );
		$available_variations=$product->get_available_variations();
		WC()->cart->add_to_cart( get_the_ID(), 1, $available_variations[0]['variation_id'], $variation );
		p3d_clear_cookies();

		wp_safe_redirect( WC()->cart->get_cart_url() );
		exit;
	}
	else if ( isset($_POST['action']) && $_POST['action']=='request_price' ) {
		p3d_clear_cookies();
	}
}

function p3d_save_thumbnail( $data, $filename ) {
	$link = '';
	if ( !empty($data) ) {
		$new_filename=$filename.'.png';
		$upload_dir = wp_upload_dir();
		$file_path=$upload_dir['basedir'].'/p3d/'.$new_filename;
		file_put_contents( $file_path, base64_decode( $data ) );
		$link = $upload_dir['baseurl'].'/p3d/'.$new_filename;
	}
	return $link;
}

//	$item_data = apply_filters( 'woocommerce_get_item_data', $item_data, $cart_item );
add_filter( 'woocommerce_get_item_data', 'p3d_get_item_data', 10, 2 );
function p3d_get_item_data($item_data, $cart_item) {
	if (isset($cart_item['3dp_options'])) {
		foreach ( $item_data as $key => $data ) {
//			if ($data['key']=='Model') {
			$extension = p3d_extension($data['value']);
			if ($extension=='stl' || $extension=='obj') {
				list ($starting_index,) = explode('_', $data['value']);
				$item_data[$key]['display']="$starting_index.$extension";
			}
//			}
		}
	}
	return $item_data;
}

add_filter( 'woocommerce_cart_item_name', 'p3d_woocommerce_cart_item_name', 10, 3 );
function p3d_woocommerce_cart_item_name($title, $cart_item, $cart_item_key) {
	global $woocommerce;
	$_product = $cart_item['data'];

	if( version_compare( $woocommerce->version, '2.6', "<" ) ) {
		$product_permalink = get_permalink( $cart_item['product_id'] );
	}
	else {
		$product_permalink = $_product->get_permalink( $cart_item );
	}
	if (isset($cart_item['3dp_options']) && is_array($cart_item['3dp_options']) && strlen($cart_item['3dp_options']['model_name'])>0) {
		//$original_model = p3d_get_original($cart_item['3dp_options']['model_name']);
#var_dump($cart_item['3dp_options']['model_name_original']);
//var_dump(WC()->session->get($cart_item['3dp_options']['model_name_original']));
#		if ($cart_item['3dp_options']['model_name_original']!='') {
		if ($cart_item['3dp_options']['model_name_original']!='') {
			$original_name = $cart_item['3dp_options']['model_name_original'];
		}
		else {
			list ($starting_index, ) = explode('_', $cart_item['3dp_options']['model_name']);
			$original_name = $starting_index.".".p3d_extension($cart_item['3dp_options']['model_name']);
		}

#		if (strlen($original_model)) {
			#$original_name = explode('_', esc_html(stripslashes(rawurldecode(p3d_basename($original_model)))), 2);
#			$title = sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), end($original_name) );
#		}
		if ($original_name) {
			$title = sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $original_name );
		}
	}
		

	return $title;
}

//Show the screenshot of the product
add_filter( 'woocommerce_cart_item_thumbnail', 'p3d_cart_item_thumbnail', 11, 3 );
function p3d_cart_item_thumbnail( $img, $cart_item, $cart_item_key ) {
#$cart_item_meta['3dp_options']['thumbnail']
#	if ( isset( $cart_item['3dp_options'] ) && is_array( $cart_item['3dp_options'] ) && !empty( $cart_item['3dp_options']['thumbnail_data'] ) ) {
#		$img = '<img src="data:image/png;base64,'.$cart_item['3dp_options']['thumbnail_data'].'" style="width:100px;">';
#	}
	if ( isset( $cart_item['3dp_options'] ) && is_array( $cart_item['3dp_options'] ) && !empty( $cart_item['3dp_options']['thumbnail'] ) ) {
		$img = '<img class="p3d-cart-thumbnail" src="'.$cart_item['3dp_options']['thumbnail'].'">';
	}

	return $img;
}

//price for mini-cart, etc
add_filter( 'woocommerce_cart_item_price', 'p3d_cart_item_price', 10, 3 );
function p3d_cart_item_price( $price, $cart_item, $cart_item_key ) {
	if ( isset( $cart_item['3dp_options'] ) && is_array( $cart_item['3dp_options'] ) && !empty( $cart_item['3dp_options']['product-price'] ) ) {
		$price=wc_price($cart_item['3dp_options']['product-price']);
	}

	return $price;
}


/*
add_filter( 'woocommerce_my_account_my_orders_actions', 'p3d_my_account_my_orders_actions', 50, 2 );
function p3d_my_account_my_orders_actions( $actions, $order ) {
	if ( $order->has_status( 'completed' ) ) {

$p3d_items=0;
$nonp3d_items=0;
$order_items = $order->get_items();
foreach ($order_items as $order_item) {
	if (p3d_is_p3d($order_item['product_id'])) {
		$p3d_items++;
		$permalink = get_permalink($order_item['product_id']);
	}
	else $nonp3d_items++;
}

		$actions['order-again'] = array(
			'url'  => wp_nonce_url( add_query_arg( 'order_again', $order->id ) , 'woocommerce-order_again' ),
			'name' => __( 'Order Again', 'woocommerce' )
		);
	}
	return $actions;
}
*/


add_action( 'woocommerce_before_calculate_totals', 'p3d_add_custom_price', 9999 );
function p3d_add_custom_price( $cart_object ) {
	global $woocommerce;
	$settings = get_option('3dp_settings');
	
	foreach ( $cart_object->cart_contents as $key => $value ) {
		if ( version_compare( $woocommerce->version, '3.0.0', '<' ) ) { 
			if ( isset( $value['3dp_options']['product-price'] ) ) $value['data']->price=$value['3dp_options']['product-price'];
		}
		else {
			if ( isset( $value['3dp_options']['product-price'] ) ) $value['data']->set_price($value['3dp_options']['product-price']);
		}
#print_r($value['data']);
		if ( isset( $value['3dp_options']['printer_name'] ) ) $cart_object->cart_contents[$key]['variation']['attribute_pa_p3d_printer']=$value['3dp_options']['printer_name'];
		if ( isset( $value['3dp_options']['infill'] ) ) $cart_object->cart_contents[$key]['variation']['attribute_pa_p3d_infill']=$value['3dp_options']['infill'];
		if ( isset( $value['3dp_options']['material_name'] ) ) $cart_object->cart_contents[$key]['variation']['attribute_pa_p3d_material']=$value['3dp_options']['material_name'];
		if ( isset( $value['3dp_options']['coating_name'] ) ) $cart_object->cart_contents[$key]['variation']['attribute_pa_p3d_coating']=$value['3dp_options']['coating_name'];
		if ( isset( $value['3dp_options']['model_name'] ) ) $cart_object->cart_contents[$key]['variation']['attribute_pa_p3d_model']=rawurlencode($value['3dp_options']['model_name']);
//			$original_name = WC()->session->get($value['3dp_options']['model_name']);
//			if ($original_name)
//		}

		if ( isset( $value['3dp_options']['unit'] ) ) $cart_object->cart_contents[$key]['variation']['attribute_pa_p3d_unit']=rawurlencode($value['3dp_options']['unit']);
//$md5_filename
		if ( isset( $value['3dp_options']['scale'] ) ) {
			$cart_object->cart_contents[$key]['variation']['attribute_pa_p3d_scale']='&times;'.rawurlencode($value['3dp_options']['scale']);
			if ( isset( $value['3dp_options']['length'] ) && is_numeric($value['3dp_options']['length'] ) ) {
				$cart_object->cart_contents[$key]['variation']['attribute_pa_p3d_scale'] .= ' ('.round($value['3dp_options']['length'], 2) . ' &times; ' . round($value['3dp_options']['width'], 2) . ' &times; ' . round($value['3dp_options']['height'], 2).' '.__('cm', '3dpint').')';
			}
		}
		if ( $settings['api_analyse'] !='on' || (isset($value['3dp_options']) && $value['3dp_options']['infill']) == '' && is_array($cart_object->cart_contents[$key]['variation'])) {
			if (isset($cart_object->cart_contents[$key]['variation']['attribute_pa_p3d_infill'])) {
				unset ($cart_object->cart_contents[$key]['variation']['attribute_pa_p3d_infill']);
			}
		}

	}
}

function p3d_process_attr($attr) {
	if ($attr!='all') $attr = (int)$attr;
	return $attr;
}

/**
 * Add 3DPrint weight and dimensions to WooCommerce cart item objects
 * @author mike@curren.me
 * modfied by fuzzoid
 * @requires WooCommerce plugin - https://woocommerce.com/
 * @requires Wordpress 3D Printing plugin - http://www.wp3dprinting.com/
 */

function add_3dp_stats_to_wc_cart_objects( $cart_object ) {

    if ( (is_admin() && ! defined( 'DOING_AJAX' ) ) || $cart_object->is_empty() )
        return;

    foreach ( $cart_object->get_cart() as $cart_item ) {
	if (!isset($cart_item['3dp_options'])) continue;
        /**
         * Set cart item weight
         */
	if (method_exists($cart_item['data'], 'set_weight'))
	        $cart_item['data']->set_weight( wc_get_weight($cart_item['3dp_options']['weight'], strtolower( get_option( 'woocommerce_weight_unit' ) )) );


        /**
         * Set cart item dimensions
         */
	if (method_exists($cart_item['data'], 'set_width'))
	        $cart_item['data']->set_width( wc_get_dimension($cart_item['3dp_options']['width'], strtolower( get_option( 'woocommerce_dimension_unit' ))));
	if (method_exists($cart_item['data'], 'set_length'))
	        $cart_item['data']->set_length( wc_get_dimension($cart_item['3dp_options']['length'], strtolower( get_option( 'woocommerce_dimension_unit' ))));
	if (method_exists($cart_item['data'], 'set_height'))
	        $cart_item['data']->set_height( wc_get_dimension($cart_item['3dp_options']['height'], strtolower( get_option( 'woocommerce_dimension_unit' ))));

    }

    /**
     * Debugging info 
     * NOTE: un-comment below to show total cart weight on WooCommerce Cart page
     */

//     echo '<pre>Cart weight: '; print_r( $cart_object->get_cart_contents_weight() ); echo '</pre><br>';
}
add_action( 'woocommerce_before_calculate_totals', 'add_3dp_stats_to_wc_cart_objects', 10, 1);

/*
add_filter( 'woocommerce_cart_contents_weight', 'p3d_cart_contents_weight', 10, 1 );
function p3d_cart_contents_weight($weight) {
	foreach( WC()->cart->get_cart() as $cart_item ) {
		if ($cart_item['3dp_options']['weight']>0) $weight += wc_get_weight($cart_item['3dp_options']['weight'] * $cart_item['quantity'], strtolower( get_option( 'woocommerce_weight_unit' ) ));
	}

	return $weight;
}
*/
/*
//purge shipping rate cache
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if (!is_plugin_active('dokan-pro/dokan-pro.php')) {
	add_action( 'woocommerce_before_calculate_totals', 'p3d_clear_wc_shipping_rates_cache' );
}
function p3d_clear_wc_shipping_rates_cache($data){
	$packages = WC()->cart->get_shipping_packages();

	foreach ($packages as $key => $value) {
		$shipping_session = "shipping_for_package_$key";
		unset(WC()->session->$shipping_session);
	}

}
*/

function p3d_generate_model_key($printer, $infill, $scale, $unit) {
//$cart_item_meta['3dp_options']['model_key']=$printer['layer_height']."_".$printer['wall_thickness']."_".$printer['bottom_top_thickness']."_".$printer['nozzle_size']."_".$infill."_".$scale."_".$unit."_".$printer['support']."_".$printer['support_type']."_".$printer['support_angle'];
return md5(serialize($printer)).$infill.$scale.$unit;
}


function p3d_generate_request_key($product_id, $printer_id, $material_id, $coating_id, $infill, $unit, $scale, $email_address, $model_file) {
	return md5($product_id.$printer_id.$material_id.$coating_id.$infill.$unit.$scale.$email_address.$model_file);
}

add_filter( 'woocommerce_add_cart_item_data', 'p3d_add_cart_item_data', 10, 2 );
function p3d_add_cart_item_data( $cart_item_meta, $product_id ) {
	global $woocommerce, $order;
	$settings = get_option('3dp_settings');

	if ( isset( $_REQUEST['attribute_pa_p3d_material'] ) && isset( $_REQUEST['attribute_pa_p3d_printer'] ) && isset( $_REQUEST['attribute_pa_p3d_model'] ) && isset( $_REQUEST['attribute_pa_p3d_unit'] ) ) {

		$thumbnail_data=$_REQUEST['p3d_thumb'];
		$thumbnail_url=p3d_save_thumbnail( $thumbnail_data, $_REQUEST['attribute_pa_p3d_model'] );

		$material_id=(int)$_REQUEST['attribute_pa_p3d_material'];
		if (is_numeric($_REQUEST['attribute_pa_p3d_coating'])) $coating_id=(int)$_REQUEST['attribute_pa_p3d_coating'];
		else $coating_id="";
		$printer_id=(int)$_REQUEST['attribute_pa_p3d_printer'];
		$materials_array = p3d_get_option( '3dp_materials' );
		$material=$materials_array[$material_id];
		$scale = (float)$_REQUEST['p3d_resize_scale'];
		if (empty($scale)) $scale = 1;

		$infill=(int)$_REQUEST['attribute_pa_p3d_infill'];

		$coatings_array = p3d_get_option( '3dp_coatings' );
		if (is_numeric($coating_id)) $coating=$coatings_array[$coating_id];
		else $coating = array();
		if ($_REQUEST['attribute_pa_p3d_unit']=='inch')
			$unit='inch';
		else
			$unit='mm';

		$printers_array = p3d_get_option( '3dp_printers' );
		$printer=$printers_array[$printer_id];


		$model_file=p3d_basename($_REQUEST['attribute_pa_p3d_model']);

		if ( $thumbnail_url ) $cart_item_meta['3dp_options']['thumbnail']=$thumbnail_url;
		$cart_item_meta['3dp_options']['printer_name']=$printer_id.'.'.str_replace( array( '&', '?', '#', '/', '\\' ), '_', strip_tags( __($printer['name'], '3dprint') ) );
		$cart_item_meta['3dp_options']['infill']=$infill.'%';
		$cart_item_meta['3dp_options']['unit']=$unit;
		if ($printer['type']!='fff') $cart_item_meta['3dp_options']['infill'] = '';
#		$cart_item_meta['3dp_options']['thumbnail_data']=$thumbnail_data;
		$cart_item_meta['3dp_options']['material_name']=$material_id.'.'.str_replace( array( '&', '?', '#', '/', '\\' ), '_', strip_tags( __($material['name'], '3dprint') ) );
		if (isset($coating) && count($coating)>0) $cart_item_meta['3dp_options']['coating_name']=$coating_id.'.'.str_replace( array( '&', '?', '#', '/', '\\' ), '_', strip_tags( __($coating['name'], '3dprint') ) );
		$cart_item_meta['3dp_options']['model_name']=str_replace( array( '&', '?', '#', '/', '\\' ), '_',  p3d_basename( $model_file ) );
		$cart_item_meta['3dp_options']['model_name_original']= stripslashes(wp_kses_post($_REQUEST['file_original'])); //todo: shorten if too long?
		$cart_item_meta['pa_model_name_original']= stripslashes(wp_kses_post($_REQUEST['file_original'])); //todo: shorten if too long?
#		$cart_item_meta['3dp_options']['model_key']=$printer['layer_height']."_".$printer['wall_thickness']."_".$printer['bottom_top_thickness']."_".$printer['nozzle_size']."_".$infill."_".$scale."_".$unit."_".$printer['support']."_".$printer['support_type']."_".$printer['support_angle'];
		$cart_item_meta['3dp_options']['model_key']=p3d_generate_model_key($printer, $infill, $scale, $unit);


		if ( isset( $_GET['p3d_buynow'] ) && $_GET['p3d_buynow']==1 ) {
			$p3d_price_requests=p3d_get_option( '3dp_price_requests' );
			$unit=$_REQUEST['unit'];
			$scale=$_REQUEST['scale'];
			$email_address=$_REQUEST['email_address'];
#			$product_key=$product_id.'_'.$printer_id.'_'.$material_id.'_'.$coating_id.'_'.$infill.'_'.$unit.'_'.$scale.'_'.$email_address.'_'.base64_encode( p3d_basename( $model_file ) );
			$product_key = p3d_generate_request_key($product_id, $printer_id, $material_id, $coating_id, $infill, $unit, $scale, $email_address, $model_file);


			foreach ( $_GET as $key=>$value ) {
				if ( strstr( $key, 'attribute' ) && $p3d_price_requests[$product_key]['attributes'][$key]!=$value ) $price_error=true;
			}
			if ( $price_error ) die( __( 'Attribute error' , '3dprint' ) );
#var_dump($p3d_price_requests[$product_key]);exit;
			$p3d_price=$p3d_price_requests[$product_key]['price'];
			$cart_item_meta['3dp_options']['thumbnail']=$p3d_price_requests[$product_key]['thumbnail_url'];

		}
		else {
			//$product_key=$product_id.'_'.$printer_id.'_'.$material_id.'_'.$coating_id.'_'.$infill.'_'.base64_encode( p3d_basename( $model_file ) );
			$upload_dir = wp_upload_dir();
			$file_path=$upload_dir['basedir'].'/p3d/'.p3d_basename( $model_file );

			$p3d_file_url_meta = get_post_meta($product_id, 'p3d_file_url'); $p3d_file_url=$p3d_file_url_meta[0];
			$p3d_product_price_type_meta = get_post_meta($product_id, 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];
			if (strlen($p3d_file_url)>0) {
				$upload_dir = wp_upload_dir();
				$uploads = dirname($upload_dir['basedir']).'/'.dirname(substr($p3d_file_url, strpos($p3d_file_url, 'uploads/'))).'/';
				$basename = p3d_basename( $p3d_file_url );
				$file_path = $uploads . $basename;
			}


			if ( !file_exists( $file_path ) ) return false;

			if (strlen($p3d_file_url)>0 && $p3d_product_price_type=='fixed') {//skip for performance optimization
				$model_stats['model']=array('material_volume'=>0, 'box_volume'=>0, 'surface_area'=>0, 'print_time'=>0);
			}
			else {
				$model_stats['model'] = p3d_get_model_stats( $file_path, $unit, $scale, $printer_id, $infill );
			}
			$model_stats['product_id'] = $product_id;
			$weight = $model_stats['model']['material_volume'] * $material['density'];
			$cart_item_meta['3dp_options']['weight'] = $weight * 0.001;//kg

			$cart_item_meta['3dp_options']['scale'] = $scale;
			if (isset($model_stats['model']['x_dim'])) $cart_item_meta['3dp_options']['width'] = $model_stats['model']['x_dim'] ;//cm
			if (isset($model_stats['model']['y_dim'])) $cart_item_meta['3dp_options']['length'] = $model_stats['model']['y_dim'];
			if (isset($model_stats['model']['z_dim'])) $cart_item_meta['3dp_options']['height'] = $model_stats['model']['z_dim'];
			$path_parts = pathinfo($model_file);
			$resized_file_path = $upload_dir['basedir'].'/p3d/'.$path_parts['filename'].'_resized.'.$path_parts['extension'];
			$mktime = time();
			if (strlen($p3d_file_url)>0) {
				$resized_file_path = $uploads.$mktime."_".$path_parts['filename'].'_resized.'.$path_parts['extension'];
			}

			if (file_exists($resized_file_path)) {
				$model_file=p3d_basename($resized_file_path);
				$cart_item_meta['3dp_options']['model_name']=str_replace( array( '&', '?', '#', '/', '\\' ), '_',  p3d_basename( $model_file ) );
				copy($upload_dir['basedir'].'/p3d/'.$path_parts['filename'].'.'.$path_parts['extension'].'.png', $resized_file_path.'.png');
			}

			$product = new WC_Product( $product_id );


			global $min_price;
			if ( version_compare( $woocommerce->version, '3.0.0', '<' ) ) { 
				$min_price = $product->price;
			}
			else {
				$min_price = $product->get_price();
			}

			$printing_price = p3d_calculate_printing_cost( $printer_id, $material_id, $coating_id, $model_stats, $_REQUEST );

			$p3d_price = p3d_round_price( $printing_price, $settings['price_num_decimals'] );
#var_dump( $p3d_price);

		}

//		print_r($cart_item_meta);exit;
		$cart_item_meta['3dp_options']['product-price'] = $p3d_price;

	}
	return apply_filters( '3dprint_cart_item_meta', $cart_item_meta, $model_stats,  $p3d_price);	
}

function p3d_round_price($price, $digits)
{
	$price = round($price, $digits);
	if ($price==0) return pow(10, abs($digits));
	else return $price;
}


add_action( 'woocommerce_before_calculate_totals', 'p3d_add_startup_cost', 10 );
function p3d_add_startup_cost( $cart_object ) {
	global $woocommerce;
	$settings = get_option('3dp_settings');
	$has_p3d = false;	

	foreach ( $cart_object->cart_contents as $key => $value ) {
		if (p3d_is_p3d($value['product_id'])) {
			$has_p3d = true;
			break;
		}
	}
	if ($has_p3d && (float)$settings['startup_price']>0) {
		$extra_fee_option_taxable = false;
		if ($settings['startup_price_taxable']=='on') $extra_fee_option_taxable=true;
		WC()->cart->add_fee( __('Startup Cost', '3dprint'), (float)$settings['startup_price'], $extra_fee_option_taxable );
	}

}


add_action( 'woocommerce_add_to_cart', 'p3d_add_to_cart' );
function p3d_add_to_cart( $cart_item_key ) {

	$product=WC()->cart->cart_contents[$cart_item_key];
	$variation=$product['variation'];
	$settings=p3d_get_option( '3dp_settings' );
	if ( isset( $variation['attribute_pa_p3d_printer'] ) && isset( $variation['attribute_pa_p3d_material'] ) && isset( $variation['attribute_pa_p3d_model'] ) ) {
		p3d_clear_cookies();
	}

}



function p3d_query_string( $params, $name=null ) {
	$ret = "";
	foreach ( $params as $key=>$val ) {
		if ( is_array( $val ) ) {
			if ( $name==null ) $ret .= queryString( $val, $key );
			else $ret .= queryString( $val, $name."[$key]" );
		} else {
			if ( $name!=null )
				$ret.=$name."[$key]"."=$val&";
			else $ret.= "$key=$val&";
		}
	}
	return $ret;
}


function p3d_basename($file) {
	$array=explode('/',$file);
	$base=array_pop($array);
	return $base;
} 

if ( ! wp_next_scheduled( 'p3d_housekeeping' ) ) {
  wp_schedule_event( time(), 'daily', 'p3d_housekeeping' );
}

add_action( 'p3d_housekeeping', 'p3d_do_housekeeping' );
function p3d_do_housekeeping() {
	$uploads = wp_upload_dir( );
	$files = glob($uploads['basedir']."/p3d/*");
	$now   = time();
	$settings = p3d_get_option( '3dp_settings' );
	if ((int)$settings['file_max_days']>0) {
		foreach ($files as $file) {
			$filename = p3d_basename($file);
			if (is_file($file) && $filename != '.htaccess' && $filename != 'index.html') {
				if ($now - filemtime($file) >= 60 * 60 * 24 * $settings['file_max_days']) {
					unlink($file);
				}
			}
		}
	}
}


function p3d_find_all_files($dir) {
	$root = scandir($dir);
	foreach($root as $value) {
		if($value === '.' || $value === '..') {continue;}
		if(is_file("$dir/$value")) {$result[]="$dir/$value";continue;}
		foreach(p3d_find_all_files("$dir/$value") as $value) {
			$result[]=$value;
		}
	}
	return $result;
} 

function p3d_get_accepted_models () {
	$settings = get_option('3dp_settings');
	$file_extensions = explode(',', $settings['file_extensions']);
	$models = array();
	foreach ($file_extensions as $extension) {
		if ($extension=='zip') continue;
		$models[]=$extension;
		
	}
	return $models;
}

function p3d_get_support_extensions_inside_archive() {
	return array('mtl', 'png', 'jpg', 'jpeg', 'gif', 'tga', 'bmp');
}
function p3d_get_allowed_extensions_inside_archive() {
	return array_merge(p3d_get_accepted_models(), p3d_get_support_extensions_inside_archive());
}

function p3d_get_non_native_extensions() {
	return array('stp', 'step', 'igs', 'iges', 'png', 'jpg', 'jpeg', 'gif', 'bmp');
}


function p3d_get_files_to_convert () {
	$settings = get_option('3dp_settings');
	$file_extensions = explode(',', $settings['file_extensions']);
	$models = array();
	foreach ($file_extensions as $extension) {
		if ($extension=='stl' || $extension=='obj') continue;
		$models[]=$extension;
		
	}
	return $models;
}

function p3d_get_trial_number() {
	$settings = get_option('3dp_settings');

	if ( !function_exists('curl_version') ) return __( 'The server does not support curl.', '3dprint' );

	//check if file already exists on a remote server
	$post = array( 'login' => $settings['api_login']);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://srv1.wp3dprinting.com/get_trials.php");
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	if (PHP_VERSION < 7.0) {
		curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
	}
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result=curl_exec ($ch);


	if($errno = curl_errno($ch)) {
		$error_message = curl_strerror($errno);		
	}
	if ( $errno ) {
		return __( 'Error: '.$error_message, '3dprint' );
	}
	curl_close ($ch);
	return $result;
}

function p3d_process_mtl($mtl_path, $timestamp) {
	if (file_exists($mtl_path)) {
		$new_content='';
		$handle = fopen($mtl_path, "r");  
		while (($line = fgets($handle)) !== false) {
			if (substr( trim(strtolower($line)), 0, 4 ) === "map_") {
				list ($map, $file) = explode(' ', $line, 2);
				$newline = "$map $timestamp"."_".basename($file)."\n";
			} else {
				$newline = $line;
			}
			$new_content.=$newline;
		  }
		fclose($handle);
		file_put_contents($mtl_path, $new_content);
	}
}

function p3d_get_mtl($file_path) {
	if (file_exists($file_path)) {
		$handle = fopen($file_path, "r");  
		while (($line = fgets($handle)) !== false) {
			if (substr( trim(strtolower($line)), 0, 6 ) === "mtllib") {
				list ($mtllib, $file) = explode(' ', $line, 2);
				list ($time, $name) = explode('_', p3d_basename($file_path), 2);
				return $time."_".$file;
			}
		}
	}
	return '';
}

if (!function_exists('curl_file_create')) {
	function curl_file_create($filename, $mimetype = '', $postname = '') {
		return "@$filename;filename="
		. ($postname ?: basename($filename))
		. ($mimetype ? ";type=$mimetype" : '');
	}
}

add_filter( 'ninja_forms_submit_data', 'p3d_ninja_forms_submit_data' );
function p3d_ninja_forms_submit_data( $form_data ) {
	$p3d_product = false;
	$form_id = $form_data['id'];
	foreach( $form_data[ 'fields' ] as $field ) { 
		$model = Ninja_Forms()->form($form_id)->get_field( $field['id'] );
		$model_key = $model->get_setting( 'key' );

		if ($model_key == 'nf_p3d_product_id') {
			$p3d_product = true;
			break;
		}
	}

	if (!$p3d_product) return $form_data;


	$db_printers=p3d_get_option( '3dp_printers' );
	$db_materials=p3d_get_option( '3dp_materials' );
	$db_coatings=p3d_get_option( '3dp_coatings' );
	$settings=p3d_get_option( '3dp_settings' );

	foreach( $form_data[ 'fields' ] as $key => $field ) { 
		$model = Ninja_Forms()->form($form_id)->get_field( $field['id'] );
		$model_key = $model->get_setting( 'key' );

		switch ($model_key) {
			case 'email' :
				$email_address = $field[ 'value' ];

			break;
			case 'message' :
				$request_comment = $field[ 'value' ];

			break;

			case 'nf_p3d_printer' :
				$printer_id = (int)$field[ 'value' ];
				$form_data[ 'fields' ][$key][ 'value' ] = $db_printers[$printer_id]['name']; 

			break;
			case 'nf_p3d_material' :
				$material_id = (int)$field[ 'value' ];
				$form_data[ 'fields' ][$key][ 'value' ] = $db_materials[$material_id]['name']; 
			break;
			case 'nf_p3d_coating' :
				$coating_id = (int)$field[ 'value' ];
				$form_data[ 'fields' ][$key][ 'value' ] = $db_coatings[$coating_id]['name']; 
			break;
			case 'nf_p3d_product_id' :
				$product_id = (int)$field[ 'value' ];
			break;
			case 'nf_p3d_resize_scale' :
				$scale = $field[ 'value' ]; //value = Dimensions
			break;
			case 'nf_p3d_unit' :
				$unit = $field[ 'value' ];
			break;
			case 'nf_p3d_model' :
				$model_file = $field[ 'value' ];
			break;
			case 'nf_p3d_infill' :
				$infill = (int)$field[ 'value' ];
			break;
			case 'nf_p3d_thumbnail' :
				$thumbnail_data = $field[ 'value' ]; //value = URL
			break;
			case 'nf_p3d_dim_x' :
				$dim_x = $field[ 'value' ]; 
			break;
			case 'nf_p3d_dim_y' :
				$dim_y = $field[ 'value' ]; 
			break;
			case 'nf_p3d_dim_z' :
				$dim_z = $field[ 'value' ]; 
			break;
			case 'nf_p3d_estimated_price' :
				$estimated_price = $field[ 'value' ]; //value = URL
			break;
		}

	}

	$thumbnail_url=p3d_save_thumbnail( $thumbnail_data, $model_file );

	$p3d_file_url_meta = get_post_meta($product_id, 'p3d_file_url'); $p3d_file_url = $p3d_file_url_meta[0];
	$p3d_product_price_type_meta = get_post_meta($product_id, 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];

	if ($scale!=1 || $unit!='mm') {
		$upload_dir = wp_upload_dir();
		$file_path=$upload_dir['basedir'].'/p3d/'.p3d_basename( $model_file );

		if (strlen($p3d_file_url)>0) {
			$uploads = dirname($upload_dir['basedir']).'/'.dirname(substr($p3d_file_url, strpos($p3d_file_url, 'uploads/'))).'/';
			$basename = p3d_basename( $p3d_file_url );
			$file_path = $uploads . $basename;
		}

		$path_parts = pathinfo($file_path);
		p3d_get_model_stats( $file_path, $unit, $scale, $printer_id, $infill );
		$resized_file_path = $upload_dir['basedir'].'/p3d/'.$path_parts['filename'].'_resized.'.$path_parts['extension'];
	
		if (file_exists($resized_file_path)) {
			$model_file=p3d_basename($resized_file_path);
			copy($upload_dir['basedir'].'/p3d/'.$path_parts['filename'].'.'.$path_parts['extension'].'.png', $resized_file_path.'.png');
		}
	}

	$error=false;
	$upload_dir = wp_upload_dir();

	#$product_key=$product_id.'_'.$printer_id.'_'.$material_id.'_'.$coating_id.'_'.$infill.'_'.$unit.'_'.$scale.'_'.$email_address.'_'.base64_encode( $model_file );
	$product_key = p3d_generate_request_key($product_id, $printer_id, $material_id, $coating_id, $infill, $unit, $scale, $email_address, $model_file);


	$p3d_price_requests=(array)p3d_get_option( '3dp_price_requests' );

	if (!is_array($p3d_price_requests)) $p3d_price_requests = array();


	$p3d_price_requests[$product_key]['product_id'] = $product_id;
	$p3d_price_requests[$product_key]['material_id'] = $material_id;
	$p3d_price_requests[$product_key]['coating_id'] = $coating_id;
	$p3d_price_requests[$product_key]['unit'] = $unit;
	$p3d_price_requests[$product_key]['scale'] = $scale;
	$p3d_price_requests[$product_key]['email_address'] = $email_address;
	$p3d_price_requests[$product_key]['model_file'] = $model_file;
	$p3d_price_requests[$product_key]['printer'] = $db_printers[$printer_id]['name'];
	$p3d_price_requests[$product_key]['material'] = $db_materials[$material_id]['name'];
	$p3d_price_requests[$product_key]['scale_x'] = $dim_x;
	$p3d_price_requests[$product_key]['scale_y'] = $dim_y;
	$p3d_price_requests[$product_key]['scale_z'] = $dim_z;
	$p3d_price_requests[$product_key]['thumbnail_url'] = $thumbnail_url;


	if (is_numeric($coating_id))
		$p3d_price_requests[$product_key]['coating'] = $db_coatings[$coating_id]['name'];
	if (is_numeric($infill))
		$p3d_price_requests[$product_key]['infill'] = "$infill %";


	$p3d_price_requests[$product_key]['attributes']=array();
	$p3d_price_requests[$product_key]['attributes']['attribute_pa_p3d_printer']=$printer_id;
	$p3d_price_requests[$product_key]['attributes']['attribute_pa_p3d_material']=$material_id;
	$p3d_price_requests[$product_key]['attributes']['attribute_pa_p3d_coating']=$coating_id;
	$p3d_price_requests[$product_key]['attributes']['attribute_pa_p3d_infill']=$infill;
#	$p3d_price_requests[$product_key]['attributes']['attribute_pa_p3d_scale']=$scale;
	$p3d_price_requests[$product_key]['attributes']['attribute_pa_p3d_unit']=$unit;
	$p3d_price_requests[$product_key]['attributes']['attribute_pa_p3d_model']=$model_file;


/*	foreach ( $_POST as $key => $value ) { //todo
		if ( strpos( $key, 'attribute_' )===0 ) {
			if ( !strstr( $key, 'p3d_' ) ) {
				$email_attrs[$key]=$value;
			}
				$p3d_price_requests[$product_key]['attributes'][$key]=$value;
		}
	}*/

	if (file_exists($resized_file_path)) {
		$p3d_price_requests[$product_key]['attributes']['attribute_pa_p3d_model']=p3d_basename($resized_file_path);
	}

	$p3d_price_requests[$product_key]['price']='';
	$p3d_price_requests[$product_key]['estimated_price']=(float)$estimated_price;
	$p3d_price_requests[$product_key]['email']=$email_address;
	$p3d_price_requests[$product_key]['request_comment']=$request_comment;

	$p3d_price_requests[$product_key]['scale']=(float)$scale;
	$p3d_price_requests[$product_key]['scale_x']=(float)$dim_x;
	$p3d_price_requests[$product_key]['scale_y']=(float)$dim_y;
	$p3d_price_requests[$product_key]['scale_z']=(float)$dim_z;
	$dimensions=$p3d_price_requests[$product_key]['scale_x']." &times; ".$p3d_price_requests[$product_key]['scale_y']." &times; ".$p3d_price_requests[$product_key]['scale_z']." ".__('cm', '3dprint');

	update_option( "3dp_price_requests", $p3d_price_requests );


	$upload_dir = wp_upload_dir();
	$link = $upload_dir['baseurl'].'/p3d/'.rawurlencode($model_file);

	$filepath = $upload_dir['basedir']."/p3d/".rawurlencode($model_file);
	$original_file = p3d_get_original($filepath);

	$original_link = $upload_dir['baseurl']."/p3d/".rawurlencode(p3d_basename($original_file));
	if (strlen($p3d_file_url)>0) {
		$original_link = $p3d_file_url;
		$p3d_original_file_url_meta = get_post_meta($product_id, 'p3d_original_file_url'); 
		if (strlen($p3d_original_file_url_meta[0])>0) $original_link=$p3d_original_file_url_meta[0];
	}

	elseif (file_exists($original_file) && p3d_basename($filepath) != p3d_basename($original_file)) {
		$original_link = $upload_dir['baseurl']."/p3d/".rawurlencode(p3d_basename($original_file));
		$model_link=$link;
#		$original_link='<a target="_blank" href="'.$original_link.'">'.urldecode(urldecode(p3d_basename($original_file))).'</a>';
	}
	else {
		$model_link=$link;
		$original_link="";
	}


	foreach( $form_data[ 'fields' ] as $key => $field ) {
		$model = Ninja_Forms()->form($form_id)->get_field( $field['id'] );
		$model_key = $model->get_setting( 'key' );

		switch ($model_key) {
			case 'nf_p3d_model' :
				$form_data[ 'fields' ][$key][ 'value' ] = $model_link; 
			break;
			case 'nf_p3d_original_model' :
				$form_data[ 'fields' ][$key][ 'value' ] = $original_link; 
			break;
			case 'nf_p3d_thumbnail' :
				$form_data[ 'fields' ][$key][ 'value' ] = $thumbnail_url; 
			break;
			case 'nf_p3d_dimensions' :
				$form_data[ 'fields' ][$key][ 'value' ] = $dimensions; 
			break;
		}

	}


#print_r($form_data[ 'fields' ]);
	return $form_data;
}



function p3d_handle_upload() {

	error_reporting( 0 );
	set_time_limit( 5*60 );
	ini_set( 'memory_limit', '-1' );
	$allowed_extensions_inside_archive=p3d_get_allowed_extensions_inside_archive();
        $support_extensions_inside_archive=p3d_get_support_extensions_inside_archive();
	$product_id = (int)$_REQUEST['product_id'];
	$printer_id = (int)$_REQUEST['printer_id'];
	$material_id = (int)$_REQUEST['material_id'];
	$image_height = (int)$_REQUEST['image_height'];
	$image_map = (int)$_REQUEST['image_map'];
	$db_printers = p3d_get_option( '3dp_printers' );
	$printer = $db_printers[$printer_id];

	if (is_numeric($_REQUEST['coating_id'])) {
		$coating_id = (int)$_REQUEST['coating_id'];
	}
	else {
		$coating_id = '';
	}

	if ( $_REQUEST['unit'] == 'inch' ) {
		$unit = "inch";
	}
	else {
		$unit = "mm";
	}
	$model_stats = array();
	$settings = p3d_get_option( '3dp_settings' );

	$wp_upload_dir = wp_upload_dir();

	//$targetDir = get_temp_dir();
	$targetDir = $wp_upload_dir['basedir'].'/p3d/';




	$cleanupTargetDir = true; // Remove old files
	$maxFileAge = 5 * 3600; // Temp file age in seconds


	// Create target dir
	if ( !file_exists( $targetDir ) ) {
		@mkdir( $targetDir );
	}

	// Get a file name
	if ( isset( $_REQUEST["name"] ) ) {
		$fileName = $_REQUEST["name"];
	} elseif ( !empty( $_FILES ) ) {
		$fileName = $_FILES["file"]["name"];
	} else {
		$fileName = uniqid( "file_" );
	}

	$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

	// Chunking might be enabled
	$chunk = isset( $_REQUEST["chunk"] ) ? intval( $_REQUEST["chunk"] ) : 0;
	$chunks = isset( $_REQUEST["chunks"] ) ? intval( $_REQUEST["chunks"] ) : 0;


	// Remove old temp files
	if ( $cleanupTargetDir ) {
		if ( !is_dir( $targetDir ) || !$dir = opendir( $targetDir ) ) {
			die( '{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "'.__( "Failed to open temp directory.", '3dprint' ).'"}, "id" : "id"}' );
		}

		while ( ( $file = readdir( $dir ) ) !== false ) {
			$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

			// If temp file is current file proceed to the next
			if ( $tmpfilePath == "{$filePath}.part" ) {
				continue;
			}

			// Remove temp file if it is older than the max age and is not the current file
			if ( preg_match( '/\.part$/', $file ) && ( filemtime( $tmpfilePath ) < time() - $maxFileAge ) ) {
				@unlink( $tmpfilePath );
			}
		}
		closedir( $dir );
	}


	// Open temp file
	if ( !$out = @fopen( "{$filePath}.part", $chunks ? "ab" : "wb" ) ) {
		die( '{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "'.__( 'Failed to open output stream.', '3dprint' ).'"}, "id" : "id"}' );
	}

	if ( !empty( $_FILES ) ) {
		if ( $_FILES["file"]["error"] || !is_uploaded_file( $_FILES["file"]["tmp_name"] ) ) {
			die( '{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "'.__( 'Failed to move uploaded file.', '3dprint' ).'"}, "id" : "id"}' );
		}

		// Read binary input stream and append it to temp file
		if ( !$in = @fopen( $_FILES["file"]["tmp_name"], "rb" ) ) {
			die( '{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "'.__( 'Failed to open input stream.', '3dprint' ).'"}, "id" : "id"}' );
		}
	} else {
		if ( !$in = @fopen( "php://input", "rb" ) ) {
			die( '{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "'.__( 'Failed to open input stream.', '3dprint' ).'"}, "id" : "id"}' );
		}
	}

	while ( $buff = fread( $in, 4096 ) ) {
		fwrite( $out, $buff );
	}

	@fclose( $out );
	@fclose( $in );

	// Check if file has been uploaded
	if ( !$chunks || $chunk == $chunks - 1 ) {
		// Strip the temp .part suffix off
		$time = uniqid();
		$original_filename = p3d_basename($filePath);
		$md5_filename = md5_file("{$filePath}.part") . '.' . p3d_extension($filePath);
		$newfilePath = dirname($filePath) . '/' . $time.'_'.$md5_filename ;
		rename( "{$filePath}.part",  $newfilePath);
		$filePath = $newfilePath;

#echo "$md5_filename, $original_filename \n";
#echo WC()->session->get($md5_filename);
#exit;

		$uploads = wp_upload_dir();
		$uploads['path'] = $uploads['basedir'].'/p3d/';

		//$wp_filename =  wp_unique_filename( $uploads['path'], rawurlencode(p3d_basename( $filePath ) ) );

#		$wp_filename =  $time.'_'.rawurlencode( p3d_basename( $filePath ) ) ;
		$wp_filename =  p3d_basename( $newfilePath ) ;
		
                //WC()->session->set($wp_filename, $original_filename);
#		$original_filename = $wp_filename;


		$new_file = $uploads['path'] . "$wp_filename";
		

		$path_parts = pathinfo($new_file);
		$extension = strtolower($path_parts['extension']);
		$basename = $path_parts['basename'];

		$stl_count=0;


		if ($extension=='zip') {
			if (class_exists('ZipArchive')) {

				$zip = new ZipArchive;
				$res = $zip->open( $filePath );
				if ( $res === TRUE ) {

					for( $i = 0; $i < $zip->numFiles; $i++ ) {
						$file_to_extract = p3d_basename( $zip->getNameIndex($i) );
						$f2e_path_parts = pathinfo($file_to_extract);
						$f2e_extension = mb_strtolower($f2e_path_parts['extension']);

						if (!in_array($f2e_extension, $allowed_extensions_inside_archive)) continue;

						if ( in_array($f2e_extension, p3d_get_accepted_models()) && !in_array($f2e_extension, $support_extensions_inside_archive)) {
						
							$file_found = true;
#							$file_to_extract = rawurlencode( p3d_basename( $file_to_extract ) ) ;
#							$wp_filename =  $time.'_'.$file_to_extract ;
#							$wp_filename =  $file_to_extract ;
							$extension = strtolower(p3d_extension($file_to_extract));
						}


						$zip->extractTo( "$targetDir/tmp", array( $zip->getNameIndex($i) ) );
                                                $files = p3d_find_all_files("$targetDir/tmp");

						foreach ($files as $filename) {

							if (p3d_extension($filename)=='stl' || p3d_extension($filename)=='obj') { 
								$wp_filename = $time.'_'.p3d_basename(md5_file($filename)).'.'.p3d_extension($filename) ;
						                //WC()->session->set($wp_filename, p3d_basename( $file_to_extract ));
								$original_filename = p3d_basename( $file_to_extract );
								rename($filename, $uploads['path'].$wp_filename);
							}
//							else if (p3d_extension($filename)=='mtl') {
//								rename($filename, $uploads['path'].$wp_filename);
//							}
							else  { //mtl, texture files
								rename($filename, $uploads['path'].$time."_".$file_to_extract); //todo: md5 everything
							}

							if (p3d_extension($filename)=='mtl') { 
								$material_file = $time."_".$file_to_extract;
//								$material_file = $time."_".md5_file($filename).'.'.p3d_extension($filename);

								p3d_process_mtl($uploads['path'].$material_file, $time);
								$output['material']=$material_file;
							}
							if (p3d_extension($filename)=='stl') { 
								$stl_count++;
							}

						}

					}

					$zip->close();
					if ( !$file_found ) {
						die( '{"jsonrpc" : "2.0", "error" : {"code": 104, "message": "'.__( 'Model file not found.', '3dprint' ).'"}, "id" : "id"}' );
					}
#					rename($filePath, $uploads['path'].$time.'_'.$wp_filename.'.zip');

				}
				else {
					die( '{"jsonrpc" : "2.0", "error" : {"code": 105, "message": "'.__( 'Could not extract the file.', '3dprint' ).'"}, "id" : "id"}' );
				}
			}
			else {
				die( '{"jsonrpc" : "2.0", "error" : {"code": 106, "message": "'.__( 'The server does not support zip archives.', '3dprint' ).'"}, "id" : "id"}' );
			}
		} 
		elseif (in_array($extension, p3d_get_accepted_models())) {
			rename( $filePath, $new_file );
		}
		if ((in_array($extension, p3d_get_non_native_extensions()) && $material_file=='') || ($settings['api_pack']=='on' && $stl_count>1)) { //convert non-native extensions 
			if ($settings['api_pack']=='on' && $stl_count>1) { //pack
				$original_filename = p3d_basename($filePath);
				if ($printer['platform_shape']=='rectangle') {
					$params = array('platform_width'=>$printer['width'], 'platform_length'=>$printer['length'], 'spacing'=>$settings['api_pack_spacing'], 'unit'=>$unit );
				}
				elseif ($printer['platform_shape']=='circle') {
					$params = array('platform_diameter'=>$printer['diameter'], 'spacing'=>$settings['api_pack_spacing'], 'unit'=>$unit);
				}

//			        $result = p3d_convert_to_stl ($uploads['path'].$time.'_'.$wp_filename.'.zip', $params);
#echo $newfilePath;
			        $result = p3d_convert_to_stl ($newfilePath, $params);

				$output['failed_files_count'] = $result['failed_files_count'];
			}
			else { //convert
			        $result = p3d_convert_to_stl ($wp_filename, array('image_height'=>$image_height, 'image_map'=>$image_map));
			}
			$new_file = $uploads['path'] . "$wp_filename";
#			$wp_filename =  $time.'_'.urlencode( p3d_basename( $result['filename'] ) ) ;
#			rename( $uploads['path'].p3d_basename( $result['filename'] ), $uploads['path'].$time.'_'.urlencode(p3d_basename( $result['filename'] )));

			$wp_filename =  urlencode( p3d_basename( $result['filename'] ) ) ;
			rename( $uploads['path'].p3d_basename( $result['filename'] ), $uploads['path'].urlencode(p3d_basename( $result['filename'] )));

		}

		$output['jsonrpc'] = "2.0";
		$output['filename'] = $wp_filename;
		$output['filename_original'] = stripslashes(wp_kses_post($original_filename));

		if (filesize($uploads['path'].$wp_filename) > ((int)$settings['file_max_size'] * 1048576)) {
			unlink($uploads['path'].$wp_filename);
			die( '{"jsonrpc" : "2.0", "error" : {"code": 113, "message": "'.__( 'The file is too large.', '3dprint' ).'"}, "id" : "id"}' );
		}

		$output = apply_filters( '3dprint_upload', $output, $product_id, $printer_id, $material_id, $coating_id );
		wp_die( json_encode( $output ) );

	}

}



function p3d_convert_to_stl ($filepath, $params) {

	error_reporting( 0 );
	set_time_limit( 5*60 );
	ini_set( 'memory_limit', '-1' );

	$servers = p3d_get_option( '3dp_servers' );
	shuffle($servers);
	$server = $servers[0];

	$file_to_upload = $filepath;
	$upload = true;
	$repair_url = $server."/convert2.php";
/*	if (strtolower(p3d_extension($filepath))=='zip') { //packer
		$server="http://srv3.wp3dprinting.com";
		$repair_url = "$server/convert3.php";
	}
*/


	$uploads = wp_upload_dir();
	$uploads['path'] = $uploads['basedir'].'/p3d/';

        
	$basename = p3d_basename( $filepath );
	$file_to_upload = $uploads['path'] . $basename;
	if ( !file_exists ( $file_to_upload ) ) wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 107, "message": "'.__( 'The file does not exist.', '3dprint' ).'"}, "id" : "id"}' );
	if ( !function_exists('curl_version') ) wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 108, "message": "'.__( 'The server does not support curl.', '3dprint' ).'"}, "id" : "id"}' );
	//check if file already exists on a remote server
	$post = array( 'file_name' => $basename, 'file_key' => md5_file ( $file_to_upload ), 'check_existence' => 1);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "$server/check2.php");
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	if (PHP_VERSION < 7.0) {
		curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
	}
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result=curl_exec ($ch);


	if($errno = curl_errno($ch)) {
		$error_message = curl_strerror($errno);		
	}
	curl_close ($ch);
	if ( $errno ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 109, "message": "'.__( 'Error: '.$error_message, '3dprint' ).'"}, "id" : "id"}' );
	}

	$response = json_decode($result, true);
	if ($response['file_exists'] == '1') $upload = false;

	if ($upload && strtolower(p3d_extension($filepath))!='zip') {
		if (class_exists('ZipArchive') && filesize($file_to_upload) >= 1048576 ) { 
			$zip_file = "$file_to_upload.tmp.zip";
			$zip = new ZipArchive();
			
			if ($zip->open($zip_file, ZipArchive::CREATE)!==TRUE) {
			    wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 112, "message": "'.__( 'Could not create a zip archive.', '3dprint' ).'"}, "id" : "id"}' );
			}
			$zip->addFile($file_to_upload, $basename);
			$zip->close();
			$file_to_upload = $zip_file;

		}
	}

	//wp_schedule_single_event
	$post = array_merge(array( 'file_name' => $basename, 'file_key' => md5_file ( $file_to_upload ), 'file_contents'=>curl_file_create($file_to_upload) ), $params);

	if (!$upload) unset($post['file_contents']);
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,$repair_url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	if (PHP_VERSION < 7.0) {
		curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
	}
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result=curl_exec ($ch);


	if($errno = curl_errno($ch)) {
		$error_message = curl_strerror($errno);		
	}
	curl_close ($ch);
	if (file_exists($zip_file)) unlink($zip_file);

	if ( $errno ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 109, "message": "'.__( 'Error: '.$error_message, '3dprint' ).'"}, "id" : "id"}' );
	}

	$response = json_decode($result, true);
//var_dump( $result);

	if ( $response['status']=='1' && !empty ( $response['url'] ) ) {
		//download repaired file

		$ch = curl_init($response['url']);
		$repaired_file = $response['filename']; 
		$failed_files_count = (int)$response['failed_files_count']; 
		$packed = (int)$response['packed']; 
		$fp = fopen($uploads['path'].$repaired_file, 'wb'); 
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);
		$output['jsonrpc'] = "2.0";
		$output['status'] = "1"; //completed
		$output['filename'] = $repaired_file;
		$output['failed_files_count'] = $failed_files_count;

		return $output;
		//wp_die( json_encode( $output ) );
	}
	elseif ( $response['status']=='0' ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 110, "message": "'.__( $response['message'], '3dprint' ).'"}, "id" : "id"}' );
	}
	else {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 111, "message": "'.__( 'Could not convert to STL.', '3dprint' ).'"}, "id" : "id"}' );
	}
	
}

function p3d_handle_process() {
	error_reporting( 0 );
	set_time_limit( 5*60 );
	ini_set( 'memory_limit', '-1' );

	do_action('p3d_handle_process_begin');

	$settings = p3d_get_option( '3dp_settings' );

	$servers = p3d_get_option( '3dp_servers' );
	shuffle($servers);

	$server = $servers[0];

	$process_url = $server."/process.php";

	$cookie = WC()->session->get_session_cookie();
	$session_id = md5($_SERVER['REMOTE_ADDR'].$cookie[0]);

	$uploads = wp_upload_dir();
	$uploads['path'] = $uploads['basedir'].'/p3d/';

	$basename = p3d_basename( $_POST['filename'] );
	$file_to_upload = $uploads['path'] . $basename;
	$upload = true;

	$product_id = (int)$_POST['product_id'];

	$p3d_file_url_meta = get_post_meta($product_id, 'p3d_file_url'); $p3d_file_url=$p3d_file_url_meta[0];
	$p3d_product_price_type_meta = get_post_meta($product_id, 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];
	

	if (strlen($p3d_file_url)>0) {
		$upload_dir = wp_upload_dir();
		$uploads = dirname($upload_dir['basedir']).'/'.dirname(substr($p3d_file_url, strpos($p3d_file_url, 'uploads/'))).'/';
		$basename = p3d_basename( $p3d_file_url );
		$file_to_upload = $uploads . $basename;
	}

	

	$rotation_x = (float)$_POST['rotation_x'];
	$rotation_y = (float)$_POST['rotation_y'];
	$rotation_z = (float)$_POST['rotation_z'];

	$scale_x = (float)$_POST['scale_x'];
	$scale_y = (float)$_POST['scale_y'];
	$scale_z = (float)$_POST['scale_z'];


	if ( !file_exists ( $file_to_upload ) ) wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 107, "message": "'.__( 'The file '.$file_to_upload.' does not exist.', '3dprint' ).'"}, "id" : "id"}' );

	if ( !function_exists('curl_version') ) wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 108, "message": "'.__( 'The server does not support curl.', '3dprint' ).'"}, "id" : "id"}' );

	//check if file already exists on a remote server
	$post = array( 'file_name' => $basename, 'file_key' => md5_file ( $file_to_upload ), 'check_existence' => 1);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "$server/check2.php");
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	if (PHP_VERSION < 7.0) {
		curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
	}
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result=curl_exec ($ch);


	if($errno = curl_errno($ch)) {
		$error_message = curl_strerror($errno);		
	}
	curl_close ($ch);
	if ( $errno ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 109, "message": "'.__( 'Error: '.$error_message, '3dprint' ).'"}, "id" : "id"}' );
	}

	$response = json_decode($result, true);
	if ($response['file_exists'] == '1') $upload = false;

	if ($upload) {
		if (class_exists('ZipArchive') && filesize($file_to_upload) >= 1048576 ) { 
			$zip_file = "$file_to_upload.tmp.zip";
			$zip = new ZipArchive();
			
			if ($zip->open($zip_file, ZipArchive::CREATE)!==TRUE) {
			    wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 112, "message": "'.__( 'Could not create a zip archive.', '3dprint' ).'"}, "id" : "id"}' );
			}
			$zip->addFile($file_to_upload, $basename);
			$zip->close();
			$file_to_upload = $zip_file;

		}
	}



	//wp_schedule_single_event

	$post = array( 'login' => $settings['api_login'],  'rotation_x' => $rotation_x, 'rotation_y' => $rotation_y, 'rotation_z' => $rotation_z, 'scale_x' => $scale_x, 'scale_y' => $scale_y, 'scale_z' => $scale_z, 'session_id' => $session_id, 'file_name' => $basename, 'file_key' => md5_file ( $file_to_upload ), 'file_contents'=>curl_file_create($file_to_upload) );

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$process_url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	if (PHP_VERSION < 7.0) {
		curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
	}
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

	$result=curl_exec ($ch);

	curl_close ($ch);
	if (file_exists($zip_file)) unlink($zip_file);


	$output['jsonrpc'] = "2.0";
	$output['status'] = "2"; //in progress
	$output['server'] = $server;
	wp_die( json_encode( $output ) );



}

function p3d_handle_process_check() {
	error_reporting( 0 );
	set_time_limit( 5*60 );
	ini_set( 'memory_limit', '-1' );

	do_action('p3d_handle_process_check_begin');

	$settings = p3d_get_option( '3dp_settings' );


	$servers = p3d_get_option( '3dp_servers' );
	if (!empty($_POST['server']) && in_array($_POST['server'], $servers)) $server = $_POST['server'];
	else 
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 114, "message": "'.__( 'Server not found.', '3dprint' ).'"}, "id" : "id"}' );



	$process_url = $server."/check_process.php";

	$rotation_x = (float)$_POST['rotation_x'];
	$rotation_y = (float)$_POST['rotation_y'];
	$rotation_z = (float)$_POST['rotation_z'];

	$scale_x = (float)$_POST['scale_x'];
	$scale_y = (float)$_POST['scale_y'];
	$scale_z = (float)$_POST['scale_z'];

	$product_id = (int)$_POST['product_id'];


	$cookie = WC()->session->get_session_cookie();
	$session_id = md5($_SERVER['REMOTE_ADDR'].$cookie[0]);
	$uploads = wp_upload_dir();
	$uploads['path'] = $uploads['basedir'].'/p3d/';

	$basename = p3d_basename( $_POST['filename'] );
	$file_to_upload = $uploads['path'] . $basename;
	$upload = true;
    $zip_file = "";
	$p3d_file_url_meta = get_post_meta($product_id, 'p3d_file_url'); $p3d_file_url=$p3d_file_url_meta[0];
	$p3d_product_price_type_meta = get_post_meta($product_id, 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];

	if (strlen($p3d_file_url)>0) {
		$upload_dir = wp_upload_dir();
		$uploads['path'] = dirname($upload_dir['basedir']).'/'.dirname(substr($p3d_file_url, strpos($p3d_file_url, 'uploads/'))).'/';
		$basename = p3d_basename( $p3d_file_url );
		$file_to_upload = $uploads . $basename;
	}


//	if ( !file_exists ( $file_to_upload ) ) wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 107, "message": "'.__( 'The file '.$file_to_upload.' does not exist.', '3dprint' ).'"}, "id" : "id"}' );

	if ( !function_exists('curl_version') ) wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 108, "message": "'.__( 'The server does not support curl.', '3dprint' ).'"}, "id" : "id"}' );


	//wp_schedule_single_event

	$post = array( 'login' => $settings['api_login'],  'rotation_x' => $rotation_x, 'rotation_y' => $rotation_y, 'rotation_z' => $rotation_z, 'scale_x' => $scale_x, 'scale_y' => $scale_y, 'scale_z' => $scale_z, 'session_id' => $session_id, 'file_name' => $basename, 'file_key' => md5_file ( $file_to_upload ) );

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $process_url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	if (PHP_VERSION < 7.0) {
		curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
	}
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result=curl_exec ($ch);


	if($errno = curl_errno($ch)) {
		$error_message = curl_strerror($errno);		
	}
	curl_close ($ch);
	if (file_exists($zip_file)) unlink($zip_file);
	if ( $errno ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 109, "message": "'.__( 'process error: '.$error_message, '3dprint' ).'"}, "id" : "id"}' );
	}

	$response = json_decode($result, true);


	if ( $response['status']=='1' && !empty ( $response['url'] ) ) {
		//download processed file

		$ch = curl_init($response['url']);
		$processed_file = $response['filename']; 
		$fp = fopen($uploads['path'].$processed_file, 'wb'); 
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);
		$output['jsonrpc'] = "2.0";
		$output['status'] = "1"; //completed
		$output['filename'] = $processed_file;

		wp_die( json_encode( $output ) );
	}
	elseif ( $response['status']=='1' ) {
		$output['jsonrpc'] = "2.0";
		$output['status'] = "1"; //completed

		wp_die( json_encode( $output ) );

	}
	elseif ( $response['status']=='2' ) {
		$output['jsonrpc'] = "2.0";
		$output['status'] = "2"; //in progress
		$output['server'] = $server;

		wp_die( json_encode( $output ) );

	}
	elseif ( $response['status']=='0' ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 110, "message": "'.__( $response['message'], '3dprint' ).'"}, "id" : "id"}' );
	}
	else {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 111, "message": "'.__( 'Unknown error.', '3dprint' ).'"}, "id" : "id"}' );
	}

}

function p3d_handle_repair() {
	error_reporting( 0 );
	set_time_limit( 5*60 );
	ini_set( 'memory_limit', '-1' );

	do_action('p3d_handle_repair_begin');

	$settings = p3d_get_option( '3dp_settings' );

	$servers = p3d_get_option( '3dp_servers' );
	shuffle($servers);
	$repair_url = $servers[0]."/repair5.php";
	$server = $servers[0];

	$repair = $optimize = 0;
	if ($_POST['repair']=='on') $repair=1;
	if ($_POST['optimize']=='on') $optimize=1;
	$printer_id = (int)$_POST['printer_id'];
	$printers_array = p3d_get_option( '3dp_printers' );
	$printer=$printers_array[$printer_id];
	$angle=(int)$printer['angle'];
	$cookie = WC()->session->get_session_cookie();
	$session_id = md5($_SERVER['REMOTE_ADDR'].$cookie[0]);
	$uploads = wp_upload_dir();
	$uploads['path'] = $uploads['basedir'].'/p3d/';

	$basename = p3d_basename( $_POST['filename'] );
	$file_to_upload = $uploads['path'] . $basename;
	$upload = true;
	$printer_type = $printer['type'];

	if ( !file_exists ( $file_to_upload ) ) wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 107, "message": "'.__( 'The file does not exist.', '3dprint' ).'"}, "id" : "id"}' );

	if ( !function_exists('curl_version') ) wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 108, "message": "'.__( 'The server does not support curl.', '3dprint' ).'"}, "id" : "id"}' );

	//check if file already exists on a remote server
	$post = array( 'file_name' => $basename, 'file_key' => md5_file ( $file_to_upload ), 'check_existence' => 1);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "$server/check2.php");
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	if (PHP_VERSION < 7.0) {
		curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
	}
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result=curl_exec ($ch);


	if($errno = curl_errno($ch)) {
		$error_message = curl_strerror($errno);		
	}
	curl_close ($ch);
	if ( $errno ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 109, "message": "'.__( 'Error: '.$error_message, '3dprint' ).'"}, "id" : "id"}' );
	}

	$response = json_decode($result, true);
	if ($response['file_exists'] == '1') $upload = false;

	if ($upload) {
		if (class_exists('ZipArchive') && filesize($file_to_upload) >= 1048576 ) { 
			$zip_file = "$file_to_upload.tmp.zip";
			$zip = new ZipArchive();
			
			if ($zip->open($zip_file, ZipArchive::CREATE)!==TRUE) {
			    wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 112, "message": "'.__( 'Could not create a zip archive.', '3dprint' ).'"}, "id" : "id"}' );
			}
			$zip->addFile($file_to_upload, $basename);
			$zip->close();
			$file_to_upload = $zip_file;

		}
	}



	//wp_schedule_single_event

	$post = array( 'login' => $settings['api_login'],  'repair'=>$repair, 'optimize'=>$optimize, 'printer_type'=>$printer_type, 'angle'=>$angle, 'session_id' => $session_id, 'file_name' => $basename, 'file_key' => md5_file ( $file_to_upload ), 'file_contents'=>curl_file_create($file_to_upload) );

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$repair_url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	if (PHP_VERSION < 7.0) {
		curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
	}
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

	$result=curl_exec ($ch);

	curl_close ($ch);
	if (file_exists($zip_file)) unlink($zip_file);


	$output['jsonrpc'] = "2.0";
	$output['status'] = "2"; //in progress
	$output['server'] = $server;
	wp_die( json_encode( $output ) );



}

function p3d_handle_repair_check() {
	error_reporting( 0 );
	set_time_limit( 5*60 );
	ini_set( 'memory_limit', '-1' );

	do_action('p3d_handle_repair_check_begin');

	$settings = p3d_get_option( '3dp_settings' );


	$servers = p3d_get_option( '3dp_servers' );
	if (!empty($_POST['server']) && in_array($_POST['server'], $servers)) $server = $_POST['server'];
	else 
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 114, "message": "'.__( 'Server not found.', '3dprint' ).'"}, "id" : "id"}' );


	$repair_url = $server."/check_repair.php";

	$repair=$optimize=0;
	if ($_POST['repair']=='on') $repair=1;
	if ($_POST['optimize']=='on') $optimize=1;

	$cookie = WC()->session->get_session_cookie();
	$session_id = md5($_SERVER['REMOTE_ADDR'].$cookie[0]);
	$uploads = wp_upload_dir();
	$uploads['path'] = $uploads['basedir'].'/p3d/';

	$basename = p3d_basename( $_POST['filename'] );
	$file_to_upload = $uploads['path'] . $basename;
	$upload = true;


	if ( !file_exists ( $file_to_upload ) ) wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 107, "message": "'.__( 'The file does not exist.', '3dprint' ).'"}, "id" : "id"}' );

	if ( !function_exists('curl_version') ) wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 108, "message": "'.__( 'The server does not support curl.', '3dprint' ).'"}, "id" : "id"}' );


	//wp_schedule_single_event

	$post = array( 'login' => $settings['api_login'],  'repair'=>$repair, 'optimize'=>$optimize, 'session_id' => $session_id, 'file_name' => $basename, 'file_key' => md5_file ( $file_to_upload ) );

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $repair_url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	if (PHP_VERSION < 7.0) {
		curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
	}
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result=curl_exec ($ch);


	if($errno = curl_errno($ch)) {
		$error_message = curl_strerror($errno);		
	}
	curl_close ($ch);
	if (file_exists($zip_file)) unlink($zip_file);
	if ( $errno ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 109, "message": "'.__( 'Repair error: '.$error_message, '3dprint' ).'"}, "id" : "id"}' );
	}

	$response = json_decode($result, true);


	if ( $response['status']=='1' && !empty ( $response['url'] ) ) {
		//download repaired file

		$ch = curl_init($response['url']);
		$repaired_file = $response['filename']; 
		$fp = fopen($uploads['path'].$repaired_file, 'wb'); 
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);
		$output['jsonrpc'] = "2.0";
		$output['status'] = "1"; //completed
		$output['filename'] = $repaired_file;
		$output['needed_repair']=$response['needed_repair'];
		$output['degenerate_facets']=$response['degenerate_facets'];
		$output['edges_fixed']=$response['edges_fixed'];
		$output['facets_removed']=$response['facets_removed'];
		$output['facets_added']=$response['facets_added'];
		$output['facets_reversed']=$response['facets_reversed'];
		$output['backwards_edges']=$response['backwards_edges'];


		wp_die( json_encode( $output ) );
	}
	elseif ( $response['status']=='1' ) {
		$output['jsonrpc'] = "2.0";
		$output['status'] = "1"; //completed
		$output['needed_repair']=$response['needed_repair'];

		wp_die( json_encode( $output ) );

	}
	elseif ( $response['status']=='2' ) {
		$output['jsonrpc'] = "2.0";
		$output['status'] = "2"; //in progress
		$output['server'] = $server;

		wp_die( json_encode( $output ) );

	}
	elseif ( $response['status']=='0' ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 110, "message": "'.__( $response['message'], '3dprint' ).'"}, "id" : "id"}' );
	}
	else {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 111, "message": "'.__( 'Unknown error.', '3dprint' ).'"}, "id" : "id"}' );
	}

}
function p3d_validate_analyse($post) {
	$settings = p3d_get_option( '3dp_settings' );

	if ((float)$post['layer_height']<=0) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"message": "'.__( 'Layer height should not be 0', '3dprint' ).'"}, "id" : "id"}' );
	}

	if ($post['printer_type']=='dlp') return;

	if ((float)$post['nozzle_size']<=0) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"message": "'.__( 'Nozzle size should not be 0', '3dprint' ).'"}, "id" : "id"}' );
	}
	if ((float)$post['wall_thickness']<=0) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"message": "'.__( 'Wall thickness should not be 0', '3dprint' ).'"}, "id" : "id"}' );
	}
	if ((float)$post['bottom_top_thickness']<=0) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"message": "'.__( 'Bottom/Top thickness should not be 0', '3dprint' ).'"}, "id" : "id"}' );
	}
	if ((float)$post['speed']<=0) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"message": "'.__( 'Print speed should not be 0', '3dprint' ).'"}, "id" : "id"}' );
	}
	if ((float)$post['travel_speed']<=0) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"message": "'.__( 'Travel speed should not be 0', '3dprint' ).'"}, "id" : "id"}' );
	}
}

function p3d_handle_analyse() {
	error_reporting( 0 );
	set_time_limit( 5*60 );
	ini_set( 'memory_limit', '-1' );

	do_action('p3d_handle_analyse_begin');

	$settings = p3d_get_option( '3dp_settings' );
	$servers = p3d_get_option( '3dp_servers' );
	shuffle($servers);
	$server = $servers[0];


	$api_url = $server."/analyse6.php";
	$uploads = wp_upload_dir();
	$uploads['path'] = $uploads['basedir'].'/p3d/';
	$basename = p3d_basename( $_POST['filename'] );
	$file_to_upload = $uploads['path'] . $basename;
	$product_id = (int)$_POST['product_id'];
	$printer_id = (int)$_POST['printer_id'];
	$material_id = (int)$_POST['material_id'];
	$printers_array = p3d_get_option( '3dp_printers' );
	$printer=$printers_array[$printer_id];
	$materials_array = p3d_get_option( '3dp_materials' );
	$material=$materials_array[$material_id];
	$p3d_file_url_meta = get_post_meta($product_id, 'p3d_file_url'); $p3d_file_url=$p3d_file_url_meta[0];
	$p3d_product_price_type_meta = get_post_meta($product_id, 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];
	

	if (strlen($p3d_file_url)>0) {
		$upload_dir = wp_upload_dir();
		$uploads = dirname($upload_dir['basedir']).'/'.dirname(substr($p3d_file_url, strpos($p3d_file_url, 'uploads/'))).'/';
		$basename = p3d_basename( $p3d_file_url );
		$file_to_upload = $uploads . $basename;
	}
	$layer_height = (float)$printer['layer_height'];
	$wall_thickness = (float)$printer['wall_thickness'];
	$bottom_top_thickness = (float)$printer['bottom_top_thickness'];
	$nozzle_size = (float)$printer['nozzle_size'];
	$speed = (float)$printer['speed'];
	$speed_type = $printer['speed_type'];
	$travel_speed = (float)$printer['travel_speed'];
	$infill_speed = (float)$printer['infill_speed'];
	$support = (int)$printer['support'];
	$support_type = (int)$printer['support_type'];
	$support_angle = (int)$printer['support_angle'];
	$filament_diameter = (float)$material['diameter'];
	$infill = (int)$_POST['infill'];
	$cookie = WC()->session->get_session_cookie();
	$session_id = md5($_SERVER['REMOTE_ADDR'].$cookie[0]);
	$scale = (float)$_POST['scale'];
	$unit = $_POST['unit'];
	$upload = true;
	$triangulation = $_POST['triangulation'];
	$api_analyse = $_POST['api_analyse'];
	$printer_type = $printer['type'];
	$time_per_layer = (float)printer['time_per_layer'];
	$perimeters = (int)$printer['perimeters'];
	$solid_layers_top = (int)$printer['solid_layers_top'];
	$solid_layers_bottom = (int)$printer['solid_layers_bottom'];
	$infill_pattern = $printer['infill_pattern'];
	$infill_pattern_top_bottom = $printer['infill_pattern_top_bottom'];
	$infill_speed = (int)$printer['infill_speed'];
	$bottom_layer_speed=(int)$printer['bottom_layer_speed'];
	$solidarea_speed=(int)$printer['solidarea_speed'];
	$outer_shell_speed=(int)$printer['outer_shell_speed'];
	$inner_shell_speed=(int)$printer['inner_shell_speed'];
	$cool_min_layer_time=(int)$printer['cool_min_layer_time'];

	$support_pattern_spacing = (float)$printer['support_pattern_spacing'];
	$support_bridges = (int)$printer['support_bridges'];
	$support_raft_layers = (int)$printer['support_raft_layers'];
	$support_z_distance = (float)$printer['support_z_distance'];
	$brim_width = (float)$printer['brim_width'];
    $zip_file = "";

	//todo check extension
	if ( !file_exists ( $file_to_upload ) ) wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 107, "message": "'.__( 'The file '.p3d_basename($file_to_upload).' does not exist.', '3dprint' ).'"}, "id" : "id"}' );

	if ( !function_exists('curl_version') ) wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 108, "message": "'.__( 'The server does not support curl.', '3dprint' ).'"}, "id" : "id"}' );


	//check if file already exists on a remote server
	$post = array( 'login' => $settings['api_login'], 'file_name' => $basename, 'file_key' => md5_file ( $file_to_upload ), 'check_existence' => 1);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "$server/check2.php");
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	if (PHP_VERSION < 7.0) {
		curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
	}
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result=curl_exec ($ch);




	if($errno = curl_errno($ch)) {
		$error_message = curl_strerror($errno);		
	}
	curl_close ($ch);
	if ( $errno ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 109, "message": "'.__( 'Error: '.$error_message, '3dprint' ).'"}, "id" : "id"}' );
	}
	$response = json_decode($result, true);
	if ($response['file_exists'] == '1') $upload = false;

	if ($upload) {
		if ( class_exists('ZipArchive') && filesize($file_to_upload) >= 1048576 ) {
			$zip_file = "$file_to_upload.tmp.zip";
			$zip = new ZipArchive();
			
			if ($zip->open($zip_file, ZipArchive::CREATE)!==TRUE) {
				wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 112, "message": "'.__( 'Could not create a zip archive.', '3dprint' ).'"}, "id" : "id"}' );
			}
			$zip->addFile($file_to_upload, $basename);
			$zip->close();
			$file_to_upload = $zip_file;

		}
	}


	$post = array(  'login' => $settings['api_login'], 
			'subscription_login' => $settings['api_subscription_login'], 
			'subscription_key' => $settings['api_subscription_key'],
			'slicer' => $settings['slicer'],
			'layer_height' => $layer_height,
			'time_per_layer' => $time_per_layer,
			'wall_thickness' => $wall_thickness,
			'bottom_top_thickness' => $bottom_top_thickness,
			'nozzle_size' => $nozzle_size,
			'filament_diameter' => $filament_diameter,
			'speed' => $speed,
			'speed_type' => $speed_type,
			'travel_speed' => $travel_speed,
			'bottom_layer_speed' => $bottom_layer_speed,
			'solidarea_speed' => $solidarea_speed,
			'outer_shell_speed' => $outer_shell_speed,
			'inner_shell_speed' => $inner_shell_speed,
			'cool_min_layer_time' => $cool_min_layer_time,
			'support' => $support,
			'support_type' => $support_type,
			'support_angle' => $support_angle,
			'infill' => $infill,
			'file_name' => $basename, 
			'file_key' => md5_file ( $file_to_upload ), 
			'file_contents' => curl_file_create($file_to_upload), 
			'session_id' => $session_id,
			'scale' => $scale,
			'unit' => $unit,
			'triangulation' => $triangulation, 
			'api_analyse' => $api_analyse,
			'printer_type' => $printer_type,
			'perimeters' => $perimeters,
			'solid_layers_top' => $solid_layers_top,
			'solid_layers_bottom' => $solid_layers_bottom,
			'infill_pattern' => $infill_pattern, 
			'infill_pattern_top_bottom' => $infill_pattern_top_bottom, 
			'infill_speed' => $infill_speed,
			'support_pattern_spacing' => $support_pattern_spacing,
			'support_bridges' => $support_bridges,
			'support_raft_layers' => $support_raft_layers,
			'support_z_distance' => $support_z_distance,
			'brim_width' => $brim_width
			);
	p3d_validate_analyse($post);
	if (!$upload) unset($post['file_contents']);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$api_url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	if (PHP_VERSION < 7.0) {
		curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
	}
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result=curl_exec ($ch);

#echo $result;

	if($errno = curl_errno($ch)) {
		$error_message = curl_strerror($errno);		
	}
	curl_close ($ch);
	if (file_exists($zip_file)) unlink($zip_file);
	if ( $errno ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 109, "message": "'.__( 'Error: '.$error_message, '3dprint' ).'"}, "id" : "id"}' );
	}

	$response = json_decode($result, true);


	if ( $response['status']=='2' ) {
		//analyse process is slow, it goes to background and we retrieve the status later with p3d_handle_check
		$output['jsonrpc'] = "2.0";
		$output['status'] = $response['status']; //2 - in progress
		$output['server'] = $server;
		wp_die( json_encode( $output ) );
	}
	elseif ( $response['status']=='1' ) {
		//success, currently happens only for triangulation process

		$p3d_triangulation_cache = get_option('3dp_triangulation_cache');
                $p3d_triangulation_cache[md5_file($uploads['path'].$basename)] = array();

		$p3d_triangulation_cache[md5_file($uploads['path'].$basename)]['model_filament']=$response['model_filament'];
		$p3d_triangulation_cache[md5_file($uploads['path'].$basename)]['surface_area']=$response['surface_area'];

		update_option('3dp_triangulation_cache', $p3d_triangulation_cache);
		wp_die( $result );
	}
	elseif ( $response['status']=='0' ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 110, "message": "'.__( $response['message'], '3dprint' ).'"}, "id" : "id"}' );
	}
	else {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 111, "message": "'.__( 'Unknown error.', '3dprint' ).'"}, "id" : "id"}' );
	}


}

function p3d_handle_analyse_check() {
	error_reporting( 0 );
	set_time_limit( 5*60 );
	ini_set( 'memory_limit', '-1' );
	do_action('p3d_handle_check_begin');
	$uploads = wp_upload_dir();
	$uploads['path'] = $uploads['basedir'].'/p3d/';
	$product_id = (int)$_POST['product_id'];
	$material_id = (int)$_POST['material_id'];
	$printer_id = (int)$_POST['printer_id'];

	$printers_array = p3d_get_option( '3dp_printers' );
	$printer=$printers_array[$printer_id];
	$materials_array = p3d_get_option( '3dp_materials' );
	$material=$materials_array[$material_id];

	$layer_height = (float)$printer['layer_height'];
	$wall_thickness = (float)$printer['wall_thickness'];
	$bottom_top_thickness = (float)$printer['bottom_top_thickness'];
	$infill = (int)$_POST['infill'];
	$printer_type = $printer['type'];
	$nozzle_size = $printer['nozzle_size'];
	$support = $printer['support'];
	$support_type = $printer['support_type'];
	$support_angle = $printer['support_angle'];
	$time_per_layer = (float)$printer['time_per_layer'];
	$scale = (float)$_POST['scale'];
	$unit = $_POST['unit'];


	$settings = p3d_get_option( '3dp_settings' );
	$servers = p3d_get_option( '3dp_servers' );
	$basename = p3d_basename( $_POST['filename'] );

	$p3d_file_url_meta = get_post_meta($product_id, 'p3d_file_url'); $p3d_file_url=$p3d_file_url_meta[0];
	$p3d_product_price_type_meta = get_post_meta($product_id, 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];

	if (strlen($p3d_file_url)>0) {
		$upload_dir = wp_upload_dir();
		$uploads['path'] = dirname($upload_dir['basedir']).'/'.dirname(substr($p3d_file_url, strpos($p3d_file_url, 'uploads/'))).'/';
		$basename = p3d_basename( $p3d_file_url );
	}

	if (!empty($_POST['server']) && in_array($_POST['server'], $servers)) $server = $_POST['server'];
	else 
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 114, "message": "'.__( 'Server not found.', '3dprint' ).'"}, "id" : "id"}' );


	$api_url = $server."/check2.php";

	$cookie = WC()->session->get_session_cookie();
	$session_id = md5($_SERVER['REMOTE_ADDR'].$cookie[0]);

	$post = array( 'login' => $settings['api_login'], 'subscription_key' => $settings['api_subscription_key'], 'slicer' => $settings['slicer'], 'file_name' => $basename, 'session_id' => $session_id, 'scale'=>$scale, 'printer_type' => $printer_type, 'layer_height' => $layer_height, 'time_per_layer' => $time_per_layer );

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $api_url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	if (PHP_VERSION < 7.0) {
		curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
	}
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result=curl_exec ($ch);


	if($errno = curl_errno($ch)) {
		$error_message = curl_strerror($errno);		
	}
	curl_close ($ch);
	if ( $errno ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 109, "message": "'.__( 'Error: '.$error_message, '3dprint' ).'"}, "id" : "id"}' );
	}
	$response = json_decode($result, true);

	if ( $response['status']=='1' )  {

		$output['jsonrpc'] = "2.0";
		$output['status'] = $response['status']; 
		$output['progress'] = $response['progress']; 
		$output['model_filament'] = $response['model_filament']; 
		$output['surface_area'] = $response['surface_area']; 
		$output['print_time'] = $response['print_time']; 

		$p3d_cache = get_option('3dp_cache');
                $p3d_cache[md5_file($uploads['path'].$basename)] = array();
#		$model_key = $layer_height."_".$wall_thickness."_".$bottom_top_thickness."_".$nozzle_size."_".$infill."_".$scale."_".$unit."_".$support."_".$support_type."_".$support_angle;
		$model_key = p3d_generate_model_key($printer, $infill, $scale, $unit);


		$p3d_cache[md5_file($uploads['path'].$basename)][$model_key]['model_filament']=$response['model_filament'];
		$p3d_cache[md5_file($uploads['path'].$basename)][$model_key]['surface_area']=$response['surface_area'];
		$p3d_cache[md5_file($uploads['path'].$basename)][$model_key]['print_time']=$response['print_time'];
		$p3d_cache[md5_file($uploads['path'].$basename)][$model_key]['date']=date('Y-m-d', time());
		$p3d_cache[md5_file($uploads['path'].$basename)][$model_key]['time']=time();

//		$p3d_cache = get_option('3dp_cache');

		//$daily_print_hours = get_option('');
/*
		$today_date = date('Y-m-d', time());
		$today_print_time = 0;

		foreach ($p3d_cache as $md5_file => $param) {
			foreach ($param as $info) {
				if ($info['date']!='') {
					if ( $info['date'] == $today_date) {
						$today_print_time+=$info['print_time'];
					}
				}
			}
		}

		if (($today_print_time/3600) > $settings['daily_hours']) {
			$output['status'] = 0; 
			$output['error'] = __('Come back later', '3dprint'); 
//			$output['message'] = '';
		}

*/

		update_option('3dp_cache', $p3d_cache);
		wp_die( json_encode( $output ) );
	}
	if ( $response['status']=='2' )  {
		$output['jsonrpc'] = "2.0";
		$output['progress'] = $response['progress']; 
		$output['status'] = $response['status']; //2 - in progress
		$output['file'] = $response['file']; //2 - in progress
		wp_die( json_encode( $output ) );
	}
	elseif ( $response['status']=='0' ) {
		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 110, "message": "'.__( $response['message'], '3dprint' ).'"}, "id" : "id"}' );
	}
	else {

		wp_die( '{"jsonrpc" : "2.0", "error" : {"code": 111, "message": "'.__( 'Unknown error.', '3dprint' ).'"}, "id" : "id"}' );
	}

}
/*
function wbs_adding_scripts() {

    wp_register_script( 'jquery-1.8.2', get_3dprint_url() . '/assets/lib/jquery-1.8.2.js' , array(), '', false );
    wp_register_script( 'jquery-ui-1.9.0.custom', get_3dprint_url() . '/assets/lib/jquery-ui-1.9.0.custom.js' , array(), '', false );
    wp_register_script( 'codemirror', get_3dprint_url() . '/assets/lib/codemirror.js' , array(), '', false );
    wp_register_script( 'mode_gcode/gcode_mode', get_3dprint_url() . '/assets/lib/mode_gcode/gcode_mode.js' , array(), '', false );
    wp_register_script( 'three', get_3dprint_url() . '/assets/lib/three.js' , array(), '', false );
    wp_register_script( 'bootstrap', get_3dprint_url() . '/assets/lib/bootstrap.js' , array(), '', false );
    wp_register_script( 'modernizr.custom.09684', get_3dprint_url() . '/assets/lib/modernizr.custom.09684.js' , array(), '', false );
    wp_register_script( 'jquery-ui-1.9.0.custom', get_3dprint_url() . '/assets/lib/jquery-ui-1.9.0.custom.js' , array(), '', false );
    wp_register_script( 'TrackballControls', get_3dprint_url() . '/assets/lib/TrackballControls.js' , array(), '', false );
    wp_register_script( 'zlib.min', get_3dprint_url() . '/assets/lib/zlib.min.js' , array(), '', false );


    wp_register_script( 'ui', get_3dprint_url() . '/assets/js/ui.js' , array(), '', false );
    wp_register_script( 'gCodeReader', get_3dprint_url() . '/assets/js/gCodeReader.js' , array(), '', false );
    wp_register_script( 'renderer', get_3dprint_url() . '/assets/js/renderer.js' , array(), '', false );
    wp_register_script( 'analyzer', get_3dprint_url() . '/assets/js/analyzer.js', array(), '', false );
    wp_register_script( 'renderer3d', get_3dprint_url() . '/assets/js/renderer3d.js' , array(), '', false );
    //wp_register_script( 'Worker', get_3dprint_url() . '/assets/js/Worker.js' , array(), '', false );

    wp_enqueue_script( 'jquery-1.8.2');
    wp_enqueue_script('jquery-ui-1.9.0.custom');
    wp_enqueue_script( 'codemirror');
    wp_enqueue_script( 'mode_gcode/gcode_mode');
    wp_enqueue_script( 'three');
    wp_enqueue_script( 'bootstrap');
    wp_enqueue_script( 'modernizr.custom.09684');
    wp_enqueue_script( 'TrackballControls');
    wp_enqueue_script( 'zlib.min');

    wp_enqueue_script( 'ui');
    wp_enqueue_script( 'gCodeReader');
    wp_enqueue_script( 'renderer');
    wp_enqueue_script( 'analyzer');
    wp_enqueue_script( 'renderer3d');
    //wp_enqueue_script( 'Worker');

}
function wbs_adding_styles()
{
    wp_enqueue_style('style-gcode', get_3dprint_url() . '/assets/css/style-gcode.css', '', 1);
    wp_enqueue_style('bootstrap', get_3dprint_url() . '/assets/css/bootstrap.css', '', 1);
    wp_enqueue_style('bootstrap.min', get_3dprint_url() . '/assets/css/bootstrap.min.css', '', 1);
    wp_enqueue_style('bootstrap-responsive', get_3dprint_url() . '/assets/css/bootstrap-responsive.css', '', 1);
    wp_enqueue_style('bootstrap-responsive.min', get_3dprint_url() . '/assets/css/bootstrap-responsive.min.css', '', 1);
}
add_action( 'wp_enqueue_style', 'wbs_adding_styles' );
add_action( 'wp_enqueue_scripts', 'wbs_adding_scripts' );
*/
?>