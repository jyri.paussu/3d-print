<?php
/**
 *
 *
 * @author Sergey Burkov, http://www.wp3dprinting.com
 * @copyright 2015
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}



add_action( 'admin_menu', 'register_3dprint_menu_page' );
function register_3dprint_menu_page() {
	add_menu_page( '3DPrint', '3DPrint', 'manage_options', '3dprint', 'register_3dprint_menu_page_callback' );
}

function p3d_handle_shortcode_generation() {
	//https://developer.ninjaforms.com/codex/forms/


	$form = Ninja_Forms()->form()->get();
	$form_settings = $form->get_settings();
	$form_settings['title'] = __('Get a Quote', '3dprint');
	$form_settings['show_title'] = 1;
	$form_settings['clear_complete'] = 1;
	$form_settings['hide_complete'] = 0;
	$form_settings['add_submit'] = 1;


	$form->update_settings( $form_settings )->save();
	$form_id = $form->get_id();

	$field_settings = array(
		'type' => 'textbox',
		'key' => 'name',
		'label' => __('Name', '3dprint'),
		'label_pos' => 'above',
		'required' => 1,
		'order' => 1
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'email',
		'key' => 'email',
		'label' => __('Email', '3dprint'),
		'label_pos' => 'above',
		'required' => 1,
		'order' => 2
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'textarea',
		'key' => 'message',
		'label' => __('Message', '3dprint'),
		'label_pos' => 'above',
		'required' => 0,
		'order' => 3
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'submit',
		'key' => 'submit',
		'label' => __('Submit', '3dprint'),
		'processing_label' => __('Processing', '3dprint'),
		'order' => 4
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();


	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Printer', '3dprint'),
		'key' => 'nf_p3d_printer',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Material', '3dprint'),
		'key' => 'nf_p3d_material',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Coating', '3dprint'),
		'key' => 'nf_p3d_coating',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Product ID', '3dprint'),
		'key' => 'nf_p3d_product_id',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Scale', '3dprint'),
		'key' => 'nf_p3d_resize_scale',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Unit', '3dprint'),
		'key' => 'nf_p3d_unit',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Infill', '3dprint'),
		'key' => 'nf_p3d_infill',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Model', '3dprint'),
		'key' => 'nf_p3d_model',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Thumnbail', '3dprint'),
		'key' => 'nf_p3d_thumbnail',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Dimension X', '3dprint'),
		'key' => 'nf_p3d_dim_x',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Dimension Y', '3dprint'),
		'key' => 'nf_p3d_dim_y',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Dimension Z', '3dprint'),
		'key' => 'nf_p3d_dim_z',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Dimensions', '3dprint'),
		'key' => 'nf_p3d_dimensions',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Estimated Price', '3dprint'),
		'key' => 'nf_p3d_estimated_price',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$field_settings = array(
		'type' => 'hidden',
		'label' => __('Original Model', '3dprint'),
		'key' => 'nf_p3d_original_model',
		'order' => 999
	); 
	$field = Ninja_Forms()->form($form_id)->field()->get();
	$field->update_settings( $field_settings )->save();

	$nf_shortcode="[ninja_form id=$form_id]";

	$output['shortcode']=$nf_shortcode;
	$output['shortcode_edit_url']=admin_url( "admin.php?page=ninja-forms&form_id=$form_id" );
	wp_die(json_encode($output));
}

function register_3dprint_menu_page_callback() {
	global $wpdb, $woocommerce;
	if ( $_GET['page'] != '3dprint' ) return false;
	if ( !current_user_can('administrator') ) return false;

	$settings=p3d_get_option( '3dp_settings' );



	if ( isset( $_POST['action'] ) ) {
		update_option('3dp_cache', array());
	}

	if ( isset( $_POST['action'] ) && $_POST['action']=='save_login' ) {
		$settings['api_login']=sanitize_text_field($_POST['api_login']);
		update_option( '3dp_settings', $settings );
	}

	if ( isset( $_POST['action'] ) && $_POST['action']=='remove_printer' ) {
		$wpdb->delete( $wpdb->prefix."p3d_printers", array('id'=>$_POST['printer_id']) );
	}

	if ( isset( $_POST['action'] ) && $_POST['action']=='remove_material' ) {
		$wpdb->delete( $wpdb->prefix."p3d_materials", array('id'=>$_POST['material_id']) );
	}

	if ( isset( $_POST['action'] ) && $_POST['action']=='remove_coating' ) {
		$wpdb->delete( $wpdb->prefix."p3d_coatings", array('id'=>$_POST['coating_id']) );
	}

	if ( isset( $_POST['action'] ) && $_POST['action']=='remove_request' ) {
		$price_requests=p3d_get_option( '3dp_price_requests' );
		unset( $price_requests[$_POST['request_id']] );
		update_option( '3dp_price_requests', $price_requests );
	}

	if ( isset( $_POST['3dp_printer_name'] ) && count( $_POST['3dp_printer_name'] )>0 ) {
		foreach ( $_POST['3dp_printer_name'] as $printer_id => $printer ) {
			if (empty($_POST['3dp_printer_name'][$printer_id])) continue;
			$printers[$printer_id]['id']=$printer_id;
			$printers[$printer_id]['status']= (int)$_POST['3dp_printer_status'][$printer_id] ;
			$printers[$printer_id]['name']=sanitize_text_field( $_POST['3dp_printer_name'][$printer_id] );
			$printers[$printer_id]['description']=sanitize_text_field( stripslashes($_POST['3dp_printer_description'][$printer_id]) );
			$printers[$printer_id]['photo']=sanitize_text_field( str_replace('http:','',$_POST['3dp_printer_photo'][$printer_id]) );
			$printers[$printer_id]['type'] = $_POST['3dp_printer_type'][$printer_id];
			$printers[$printer_id]['full_color']= (int)$_POST['3dp_printer_full_color'][$printer_id] ;
			$printers[$printer_id]['platform_shape'] = $_POST['3dp_printer_platform_shape'][$printer_id];
			$printers[$printer_id]['diameter'] = (float)$_POST['3dp_printer_platform_diameter'][$printer_id];
			$printers[$printer_id]['width']=(float)( $_POST['3dp_printer_width'][$printer_id] );
			$printers[$printer_id]['length']=(float)( $_POST['3dp_printer_length'][$printer_id] );
			$printers[$printer_id]['height']=(float)( $_POST['3dp_printer_height'][$printer_id] );
			$printers[$printer_id]['min_side']=(float)( $_POST['3dp_printer_min_side'][$printer_id] );
			$printers[$printer_id]['layer_height']=(float)( $_POST['3dp_printer_layer_height'][$printer_id] );
			$printers[$printer_id]['time_per_layer']=(float)( $_POST['3dp_printer_time_per_layer'][$printer_id] );
			$printers[$printer_id]['wall_thickness']=(float)( $_POST['3dp_printer_wall_thickness'][$printer_id] );
			$printers[$printer_id]['bottom_top_thickness']=(float)( $_POST['3dp_printer_bottom_top_thickness'][$printer_id] );
			$printers[$printer_id]['nozzle_size']=(float)( $_POST['3dp_printer_nozzle_size'][$printer_id] );
			$printers[$printer_id]['price']= p3d_fix_price($_POST['3dp_printer_price'][$printer_id]) ;
			$printers[$printer_id]['price_type']=$_POST['3dp_printer_price_type'][$printer_id];
			$printers[$printer_id]['price1']= p3d_fix_price($_POST['3dp_printer_price1'][$printer_id]) ;
			$printers[$printer_id]['price_type1']=$_POST['3dp_printer_price_type1'][$printer_id];
			$printers[$printer_id]['price2']= p3d_fix_price($_POST['3dp_printer_price2'][$printer_id]) ;
			$printers[$printer_id]['price_type2']=$_POST['3dp_printer_price_type2'][$printer_id];
			$printers[$printer_id]['price3']= p3d_fix_price($_POST['3dp_printer_price3'][$printer_id]) ;
			$printers[$printer_id]['price_type3']=$_POST['3dp_printer_price_type3'][$printer_id];
			$printers[$printer_id]['price4']= p3d_fix_price($_POST['3dp_printer_price4'][$printer_id]) ;
			$printers[$printer_id]['price_type4']=$_POST['3dp_printer_price_type4'][$printer_id];
			$printers[$printer_id]['speed']= (int)$_POST['3dp_printer_speed'][$printer_id] ;
			$printers[$printer_id]['speed_type']=$_POST['3dp_printer_speed_type'][$printer_id];
			$printers[$printer_id]['travel_speed']= (int)$_POST['3dp_printer_travel_speed'][$printer_id] ;
			$printers[$printer_id]['bottom_layer_speed']= (int)$_POST['3dp_printer_bottom_layer_speed'][$printer_id] ;
			$printers[$printer_id]['solidarea_speed']= (int)$_POST['3dp_printer_solidarea_speed'][$printer_id] ;
			$printers[$printer_id]['outer_shell_speed']= (int)$_POST['3dp_printer_outer_shell_speed'][$printer_id] ;
			$printers[$printer_id]['inner_shell_speed']= (int)$_POST['3dp_printer_inner_shell_speed'][$printer_id] ;
			$printers[$printer_id]['cool_min_layer_time']= (int)$_POST['3dp_printer_cool_min_layer_time'][$printer_id] ;
			$printers[$printer_id]['support']=$_POST['3dp_printer_support'][$printer_id];
			$printers[$printer_id]['support_type']=$_POST['3dp_printer_support_type'][$printer_id];
			$printers[$printer_id]['support_angle']=(int)$_POST['3dp_printer_support_angle'][$printer_id];
			$printers[$printer_id]['perimeters']=$_POST['3dp_printer_perimeters'][$printer_id];
			$printers[$printer_id]['solid_layers_top']=(int)$_POST['3dp_printer_solid_layers_top'][$printer_id];
			$printers[$printer_id]['solid_layers_bottom']=(int)$_POST['3dp_printer_solid_layers_bottom'][$printer_id];
			$printers[$printer_id]['infill_pattern']=$_POST['3dp_printer_infill_pattern'][$printer_id];
			$printers[$printer_id]['infill_pattern_top_bottom']=$_POST['3dp_printer_infill_pattern_top_bottom'][$printer_id];
			$printers[$printer_id]['infill_speed']=(int)$_POST['3dp_printer_infill_speed'][$printer_id];
			$printers[$printer_id]['support_pattern_spacing']=(float)$_POST['3dp_printer_support_pattern_spacing'][$printer_id];
			$printers[$printer_id]['support_bridges']=(int)$_POST['3dp_printer_support_bridges'][$printer_id];
			$printers[$printer_id]['support_raft_layers']=(int)$_POST['3dp_printer_support_raft_layers'][$printer_id];
			$printers[$printer_id]['support_z_distance']=(float)$_POST['3dp_printer_support_z_distance'][$printer_id];
			$printers[$printer_id]['brim_width']=(float)$_POST['3dp_printer_brim_width'][$printer_id];
			$printers[$printer_id]['brim_width']=(float)$_POST['3dp_printer_brim_width'][$printer_id];
			$printers[$printer_id]['power_tariff']=(float)$_POST['3dp_printer_power_tariff'][$printer_id];
			$printers[$printer_id]['printer_power']=(float)$_POST['3dp_printer_power'][$printer_id];
			$printers[$printer_id]['printer_purchase_price']=(float)$_POST['3dp_printer_purchase_price'][$printer_id];
			$printers[$printer_id]['printer_lifetime']=(float)$_POST['3dp_printer_lifetime'][$printer_id];
			$printers[$printer_id]['printer_daily_usage']=(float)$_POST['3dp_printer_daily_usage'][$printer_id];
			$printers[$printer_id]['printer_repair_cost']=(float)$_POST['3dp_printer_repair_cost'][$printer_id];
			$printers[$printer_id]['printer_energy_hourly_cost']=(float)$_POST['3dp_printer_energy_hourly_cost'][$printer_id];
			$printers[$printer_id]['printer_depreciation_hourly_cost']=(float)$_POST['3dp_printer_depreciation_hourly_cost'][$printer_id];
			$printers[$printer_id]['printer_repair_hourly_cost']=(float)$_POST['3dp_printer_repair_hourly_cost'][$printer_id];





			if (isset($_FILES['3dp_printer_photo_upload']['tmp_name'][$printer_id]) && strlen($_FILES['3dp_printer_photo_upload']['tmp_name'][$printer_id])>0) {
				$uploaded_file = p3d_upload_file('3dp_printer_photo_upload', $printer_id);
				$printers[$printer_id]['photo']=str_replace('http:','',$uploaded_file['url']);
			}

			if ( isset($_POST['3dp_printer_materials']) && isset($_POST['3dp_printer_materials'][$printer_id]) && count( $_POST['3dp_printer_materials'][$printer_id] )>0 ) {
				$printers[$printer_id]['materials']=implode(',',$_POST['3dp_printer_materials'][$printer_id]);
			}

			if ( isset($_POST['3dp_printer_infills']) && isset($_POST['3dp_printer_infills'][$printer_id]) && count( $_POST['3dp_printer_infills'][$printer_id] )>0 ) {
				$printers[$printer_id]['infills']=implode(',',$_POST['3dp_printer_infills'][$printer_id]);
			}
			else {
				$printers[$printer_id]['infills']='';
			}

			$printers[$printer_id]['default_infill']= $_POST['3dp_printer_default_infill'][$printer_id] ;
			$printers[$printer_id]['sort_order']=(int)$_POST['3dp_printer_sort_order'][$printer_id];
			$printers[$printer_id]['group_name']=trim($_POST['3dp_printer_group_name'][$printer_id]);
		}

		foreach ($printers as $printer) {

			p3d_update_option( '3dp_printers', $printer );
		}

	}

	if ( isset( $_POST['3dp_material_name'] ) && count( $_POST['3dp_material_name'] )>0 ) {

		foreach ( $_POST['3dp_material_name'] as $material_id => $material ) {

			if (empty($_POST['3dp_material_name'][$material_id])) continue;

			$materials[$material_id]['density']=(float)$_POST['3dp_material_density'][$material_id];
			$materials[$material_id]['id'] = $material_id;
			$materials[$material_id]['status']=(int)$_POST['3dp_material_status'][$material_id];
			$materials[$material_id]['name']=sanitize_text_field( $_POST['3dp_material_name'][$material_id] );
			$materials[$material_id]['description']=sanitize_text_field( stripslashes($_POST['3dp_material_description'][$material_id]) );
			$materials[$material_id]['photo']=sanitize_text_field( str_replace('http:','',$_POST['3dp_material_photo'][$material_id]) );
			$materials[$material_id]['type'] = $_POST['3dp_material_type'][$material_id];
			$materials[$material_id]['diameter']=(float)( $_POST['3dp_material_diameter'][$material_id] );
			$materials[$material_id]['length']=(float)( $_POST['3dp_material_length'][$material_id] );
			$materials[$material_id]['weight']=(float)( $_POST['3dp_material_weight'][$material_id] );
			$materials[$material_id]['price']= p3d_fix_price($_POST['3dp_material_price'][$material_id]) ;
			$materials[$material_id]['price_type']=$_POST['3dp_material_price_type'][$material_id];
			$materials[$material_id]['price1']= p3d_fix_price($_POST['3dp_material_price1'][$material_id]) ;
			$materials[$material_id]['price_type1']=$_POST['3dp_material_price_type1'][$material_id];
			$materials[$material_id]['price2']= p3d_fix_price($_POST['3dp_material_price2'][$material_id]) ;
			$materials[$material_id]['price_type2']=$_POST['3dp_material_price_type2'][$material_id];
			$materials[$material_id]['roll_price']=(float)( $_POST['3dp_material_roll_price'][$material_id] );
			$materials[$material_id]['color']=$_POST['3dp_material_color'][$material_id];                      
			$materials[$material_id]['shininess']=$_POST['3dp_material_shininess'][$material_id];
			$materials[$material_id]['transparency']=$_POST['3dp_material_transparency'][$material_id];
			$materials[$material_id]['glow']=(int)$_POST['3dp_material_glow'][$material_id];
			$materials[$material_id]['sort_order']=(int)$_POST['3dp_material_sort_order'][$material_id];
			$materials[$material_id]['group_name']=trim($_POST['3dp_material_group_name'][$material_id]);

			if (isset($_FILES['3dp_material_photo_upload']['tmp_name'][$material_id]) && strlen($_FILES['3dp_material_photo_upload']['tmp_name'][$material_id])>0) {
				$uploaded_file = p3d_upload_file('3dp_material_photo_upload', $material_id);
				$materials[$material_id]['photo']=str_replace('http:','',$uploaded_file['url']);
			}
		}

		foreach ($materials as $material) {
			p3d_update_option( '3dp_materials', $material );
		}



	}

	if ( isset( $_POST['3dp_coating_name'] ) && count( $_POST['3dp_coating_name'] )>0 ) {

		foreach ( $_POST['3dp_coating_name'] as $coating_id => $coating ) {
			if (empty($_POST['3dp_coating_name'][$coating_id])) continue;
			$coatings[$coating_id]['id']=$coating_id;
			$coatings[$coating_id]['status']=(int)$_POST['3dp_coating_status'][$coating_id];
			$coatings[$coating_id]['name']=sanitize_text_field( $_POST['3dp_coating_name'][$coating_id] );
			$coatings[$coating_id]['description']=sanitize_text_field( stripslashes($_POST['3dp_coating_description'][$coating_id]) );
			$coatings[$coating_id]['photo']=sanitize_text_field( str_replace('http:','',$_POST['3dp_coating_photo'][$coating_id]) );
			$coatings[$coating_id]['price']=p3d_fix_price($_POST['3dp_coating_price'][$coating_id]);
			$coatings[$coating_id]['price_type']=$_POST['3dp_coating_price_type'][$coating_id];
			$coatings[$coating_id]['price1']=p3d_fix_price($_POST['3dp_coating_price1'][$coating_id]);
			$coatings[$coating_id]['price_type1']=$_POST['3dp_coating_price_type1'][$coating_id];
			$coatings[$coating_id]['color']=$_POST['3dp_coating_color'][$coating_id];
			$coatings[$coating_id]['shininess']=$_POST['3dp_coating_shininess'][$coating_id];
			$coatings[$coating_id]['glow']=$_POST['3dp_coating_glow'][$coating_id];
			$coatings[$coating_id]['transparency']=$_POST['3dp_coating_transparency'][$coating_id];
			if ( isset($_POST['3dp_coating_materials']) && count( $_POST['3dp_coating_materials'][$coating_id] )>0 ) {
				$coatings[$coating_id]['materials']=implode(',',$_POST['3dp_coating_materials'][$coating_id]);
			}
			$coatings[$coating_id]['sort_order']=(int)$_POST['3dp_coating_sort_order'][$coating_id];
			$coatings[$coating_id]['group_name']=trim($_POST['3dp_coating_group_name'][$coating_id]);

			if (isset($_FILES['3dp_coating_photo_upload']['tmp_name'][$coating_id]) && strlen($_FILES['3dp_coating_photo_upload']['tmp_name'][$coating_id])>0) {
				$uploaded_file = p3d_upload_file('3dp_coating_photo_upload', $coating_id);
				$coatings[$coating_id]['photo']=str_replace('http:','',$uploaded_file['url']);
			}

		}
		foreach ($coatings as $coating) {
			p3d_update_option( '3dp_coatings', $coating );
		}
	}


	if ( isset( $_POST['3dp_settings'] ) && !empty( $_POST['3dp_settings'] ) ) {
	        $settings_update = array_map('sanitize_text_field', $_POST['3dp_settings']);

		if (isset($_FILES['3dp_settings']['tmp_name']['ajax_loader']) && strlen($_FILES['3dp_settings']['tmp_name']['ajax_loader'])>0) {
			$uploaded_file = p3d_upload_file('3dp_settings', 'ajax_loader');
			$settings_update['ajax_loader']=str_replace('http:','',$uploaded_file['url']);
		}
		else {
			$settings_update['ajax_loader']=$settings['ajax_loader'];
		}


		update_option( '3dp_settings', $settings_update );
	}

	if ( isset( $_POST['3dp_email_templates'] ) && !empty( $_POST['3dp_email_templates'] ) ) {
	        $templates_update = array_map('wp_kses_post', $_POST['3dp_email_templates']);
	        $templates_update = array_map('nl2br', $templates_update);

		update_option( '3dp_email_templates', $templates_update );
	}


	if ( isset( $_POST['p3d_buynow'] ) && count( $_POST['p3d_buynow'] )>0 ) {

		foreach ( $_POST['p3d_buynow'] as $key=>$price ) {
			list ( $product_id, $printer_id, $material_id, $coating_id, $infill, $unit, $scale, $email_address, $base64_filename ) = explode( '_', $key );
			$price_requests=p3d_get_option( '3dp_price_requests' );
			if (!is_numeric($product_id)) { //new way
				$price_request=$price_requests[$key];
				$product_id = $price_request['product_id'];
				$printer_id = $price_request['attributes']['attribute_pa_p3d_printer'];
				$material_id = $price_request['attributes']['attribute_pa_p3d_material'];
				$coating_id = $price_request['attributes']['attribute_pa_p3d_coating'];
				$infill = $price_request['attributes']['attribute_pa_p3d_infill'];
				$unit = $price_request['attributes']['attribute_pa_p3d_unit'];
				$scale = $price_request['scale'];
				$email_address = $price_request['email_address'];
				$filename = $price_request['model_file'];

			}
			else {
				$filename = base64_decode( $base64_filename );
			}
			$product = new WC_Product_Variable( $product_id );

			$comments = $_POST['p3d_comments'][$key];
			$custom_attributes = '';
			$message = '';


			if ( count( $price_requests ) ) {
				$email=$price_requests[$key]['email'];
				$variation=$price_requests[$key]['attributes'];
				$variation['attribute_pa_p3d_model']=rawurlencode( $variation['attribute_pa_p3d_model'] );

				$query = parse_url( $product->get_permalink( $product_id ), PHP_URL_QUERY );

				if ( $query ) {
					$product_url=$product->get_permalink( $product_id ).'&'.p3d_query_string( $variation ).'p3d_buynow=1';
				}
				else {
					$product_url=$product->get_permalink( $product_id ).'?safety=1&'.p3d_query_string( $variation ).'p3d_buynow=1';
				}

				$product_url.="&unit=$unit&scale=$scale&email_address=$email_address";
				if ( $price ) {
					//echo $product_url;
					$price_requests[$key]['price']=$price;


					$db_printers=p3d_get_option( '3dp_printers' );
					$db_materials=p3d_get_option( '3dp_materials' );
					$db_coatings=p3d_get_option( '3dp_coatings' );

					$current_templates = get_option( '3dp_email_templates' );
					$template_body = $current_templates['client_email_body'];
					$template_subject = $current_templates['client_email_subject'];
					$from = $current_templates['client_email_from'];


					$upload_dir = wp_upload_dir();
					$link = $upload_dir['baseurl'] ."/p3d/".rawurlencode($filename);
					$subject=__( "Your model's price" , '3dprint' );


					$p3d_file_url_meta = get_post_meta($product_id, 'p3d_file_url'); $p3d_file_url=$p3d_file_url_meta[0];
					$p3d_product_price_type_meta = get_post_meta($product_id, 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];
					if (strlen($p3d_file_url)>0) {
						$original_link = $p3d_file_url;
						$p3d_original_file_url_meta = get_post_meta($product_id, 'p3d_original_file_url'); 
						if (strlen($p3d_original_file_url_meta[0])>0) $original_link=$p3d_original_file_url_meta[0];
						$model_link = "<a href='".$original_link."'>".p3d_basename($original_link)."</a>";
					}
					else {
						$model_link = "<a href='".$link."'>".$filename."</a>";
					}

					$dimensions=$price_requests[$key]['scale_x']." &times; ".$price_requests[$key]['scale_y']." &times; ".$price_requests[$key]['scale_z']." ".__('cm', '3dprint')."<br>";

					foreach ( $variation as $key => $value ) {
						if ( strpos( $key, 'attribute_' )===0 ) {
							$product_attributes=( $product->get_attributes() );
							$attribute_id=str_replace( 'attribute_', '', $key );
							if ( !strstr( $key, 'p3d_' ) ) {
								$attr_name=wc_attribute_label(str_replace('attribute_', '', $key ));
								$attr_value=p3d_attribute_slug_to_title($product_attributes[$attribute_id]['name'], $value);
								$custom_attributes.=$attr_name.": $attr_value <br>";
							}
						}
					}



					$buy_now = "<a href='".$product_url."'>".$product_url."</a>";

					$replace_from = array('[printer_name]', '[infill]', '[material_name]', '[coating_name]', '[model_file]', '[dimensions]','[custom_attributes]', '[price]', '[buy_link]', '[admin_comments]');
					$replace_to = array ($db_printers[$printer_id]['name'], (isset($infill) ? "$infill %" : ''), $db_materials[$material_id]['name'], 
							   (isset($db_coatings[$coating_id]['name']) ? $db_coatings[$coating_id]['name'] : ''), $model_link, $dimensions, $custom_attributes,
							    wc_price($price), $buy_now, $comments);
					$subject=str_ireplace($replace_from, $replace_to, $template_subject);
					$body=str_ireplace($replace_from, $replace_to, $template_body);


					do_action('3dprint_send_quote', $message);
					$headers = array();
					$headers[] = "From: $from";
					$headers[] = 'Content-Type: text/html; charset=UTF-8';


					if (wp_mail( $email, $subject, $body, $headers )) {
						update_option( '3dp_price_requests', $price_requests );
					} else {
						echo '<div class="error"><p>' . __('Could not email the quote(s)! Check if your wordpress site can send emails.' ,'3dprint').'</p></div>';
					}
				}
				
			}//if ( count( $price_requests ) ) 
		}//foreach ( $_POST['p3d_buynow'] as $key=>$price )

		do_action('3dprint_after_send_quotes');
	}//if ( isset( $_POST['p3d_buynow'] ) && count( $_POST['p3d_buynow'] )>0 )

	$printers=p3d_get_option( '3dp_printers' );
	$materials=p3d_get_option( '3dp_materials' );
	$coatings=p3d_get_option( '3dp_coatings' );
	$paginated_printers=p3d_get_option( '3dp_printers', true );

	$paginated_materials=p3d_get_option( '3dp_materials', true );
	$paginated_coatings=p3d_get_option( '3dp_coatings', true );

	$settings=p3d_get_option( '3dp_settings' );
	$price_requests=p3d_get_option( '3dp_price_requests' );
	$current_templates = get_option( '3dp_email_templates' );

	$shortcode_atts = shortcode_parse_atts( $settings['ninjaforms_shortcode'] );
	if (isset($shortcode_atts['id']))
		$form_id = (int)$shortcode_atts['id'];
	else	
		$form_id = 0;

	add_thickbox(); 

	if (count($_POST)) {

		foreach ($materials as $key => $material) {
			wp_set_object_terms( 0, strval($key), 'pa_p3d_material' , false );
		}
		foreach ($printers as $key => $printer) {
			wp_set_object_terms( 0, strval($key), 'pa_p3d_printer' , false );
		}
		foreach ($coatings as $key => $coating) {
			wp_set_object_terms( 0, strval($key), 'pa_p3d_coating' , false );
		}

	}

#			wp_set_object_terms( $post_id, 'all', 'pa_p3d_coating' , false );
#			$materials=p3d_get_option( '3dp_materials' );
#			print_r($materials);
#	p3d_check_install();
/*	$today_print_time=0;
	$p3d_cache = get_option('3dp_cache');


	$today_date = date('Y-m-d', time());
		foreach ($p3d_cache as $md5_file => $param) {
			foreach ($param as $info) {
				if ($info['date']!='') {

					if ( $info['date'] == $today_date) {

						$today_print_time+=$info['print_time'];
					}
				}
			}
		}

*/


?>
<script language="javascript">
var p3d_ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
function p3dGenerateNinjaFormsShortcode () {
	jQuery('#p3d-generate-image').css('visibility', 'visible');
	jQuery('#p3d-generate-button').hide();
	jQuery.ajax({
		method: "POST",
		type: "POST",
		url: p3d_ajax_url,
		data: { 
			action: "p3d_handle_shortcode_generation", 
		      }
		})
		.done(function( msg ) {
			jQuery('#p3d-generate-image').css('visibility', 'hidden');
			jQuery('#p3d-generate-button').show();
			var data = jQuery.parseJSON( msg );
			if (typeof(data.shortcode)!=='undefined') {
				jQuery('#p3d-ninjaforms-shortcode').val(data.shortcode);
				jQuery('#p3d-shortcode-edit').prop('href', data.shortcode_edit_url);
				alert("<?php _e("Success! Don't forget to save the changes.", '3dprint');?>");
			}
			else {
				alert('<?php _e('Could not generate shortcode!', '3dprint');?>');
			}
		})
}

function p3dCalculateFilamentPrice(material_obj, price_id) {
	var diameter=parseFloat(jQuery(material_obj).closest('table.material').find('input.3dp_diameter').val());
	var length=parseFloat(jQuery(material_obj).closest('table.material').find('input.3dp_length').val());
	var weight=parseFloat(jQuery(material_obj).closest('table.material').find('input.3dp_weight').val());
	var price=parseFloat(jQuery(material_obj).closest('table.material').find('input.3dp_roll_price').val());
	var price_type=jQuery(material_obj).closest('table.material').find('select.3dp_price_type'+price_id).val();

	if (price_type=='cm3') {
		if (!diameter || !price || !length) {alert('<?php _e( 'Please input roll price, diameter and length', '3dprint' );?>');return false;}
		var volume=(Math.PI*((diameter*diameter)/4)*(length*1000))/1000;
		var volume_cost=price/volume;
		jQuery(material_obj).closest('table.material').find('input.3dp_price'+price_id).val(volume_cost.toFixed(2));
	}
	else if (price_type=='gram') {
	
		if (!weight || !price) {alert('<?php _e( 'Please input roll price and weight', '3dprint' );?>');return false;}
		var weight_cost=price/(weight*1000);
		jQuery(material_obj).closest('table.material').find('input.3dp_price'+price_id).val(weight_cost.toFixed(2));
	}

}

function p3dCalculateFilamentDensity(material_obj) {
	var diameter=parseFloat(jQuery(material_obj).closest('table.material').find('input.3dp_diameter').val());
	var length=parseFloat(jQuery(material_obj).closest('table.material').find('input.3dp_length').val());
	var weight=parseFloat(jQuery(material_obj).closest('table.material').find('input.3dp_weight').val());

	if (!diameter || !weight || !length) {alert('<?php _e( 'Please input diameter, length and weight', '3dprint' );?>');return false;}
	var density = parseFloat( ( weight*1000 )/( Math.PI*( Math.pow( diameter, 2 )/4 )*length ) ).toFixed(2);
	jQuery(material_obj).closest('table.material').find('input[name^=3dp_material_density]').val(density);
}
<?php
if (strlen($settings['api_login'])==0) {
?>
jQuery( function() {
    jQuery( "#p3d_wizard" ).dialog({
      modal: true
    });
  } );
<?php
}
?>

<?php
if ($_POST['action']=='save_login') {
?>
jQuery( function() {
    jQuery( "#p3d_howto" ).dialog({
      modal: true,
      width: 900,
      height: 600
    });
  } );

<?php
}
?>
</script>

<div id="p3d_wizard" style="display:none;">
<form action="" method="post">
<p>
	<?php _e('Please input the e-mail with which you ordered the plugin. It is needed for getting plugin updates.', '3dprint'); ?>
</p>
<input type="hidden" name="action" value="save_login">
<input type="text" name="api_login" value="" placeholder='user@example.com'><br>
<input type="submit" value="<?php _e('Save', '3dprint');?>">

</form>
</div>

<div id="p3d_howto" style="display:none;">
<p><?php _e('This video will guide you how to set up your first 3DPrint product!', '3dprint');?></p>
<iframe width="854" height="480" src="https://www.youtube.com/embed/ZMziju_Cueg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
<div class="wrap">
	<h2><?php _e( '3D printing settings', '3dprint' );?></h2>

	<div id="3dp_tabs">

		<ul>
			<li><a href="#3dp_tabs-0"><?php _e( 'Settings', '3dprint' );?></a></li>
			<li><a href="#3dp_tabs-1"><?php _e( 'Printers', '3dprint' );?></a></li>
			<li><a href="#3dp_tabs-2"><?php _e( 'Materials', '3dprint' );?></a></li>
			<li><a href="#3dp_tabs-3"><?php _e( 'Coatings', '3dprint' );?></a></li>
			<li><a href="#3dp_tabs-4"><?php _e( 'Price Requests', '3dprint' );?></a></li>
			<li><a href="#3dp_tabs-5"><?php _e( 'Email Templates', '3dprint' );?></a></li>
		</ul>
		<div id="3dp_tabs-0">
			<p><?php _e( 'In  <a target="_blank" href="https://www.youtube.com/watch?v=ZMziju_Cueg">this video</a> you can see how to set up a 3d printing product.', '3dprint' );?></p>
			<form method="post" action="admin.php?page=3dprint#3dp_tabs-0" enctype="multipart/form-data">
				<p><b><?php _e( 'Checkout', '3dprint' );?></b></p>
				<table>
					<tr>
						<td><?php _e( 'Checkout', '3dprint' );?></td>
						<td>
							<select name="3dp_settings[pricing]" onchange="p3dCheckPricing(this.value);">
								<option <?php if ( $settings['pricing']=='checkout' ) echo 'selected';?> value="checkout"><?php _e( 'Calculate price and add to cart' , '3dprint' );?></option>
								<option <?php if ( $settings['pricing']=='request_estimate' ) echo 'selected';?> value="request_estimate"><?php _e( 'Give an estimate and request price', '3dprint' );?></option>
								<option <?php if ( $settings['pricing']=='request' ) echo 'selected';?> value="request"><?php _e( 'Request price', '3dprint' );?></option>
						 	</select>
							<a id="extra_pricing_button" <?php if ( $settings['pricing']!='checkout' ) echo 'style="display:none;"';?> href="#TB_inline?width=300&height=200&inlineId=show_pricing_extra" class="thickbox"><button onclick="return false;">...</button></a>
							<div id="show_pricing_extra" style="display:none;">
							        <table>
							        <tr>
								<td><?php _e( 'If a model can not be repaired', '3dprint' );?>:</td>
								<td>
								<select name="3dp_settings[pricing_irrepairable]">
									<option <?php if ( $settings['pricing_irrepairable']=='checkout' ) echo 'selected';?> value="checkout"><?php _e( 'Calculate price and add to cart' , '3dprint' );?></option>
									<option <?php if ( $settings['pricing_irrepairable']=='request' ) echo 'selected';?> value="request"><?php _e( 'Request price', '3dprint' );?></option>
							 	</select>
							 	</td>
								</tr>
								<tr>
								<td><?php _e( 'If a model is too large', '3dprint' );?>:</td>
								<td>
								<select name="3dp_settings[pricing_too_large]" >
									<option <?php if ( $settings['pricing_too_large']=='checkout' ) echo 'selected';?> value="checkout"><?php _e( 'Calculate price and add to cart' , '3dprint' );?></option>
									<option <?php if ( $settings['pricing_too_large']=='request' ) echo 'selected';?> value="request"><?php _e( 'Request price', '3dprint' );?></option>
							 	</select>
							 	</td>

								</tr>
								<tr>
								<td><?php _e( 'If could not pack multiple models', '3dprint' );?>:</td>
								<td>
								<select name="3dp_settings[pricing_arrange]" >
									<option <?php if ( $settings['pricing_arrange']=='checkout' ) echo 'selected';?> value="checkout"><?php _e( 'Calculate price and add to cart' , '3dprint' );?></option>
									<option <?php if ( $settings['pricing_arrange']=='request' ) echo 'selected';?> value="request"><?php _e( 'Request price', '3dprint' );?></option>
							 	</select>
							 	</td>
								</tr>
								<tr>
								<td><?php _e( 'If a model can not be analysed or API subscription is expired', '3dprint' );?>:</td>
								<td>
								<select name="3dp_settings[pricing_api_expired]" >
									<option <?php if ( $settings['pricing_api_expired']=='none' ) echo 'selected';?> value="none"><?php _e( 'None' , '3dprint' );?></option>
									<option <?php if ( $settings['pricing_api_expired']=='request' ) echo 'selected';?> value="request"><?php _e( 'Request price', '3dprint' );?></option>
							 	</select>
							 	</td>
								</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Product Minimum Price', '3dprint' );?></td>
						<td>
							<select name="3dp_settings[minimum_price_type]">
								<option <?php if ( $settings['minimum_price_type']=='minimum_price' ) echo 'selected';?> value="minimum_price"><?php _e( 'Minimum Price' , '3dprint' );?></option>
								<option <?php if ( $settings['minimum_price_type']=='starting_price' ) echo 'selected';?> value="starting_price"><?php _e( 'Starting Price' , '3dprint' );?></option>
						 	</select>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Minimum Price: if total is less than minimum price then total = minimum price. <br> Starting Price: total = total + starting price.<br> The price is set on the product page.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Startup Cost', '3dprint' );?></td>
						<td>
							<input type="text" name="3dp_settings[startup_price]" size="3" value="<?php echo $settings['startup_price'];?>" /><?php echo get_woocommerce_currency_symbol(); ?> 
							<img class="tooltip" data-title="<?php esc_attr_e( 'Adds an extra fee to an order if it contains a 3DPrint product', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
							&nbsp;&nbsp;
							<input type="checkbox" name="3dp_settings[startup_price_taxable]" <?php if ($settings['startup_price_taxable']=='on') echo 'checked';?>><?php _e('Taxable', '3dprint');?>
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Round Price To', '3dprint' );?></td>
						<td>
							<input type="text" name="3dp_settings[price_num_decimals]" size="3" value="<?php echo $settings['price_num_decimals'];?>" /><?php _e('digits', '3dprint'); ?> 
							<img class="tooltip" data-title="<?php esc_attr_e( 'Examples:<br>2 digits rounds 1.9558 to 1.96<br>-3 digits rounds 1241757 to 1242000', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">

						</td>
					</tr>

				</table>

				<hr>
				<p><b><?php _e( 'Product Viewer', '3dprint' );?></b></p>
				<table>

					<tr>
						<td><?php _e( 'Canvas Resolution', '3dprint' );?></td>
						<td><input size="3" type="text"  placeholder="<?php _e( 'Width', '3dprint' );?>" name="3dp_settings[canvas_width]" value="<?php echo $settings['canvas_width'];?>">px &times; <input size="3"  type="text" placeholder="<?php _e( 'Height', '3dprint' );?>" name="3dp_settings[canvas_height]" value="<?php echo $settings['canvas_height'];?>">px</td>
					</tr>
					<tr>
						<td><?php _e( 'Display Mode', '3dprint' );?></td>
						<td>
							<select name="3dp_settings[display_mode]">
								<option <?php if ( $settings['display_mode']=='on_page' ) echo 'selected';?> value="on_page"><?php _e( 'On page', '3dprint' );?></option>
								<option <?php if ( $settings['display_mode']=='fullscreen' ) echo 'selected';?> value="fullscreen"><?php _e( 'Fullscreen', '3dprint' );?></option>
							</select> 
							<?php _e( '<a target="_blank" href="https://www.youtube.com/watch?v=Mn-ZxSjf1KU">This video</a> explains how to set the display mode for a specific product.', '3dprint' );?>
						</td>
					</tr>

					<tr>
						<td><?php _e( 'Default Unit', '3dprint' );?></td>
						<td>
							<select name="3dp_settings[default_unit]">
								<option <?php if ( $settings['default_unit']=='mm' ) echo 'selected';?> value="mm"><?php _e( 'Mm', '3dprint' );?></option>
								<option <?php if ( $settings['default_unit']=='inch' ) echo 'selected';?> value="inch"><?php _e( 'Inch', '3dprint' );?></option>
							</select> 
						</td>
					</tr>

					<tr>
						<td><?php _e( 'Shading', '3dprint' );?></td>
						<td>
							<select name="3dp_settings[shading]">
								<option <?php if ( $settings['shading']=='flat' ) echo 'selected';?> value="flat"><?php _e( 'Flat', '3dprint' );?></option>
								<option <?php if ( $settings['shading']=='smooth' ) echo 'selected';?> value="smooth"><?php _e( 'Smooth', '3dprint' );?></option>
							</select> 
							<img class="tooltip" data-title="<img src='<?php echo plugins_url( '3dprint/images/shading.jpg' );?>'>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>

					<tr>
						<td><?php _e( 'Cookie Lifetime', '3dprint' );?></td>
						<td>
							<select name="3dp_settings[cookie_expire]">
								<option <?php if ( $settings['cookie_expire']=='0' ) echo 'selected';?> value="0">0 <?php _e( '(no cookies)', '3dprint' );?> 
								<option <?php if ( $settings['cookie_expire']=='1' ) echo 'selected';?> value="1">1
								<option <?php if ( $settings['cookie_expire']=='2' ) echo 'selected';?> value="2">2
							</select> <?php _e( 'days', '3dprint' );?> 

						</td>
					</tr>

					<tr>
						<td><?php _e( 'Printers Layout', '3dprint' );?></td>
						<td>
							<select name="3dp_settings[printers_layout]">
								<option <?php if ( $settings['printers_layout']=='lists' ) echo 'selected';?> value="lists"><?php _e( 'List', '3dprint' );?></option>
								<option <?php if ( $settings['printers_layout']=='dropdowns' ) echo 'selected';?> value="dropdowns"><?php _e( 'Dropdown', '3dprint' );?></option>
								<option <?php if ( $settings['printers_layout']=='slider' ) echo 'selected';?> value="slider"><?php _e( 'Slider', '3dprint' );?></option>
								<option <?php if ( $settings['printers_layout']=='group_slider' ) echo 'selected';?> value="group_slider"><?php _e( 'Group Slider', '3dprint' );?></option>
							</select> 
							&nbsp; <span style="<?php if ( $settings['printers_layout']!='slider' ) echo "display:none;";?>"><?php _e('Items per slide', '3dprint');?> <input type="text" size="3" name="3dp_settings[printers_slider_num]" value="<?php echo $settings['printers_slider_num'];?>"></span>
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Materials Layout', '3dprint' );?></td>
						<td>
							<select name="3dp_settings[materials_layout]">
								<option <?php if ( $settings['materials_layout']=='lists' ) echo 'selected';?> value="lists"><?php _e( 'List', '3dprint' );?></option>
								<option <?php if ( $settings['materials_layout']=='dropdowns' ) echo 'selected';?> value="dropdowns"><?php _e( 'Dropdown', '3dprint' );?></option>
								<option <?php if ( $settings['materials_layout']=='colors' ) echo 'selected';?> value="colors"><?php _e( 'Colors', '3dprint' );?></option>
								<option <?php if ( $settings['materials_layout']=='slider' ) echo 'selected';?> value="slider"><?php _e( 'Slider', '3dprint' );?></option>
								<option <?php if ( $settings['materials_layout']=='group_slider' ) echo 'selected';?> value="group_slider"><?php _e( 'Group Slider', '3dprint' );?></option>
							</select> 
							&nbsp; <span style="<?php if ( $settings['materials_layout']!='slider' ) echo "display:none;";?>"><?php _e('Items per slide', '3dprint');?> <input type="text" size="3" name="3dp_settings[materials_slider_num]" value="<?php echo $settings['materials_slider_num'];?>"></span>
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Coatings Layout', '3dprint' );?></td>
						<td>
							<select name="3dp_settings[coatings_layout]">
								<option <?php if ( $settings['coatings_layout']=='lists' ) echo 'selected';?> value="lists"><?php _e( 'List', '3dprint' );?></option>
								<option <?php if ( $settings['coatings_layout']=='dropdowns' ) echo 'selected';?> value="dropdowns"><?php _e( 'Dropdown', '3dprint' );?></option>
								<option <?php if ( $settings['coatings_layout']=='colors' ) echo 'selected';?> value="colors"><?php _e( 'Colors', '3dprint' );?></option>
								<option <?php if ( $settings['coatings_layout']=='slider' ) echo 'selected';?> value="slider"><?php _e( 'Slider', '3dprint' );?></option>
								<option <?php if ( $settings['coatings_layout']=='group_slider' ) echo 'selected';?> value="group_slider"><?php _e( 'Group Slider', '3dprint' );?></option>
							</select> 
							&nbsp; <span style="<?php if ( $settings['coatings_layout']!='slider' ) echo "display:none;";?>"><?php _e('Items per slide', '3dprint');?> <input type="text" size="3" name="3dp_settings[coatings_slider_num]" value="<?php echo $settings['coatings_slider_num'];?>"></span>
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Infills Layout', '3dprint' );?></td>
						<td>
							<select name="3dp_settings[infills_layout]">
								<option <?php if ( $settings['infills_layout']=='lists' ) echo 'selected';?> value="lists"><?php _e( 'List', '3dprint' );?></option>
								<option <?php if ( $settings['infills_layout']=='dropdowns' ) echo 'selected';?> value="dropdowns"><?php _e( 'Dropdown', '3dprint' );?></option>
							</select> 
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Background Color', '3dprint' );?></td>
						<td><input type="text" class="3dp_color_picker" name="3dp_settings[background1]" value="<?php echo $settings['background1'];?>"></td>
					</tr>

					<tr>
						<td><?php _e( 'Grid Color', '3dprint' );?></td>
						<td><input type="text" class="3dp_color_picker" name="3dp_settings[plane_color]" value="<?php echo $settings['plane_color'];?>"></td>
					</tr>
					<tr>
						<td><?php _e( 'Ground Color', '3dprint' );?></td>
						<td><input type="text" class="3dp_color_picker" name="3dp_settings[ground_color]" value="<?php echo $settings['ground_color'];?>"></td>
					</tr>
					<tr>
						<td><?php _e( 'Printer Color', '3dprint' );?></td>
						<td><input type="text" class="3dp_color_picker" name="3dp_settings[printer_color]" value="<?php echo $settings['printer_color'];?>"></td>
					</tr>
					<tr>
						<td><?php _e( 'Button Background', '3dprint' );?></td>
						<td><input type="text" class="3dp_color_picker" name="3dp_settings[button_color1]" value="<?php echo $settings['button_color1'];?>"></td>
					</tr>
					<tr>
						<td><?php _e( 'Button Shadow', '3dprint' );?></td>
						<td><input type="text" class="3dp_color_picker" name="3dp_settings[button_color2]" value="<?php echo $settings['button_color2'];?>"></td>
					</tr>
					<tr>
						<td><?php _e( 'Button Progress Bar', '3dprint' );?></td>
						<td><input type="text" class="3dp_color_picker" name="3dp_settings[button_color3]" value="<?php echo $settings['button_color3'];?>"></td>
					</tr>
					<tr>
						<td><?php _e( 'Button Font', '3dprint' );?></td>
						<td><input type="text" class="3dp_color_picker" name="3dp_settings[button_color4]" value="<?php echo $settings['button_color4'];?>"></td>
					</tr>
					<tr>
						<td><?php _e( 'Button Tick', '3dprint' );?></td>
						<td><input type="text" class="3dp_color_picker" name="3dp_settings[button_color5]" value="<?php echo $settings['button_color5'];?>"></td>
					</tr>
					<tr>
						<td><?php _e( 'Loading Image', '3dprint' );?></td>
						<td>
							<img class="3dprint-preview" src="<?php echo esc_url($settings['ajax_loader']);?>">
							<input type="file" name="3dp_settings[ajax_loader]" accept="image/*">
						</td>
					</tr>

					<tr>
						<td><?php _e( 'Auto Rotation', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[auto_rotation]" value="0"><input type="checkbox" name="3dp_settings[auto_rotation]" <?php if ($settings['auto_rotation']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Resize model on scale', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[resize_on_scale]" value="0"><input type="checkbox" name="3dp_settings[resize_on_scale]" <?php if ($settings['resize_on_scale']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Fit camera to model on resize', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[fit_on_resize]" value="0"><input type="checkbox" name="3dp_settings[fit_on_resize]" <?php if ($settings['fit_on_resize']=='on') echo 'checked';?>></td>
					</tr>



<!--					<tr>
						<td><?php _e( 'Zoom', '3dprint' );?></td>
						<td><input size="3" type="text" name="3dp_settings[zoom]" value="<?php echo $settings['zoom'];?>"></td>
					</tr>
					<tr>
						<td><?php _e( 'Angle X', '3dprint' );?></td>
						<td><input size="3" type="text" name="3dp_settings[angle_x]" value="<?php echo $settings['angle_x'];?>">&deg;</td>
					</tr>
					<tr>
						<td><?php _e( 'Angle Y', '3dprint' );?></td>
						<td><input size="3" type="text" name="3dp_settings[angle_y]" value="<?php echo $settings['angle_y'];?>">&deg;</td>
					</tr>
					<tr>
						<td><?php _e( 'Angle Z', '3dprint' );?></td>
						<td><input size="3" type="text" name="3dp_settings[angle_z]" value="<?php echo $settings['angle_z'];?>">&deg;</td>
					</tr>-->
					<tr>
						<td><?php _e( 'Show Shadows', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_shadow]" value="0"><input type="checkbox" name="3dp_settings[show_shadow]" <?php if ($settings['show_shadow']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Ground Mirror', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[ground_mirror]" value="0"><input type="checkbox" name="3dp_settings[ground_mirror]" <?php if ($settings['ground_mirror']=='on') echo 'checked';?>></td>
					</tr>

					<tr>
						<td><?php _e( 'Show Printer Box', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_printer_box]" value="0"><input type="checkbox" name="3dp_settings[show_printer_box]" <?php if ($settings['show_printer_box']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Show Grid', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_grid]" value="0"><input type="checkbox" name="3dp_settings[show_grid]" <?php if ($settings['show_grid']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Show Axis', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_axis]" value="0"><input type="checkbox" name="3dp_settings[show_axis]" <?php if ($settings['show_axis']=='on') echo 'checked';?>></td>
					</tr>

<!--					<tr>
						<td><?php _e( 'Show Canvas Stats', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_canvas_stats]" value="0"><input type="checkbox" name="3dp_settings[show_canvas_stats]" <?php if ($settings['show_canvas_stats']=='on') echo 'checked';?>></td>
					</tr>-->
					<tr>
						<td><?php _e( 'Show Upload Button', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_upload_button]" value="0"><input type="checkbox" name="3dp_settings[show_upload_button]" <?php if ($settings['show_upload_button']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Show Scaling', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_scale]" value="0"><input type="checkbox" name="3dp_settings[show_scale]" <?php if ($settings['show_scale']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Can Scale Axis Independently', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[scale_xyz]" value="0"><input type="checkbox" name="3dp_settings[scale_xyz]" <?php if ($settings['scale_xyz']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Show Rotation', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_rotation]" value="0"><input type="checkbox" name="3dp_settings[show_rotation]" <?php if ($settings['show_rotation']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Hide Ground on Rotation', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[hide_plane_on_rotation]" value="0"><input type="checkbox" name="3dp_settings[hide_plane_on_rotation]" <?php if ($settings['hide_plane_on_rotation']=='on') echo 'checked';?>></td>
					</tr>

					<tr>
						<td><?php _e( 'Show File Unit', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_unit]" value="0"><input type="checkbox" name="3dp_settings[show_unit]" <?php if ($settings['show_unit']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Show Filename', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_filename]" value="0"><input type="checkbox" name="3dp_settings[show_filename]" <?php if ($settings['show_filename']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Show Model Stats', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_model_stats]" value="0"><input type="checkbox" name="3dp_settings[show_model_stats]" <?php if ($settings['show_model_stats']=='on') echo 'checked';?>>
							<div id="show_model_stats_extra" style="display:none;">
								<table>
									<tr>
										<td><?php _e( 'Material Volume', '3dprint' );?></td>
										<td><input type="hidden" name="3dp_settings[show_model_stats_material_volume]" value="0"><input type="checkbox" name="3dp_settings[show_model_stats_material_volume]" <?php if ($settings['show_model_stats_material_volume']=='on') echo 'checked';?>></td>
									</tr>
									<tr>
										<td><?php _e( 'Box Volume', '3dprint' );?></td>
										<td><input type="hidden" name="3dp_settings[show_model_stats_box_volume]" value="0"><input type="checkbox" name="3dp_settings[show_model_stats_box_volume]" <?php if ($settings['show_model_stats_box_volume']=='on') echo 'checked';?>></td>
									</tr>
									<tr>
										<td><?php _e( 'Surface Area', '3dprint' );?></td>
										<td><input type="hidden" name="3dp_settings[show_model_stats_surface_area]" value="0"><input type="checkbox" name="3dp_settings[show_model_stats_surface_area]" <?php if ($settings['show_model_stats_surface_area']=='on') echo 'checked';?>></td>
									</tr>
									<tr>
										<td><?php _e( 'Model Weight', '3dprint' );?></td>
										<td><input type="hidden" name="3dp_settings[show_model_stats_model_weight]" value="0"><input type="checkbox" name="3dp_settings[show_model_stats_model_weight]" <?php if ($settings['show_model_stats_model_weight']=='on') echo 'checked';?>></td>
									</tr>
									<tr>
										<td><?php _e( 'Model Dimensions', '3dprint' );?></td>
										<td><input type="hidden" name="3dp_settings[show_model_stats_model_dimensions]" value="0"><input type="checkbox" name="3dp_settings[show_model_stats_model_dimensions]" <?php if ($settings['show_model_stats_model_dimensions']=='on') echo 'checked';?>></td>
									</tr>
									<tr>
										<td><?php _e( 'Print Time', '3dprint' );?></td>
										<td><input type="hidden" name="3dp_settings[show_model_stats_model_hours]" value="0"><input type="checkbox" name="3dp_settings[show_model_stats_model_hours]" <?php if ($settings['show_model_stats_model_hours']=='on') echo 'checked';?>></td>
									</tr>

								</table>
							</div>

							<a href="#TB_inline?width=300&height=200&inlineId=show_model_stats_extra" class="thickbox"><button onclick="return false;">...</button></a>
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Show Printers', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_printers]" value="0"><input type="checkbox" name="3dp_settings[show_printers]" <?php if ($settings['show_printers']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Show Materials', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_materials]" value="0"><input type="checkbox" name="3dp_settings[show_materials]" <?php if ($settings['show_materials']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Show Coatings', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_coatings]" value="0"><input type="checkbox" name="3dp_settings[show_coatings]" <?php if ($settings['show_coatings']=='on') echo 'checked';?>></td>
					</tr>

				</table>
				<hr>
				<p><b><?php _e( 'File Upload', '3dprint' );?></b></p>
				<table>
					<tr>
						<td><?php _e( 'Max. File Size', '3dprint' );?></td>
						<td><input size="3" type="text" name="3dp_settings[file_max_size]" value="<?php echo $settings['file_max_size'];?>"><?php _e( 'mb' );?> </td>
					</tr>
					<tr>
						<td><?php _e( 'File Chunk Size', '3dprint' );?></td>
						<td><input size="3" type="text" name="3dp_settings[file_chunk_size]" value="<?php echo $settings['file_chunk_size'];?>"><?php _e( 'mb' );?> </td>
					</tr>
					<tr>
						<td><?php _e( 'Allowed Extensions', '3dprint' );?></td>
						<td><input size="9" type="text" name="3dp_settings[file_extensions]" value="<?php echo $settings['file_extensions'];?>"></td>
					</tr>
					<tr>
						<td><?php _e( 'Delete files older than', '3dprint' );?></td>
						<td><input size="3" type="text" name="3dp_settings[file_max_days]" value="<?php echo $settings['file_max_days'];?>"><?php _e( 'days', '3dprint' );?> </td>
					</tr>
				</table>
				<hr>

				<p><b><?php _e( 'Plugin Updates', '3dprint' );?></b></p>

				<p><i><?php _e('This login is used for getting plugin updates and using server-side features listed below (repair, optimize, etc).', '3dprint');?></i></p>

				<table>
					<tr>
						<td><?php _e( 'Login', '3dprint' );?></td>
						<td><input type="text" placeholder="user@example.com" name="3dp_settings[api_login]" value="<?php echo $settings['api_login'];?>">&nbsp;<?php _e( '(the e-mail you used when you were ordering the plugin)', '3dprint' );?></td>
					</tr>
				</table>

				<hr>
				<p><b><?php _e( 'Model Processing', '3dprint' );?></b></p>
				<table>
					<tr>
						<td><?php _e( 'Repair Models', '3dprint' );?></td>
						<td>
							<input type="hidden" name="3dp_settings[api_repair]" value="0"><input type="checkbox" name="3dp_settings[api_repair]" <?php if ($settings['api_repair']=='on') echo 'checked';?>>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Try to repair uploaded models through Slic3r. Use with caution as it might mess up models.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Optimize Models', '3dprint' );?></td>
						<td>
							<input type="hidden" name="3dp_settings[api_optimize]" value="0"><input type="checkbox" name="3dp_settings[api_optimize]" <?php if ($settings['api_optimize']=='on') echo 'checked';?>>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Optimize the model orientation on the printing platform to improve the efficiency of 3D printing.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Pack Models', '3dprint' );?></td>
						<td>
							<input type="hidden" name="3dp_settings[api_pack]" value="0"><input type="checkbox" name="3dp_settings[api_pack]" <?php if ($settings['api_pack']=='on') echo 'checked';?>>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Pack multiple STL models from ZIP file to fit into the build tray (STL only).', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
							<?php _e('Minimal spacing (mm)');?>:<input type="hidden" name="3dp_settings[api_pack_spacing]" value="0"><input type="text" size="2" name="3dp_settings[api_pack_spacing]" value="<?php echo $settings['api_pack_spacing'];?>">
						</td>
					</tr>

				</table>
				<hr>
				<p><b><?php _e( 'Analyse API', '3dprint' );?></b></p>
				<p><i><?php _e('This is needed for using infill and getting accurate material volume. Paid monthly subscription is required.', '3dprint');?></i></p>
				<p><i><?php _e('Demo is available <a href="http://www.wp3dprinting.com/index.php/product/3d-printing-demo-product/">here.</a>', '3dprint');?></i></p>
				<p><i><?php _e('Free API calls left ', '3dprint');?>: <?php echo p3d_get_trial_number();?></i></p>

				<table>
					<tr>
						<td><?php _e( 'Enable API', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[api_analyse]" value="0"><input type="checkbox" name="3dp_settings[api_analyse]" <?php if ($settings['api_analyse']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Slicer', '3dprint' );?></td>
						<td>
							<select name="3dp_settings[slicer]">
								<option <?php if ( $settings['slicer']=='cura' ) echo 'selected';?> value="cura"><?php _e( 'Cura 15.04', '3dprint' );?></option>
								<option <?php if ( $settings['slicer']=='slic3r' ) echo 'selected';?> value="slic3r"><?php _e( 'Slic3r 1.2.9', '3dprint' );?></option>
							</select> 
						</td>
					</tr>




					<tr>
						<td><?php _e( 'Show Infills', '3dprint' );?></td>
						<td><input type="hidden" name="3dp_settings[show_infills]" value="0"><input type="checkbox" name="3dp_settings[show_infills]" <?php if ($settings['show_infills']=='on') echo 'checked';?>></td>
					</tr>
					<tr>
						<td><?php _e( 'Login', '3dprint' );?></td>
						<td><input type="text" placeholder="user@example.com" name="3dp_settings[api_subscription_login]" value="<?php echo $settings['api_subscription_login'];?>">&nbsp;<?php _e( '(the e-mail you used when you were ordering the subscription)', '3dprint' );?></td>
					</tr>
					<tr>
						<td><?php _e( 'Key', '3dprint' );?></td>
						<td><input type="text" placeholder="" name="3dp_settings[api_subscription_key]" value="<?php echo $settings['api_subscription_key'];?>">&nbsp;<a target="_blank" href="https://secure.avangate.com/order/checkout.php?PRODS=4678226&QTY=1&CART=1&CARD=1"><?php _e('Buy Now!', '3dprint');?></a></td>
					</tr>
				</table>
				<hr>
				<p><b><?php _e( 'Form builder', '3dprint' );?></b></p>
<?php
				include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
				if( !is_plugin_active( 'ninja-forms/ninja-forms.php' ) ) {
					_e('Please install <a href="https://wordpress.org/plugins/ninja-forms/">NinjaForms</a> plugin first.', '3dprint');
				}
				else {
?>
				<p><?php _e( 'In <a target="_blank" href="https://youtu.be/ZB82ozu8I94">this video</a> you can see how to configure NinjaForms integration.', '3dprint' );?></p>
				<table>
					<tr>
						<td><?php _e( 'Use NinjaForms', '3dprint' );?></td>
						<td>
							<input type="hidden" name="3dp_settings[use_ninjaforms]" value="0">
							<input type="checkbox" name="3dp_settings[use_ninjaforms]" <?php if ($settings['use_ninjaforms']=='on') echo 'checked';?>>&nbsp;
							<img class="tooltip" data-title="<?php esc_attr_e( 'Use NinjaForms 3.0+ builder for the price request form.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>
					<tr>
						<td><?php _e( 'NinjaForms shortcode', '3dprint' );?></td>
						<td><input id="p3d-ninjaforms-shortcode" type="text" placeholder="[ninja_form id=1]" name="3dp_settings[ninjaforms_shortcode]" value="<?php echo $settings['ninjaforms_shortcode'];?>">&nbsp;
							<button id="p3d-generate-button" type="button" onclick="p3dGenerateNinjaFormsShortcode();"><?php _e('Generate', '3dprint')?></button>
							<img id="p3d-generate-image" style="display:inline-block;visibility:hidden;" alt="Generating" src="<?php echo plugins_url( '3dprint/images/ajax-loader-small.gif'); ?>">
<?php
if ($form_id>0) {
?>
							<a id="p3d-shortcode-edit" style="<?php if ($settings['ninjaforms_shortcode']=='') echo 'display:none;'?>" href="<?php echo admin_url( "admin.php?page=ninja-forms&form_id=$form_id" );?>"><?php _e('Edit', '3dprint');?></a>
<?php
}
?>

						</td>
					</tr>
				</table>
<?php
				}
?>

				<hr>
				<p><b><?php _e( 'Other', '3dprint' );?></b></p>
				<table>
					<tr>
						<td><?php _e( 'Email', '3dprint' );?></td>
						<td><input type="text" placeholder="user@example.com" name="3dp_settings[email_address]" value="<?php echo $settings['email_address'];?>">&nbsp;
						<img class="tooltip" data-title="<?php esc_attr_e( 'The email where price requests go.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						<?php if ($settings['use_ninjaforms']=='on') { ?>
							<p style="color:red;"><?php echo sprintf(__('NinjaForms is being used. Please configure this e-mail on the <a href="%s">appropriate NinjaForms page.</a>', '3dprint'), admin_url( "admin.php?page=ninja-forms&form_id=$form_id" ));?>&nbsp;</p>
						<?php }?>
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Server Side Triangulation', '3dprint' );?></td>
						<td>
							<input type="hidden" name="3dp_settings[server_triangulation]" value="0">
							<input type="checkbox" name="3dp_settings[server_triangulation]" <?php if ($settings['server_triangulation']=='on') echo 'checked';?>>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Some complex OBJ files may require server side triangulation to get more accurate stats.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Load Everywhere', '3dprint' );?></td>
						<td>
							<input type="hidden" name="3dp_settings[load_everywhere]" value="0">
							<input type="checkbox" name="3dp_settings[load_everywhere]" <?php if ($settings['load_everywhere']=='on') echo 'checked';?>>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Enable if you need to load 3DPrint css and js files on all pages of the site.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Skip animation on mobile devices', '3dprint' );?></td>
						<td>
							<input type="hidden" name="3dp_settings[mobile_no_animation]" value="0">
							<input type="checkbox" name="3dp_settings[mobile_no_animation]" <?php if ($settings['mobile_no_animation']=='on') echo 'checked';?>>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Shows a static picture instead of a 3D model on mobile devices for better performance.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Items per page', '3dprint' );?></td>
						<td>
							<input size="3" type="text" name="3dp_settings[items_per_page]" value="<?php echo $settings['items_per_page'];?>">
							<img class="tooltip" data-title="<?php esc_attr_e( 'Number of iterms per page in the admin area.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>


				</table>
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e( 'Save Changes', '3dprint' ) ?>" />
				</p>
			</form>
		</div>  
		<div id="3dp_tabs-1">
			<form method="post" action="admin.php?page=3dprint&cpage=1&p3d_section=p3d_printers#3dp_tabs-1" enctype="multipart/form-data">

<?php 			wp_nonce_field( 'update-options' ); ?>
<?php
	echo paginate_links( array(
        	'base' => add_query_arg( array('cpage' => '%#%', 'p3d_section' => 'p3d_printers') ).'#3dp_tabs-1',
	        'format' => '',
	        'prev_text' => __('&laquo;'),
	        'next_text' => __('&raquo;'),
	        'total' => ceil(p3d_get_option_total('p3d_printers') / $settings['items_per_page']),
	        'current' => ((isset($_GET['cpage']) && is_numeric($_GET['cpage']) && $_GET['p3d_section']=='p3d_printers') ? $_GET['cpage'] : 1)
	));
?>
<br>
<?php

	if ( is_array( $paginated_printers ) && count( $paginated_printers )>0 ) {

		$i=0;
		foreach ( $paginated_printers as $printer ) {

?>
				<input type="hidden" name="action" value="update" />
				<div class="p3d-expand">
				<h3><?php echo '#'.$printer['id'].' '.$printer['name'];?></h3>
				<div>
				<table id="printer-<?php echo $printer['id'];?>" data-id="<?php echo $printer['id'];?>" class="form-table printer">
					<tr>
						<td colspan="3"><span class="item_id"><?php echo "<b>ID #".$printer['id']."</b>";?></span></td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<?php _e( 'Printer Name', '3dprint' ); ?>
						</th>
						<td>
							<input type="text" name="3dp_printer_name[<?php echo $printer['id'];?>]" value="<?php echo $printer['name'];?>" />&nbsp;
						</td>
					</tr>

					<tr valign="top">
						<th scope="row">
							<?php _e( 'Printer Description', '3dprint' ); ?>
						</th>
						<td>
							<textarea name="3dp_printer_description[<?php echo $printer['id'];?>]" /><?php echo $printer['description'];?></textarea>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Photo', '3dprint' );?></th>
						<td>
							<a href="<?php echo $printer['photo'];?>"><img class="p3d-preview" src="<?php echo esc_url($printer['photo']);?>"></a>
							<input type="text" name="3dp_printer_photo[<?php echo $printer['id'];?>]" value="<?php echo esc_url($printer['photo']);?>" />
							<input type="file" name="3dp_printer_photo_upload[<?php echo $printer['id'];?>]" accept="image/*">
						</td>
					</tr>



					<tr>
						<th scope="row"><?php _e( 'Enabled', '3dprint' );?></th>
						<td>
							<select name="3dp_printer_status[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['status']=='1' ) echo "selected";?> value="1"><?php _e('Yes', '3dprint');?></option>
								<option <?php if ( $printer['status']=='0' ) echo "selected";?> value="0"><?php _e('No', '3dprint');?></option>
							</select>

						</td>
					</tr>


				 	<tr valign="top">
						<th scope="row"><?php _e( 'Printer Type', '3dprint' );?></th>
						<td>
							<select class="select_printer" name="3dp_printer_type[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['type']=='fff' ) echo "selected";?> value="fff"><?php _e( 'FFF/FDM', '3dprint' );?>
								<option <?php if ( $printer['type']=='dlp' ) echo "selected";?> value="dlp"><?php _e( 'DLP', '3dprint' );?>
								<option <?php if ( $printer['type']=='other' ) echo "selected";?> value="other"><?php _e( 'Other', '3dprint' );?>
							</select>
						</td>
					</tr>
					<tr>
						<th scope="row"><?php _e( 'Full Color Printing', '3dprint' );?></th>
						<td>
							<select name="3dp_printer_full_color[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['full_color']=='1' ) echo "selected";?> value="1"><?php _e('Yes', '3dprint');?></option>
								<option <?php if ( $printer['full_color']=='0' ) echo "selected";?> value="0"><?php _e('No', '3dprint');?></option>
							</select>

						</td>
					</tr>

				 	<tr valign="top">
						<th scope="row"><?php _e( 'Build Tray Shape', '3dprint' );?></th>
						<td>
							<select class="select_shape" name="3dp_printer_platform_shape[<?php echo $printer['id'];?>]" onchange="p3dSelectPlatformShape(this);">
								<option <?php if ( $printer['platform_shape']=='rectangle' ) echo "selected";?> value="rectangle"><?php _e( 'Rectangle', '3dprint' );?>
								<option <?php if ( $printer['platform_shape']=='circle' ) echo "selected";?> value="circle"><?php _e( 'Circle', '3dprint' );?>
							</select>
						</td>
					</tr>

					<tr class="platform_shape_circle" valign="top" <?php if ( $printer['platform_shape']=='rectangle' ) echo 'style="display:none;"';?>>
						<th scope="row"><?php _e( 'Build Tray Diameter', '3dprint' ); ?></th>
						<td><input type="text" name="3dp_printer_platform_diameter[<?php echo $printer['id'];?>]" value="<?php echo $printer['diameter'];?>" /><?php _e( 'mm', '3dprint' );?></td>
					</tr>

					<tr class="platform_shape_rectangle" valign="top" <?php if ( $printer['platform_shape']=='circle' ) echo 'style="display:none;"';?>>
						<th scope="row"><?php _e( 'Build Tray Length', '3dprint' ); ?></th>
						<td><input type="text" name="3dp_printer_length[<?php echo $printer['id'];?>]" value="<?php echo $printer['length'];?>" /><?php _e( 'mm', '3dprint' );?></td>
					</tr>

					<tr class="platform_shape_rectangle" valign="top" <?php if ( $printer['platform_shape']=='circle' ) echo 'style="display:none;"';?>>
						<th scope="row"><?php _e( 'Build Tray Width', '3dprint' ); ?></th>
						<td><input type="text" name="3dp_printer_width[<?php echo $printer['id'];?>]" value="<?php echo $printer['width'];?>" /><?php _e( 'mm', '3dprint' );?></td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Build Tray Height', '3dprint' ); ?></th>
						<td><input type="text" name="3dp_printer_height[<?php echo $printer['id'];?>]" value="<?php echo $printer['height'];?>" /><?php _e( 'mm', '3dprint' );?></td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Minimum Model Side', '3dprint' ); ?></th>
						<td><input type="text" name="3dp_printer_min_side[<?php echo $printer['id'];?>]" value="<?php echo $printer['min_side'];?>" /><?php _e( 'mm', '3dprint' );?></td>
					</tr>
					<tr valign="top" style="<?php if ($settings['api_analyse']!='on') echo 'display:none;'; ?>">
						<th scope="row"></th>
						<td><button onclick="p3dSetPrinterType(jQuery(this).closest('table.form-table').find('.select_printer').val(), this);return false;"><?php _e('Advanced Options');?></button></td>
					</tr>


					<tr valign="top" class="printer_dlp p3d-advanced">
						<th scope="row"><?php _e( 'Time per Layer', '3dprint' ); ?></th>
						<td><input type="text" name="3dp_printer_time_per_layer[<?php echo $printer['id'];?>]" value="<?php echo $printer['time_per_layer'];?>" /><?php _e( 'sec', '3dprint' );?> <img class="tooltip" data-title="<?php esc_attr_e( 'Total time required to print a layer including exposure time, bed move time, etc.<br>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td> 
					</tr>


					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<td colspan="2" align="center"><b><?php _e('Slicer settings', '3dprnit');?></b></td>
					</tr>
					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<th scope="row"><?php _e( 'Layer Height', '3dprint' ); ?></th>
						<td><input <?php if ($printer['type']=='fff' && $settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_layer_height[<?php echo $printer['id'];?>]" value="<?php echo $printer['layer_height'];?>" /><?php _e( 'mm', '3dprint' );?> <img class="tooltip" data-title="<?php esc_attr_e( 'Layer height in millimeters.<br>This is the most important setting to determine the quality of your print. Normal quality prints are 0.1mm, high quality is 0.06mm.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td> 
					</tr>

					<tr valign="top" class="printer_fff printer_dlp p3d-advanced <?php if ($settings['slicer']!='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Perimeters', '3dprint' ); ?></th>
						<td>
							<input <?php if ($printer['type']=='fff' && $settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_perimeters[<?php echo $printer['id'];?>]" value="<?php echo $printer['perimeters'];?>" />
							<img class="tooltip" data-title="<?php esc_attr_e( 'This option sets the number of perimeters to generate for each layer. Note that Slic3r may increase this number automatically when it detects sloping surfaces which benefit from a higher number of perimeters if the Extra Perimeters option is enabled.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td> 
					</tr>

					<tr valign="top" class="printer_fff printer_dlp p3d-advanced <?php if ($settings['slicer']!='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Top Solid Layers', '3dprint' ); ?></th>
						<td>
							<input <?php if ($printer['type']=='fff' && $settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_solid_layers_top[<?php echo $printer['id'];?>]" value="<?php echo $printer['solid_layers_top'];?>" />
							<img class="tooltip" data-title="<?php esc_attr_e( 'Number of solid layers to generate on top surfaces.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td> 
					</tr>
					<tr valign="top" class="printer_fff printer_dlp p3d-advanced <?php if ($settings['slicer']!='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Bottom Solid Layers', '3dprint' ); ?></th>
						<td>
							<input <?php if ($printer['type']=='fff' && $settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_solid_layers_bottom[<?php echo $printer['id'];?>]" value="<?php echo $printer['solid_layers_bottom'];?>" />
							<img class="tooltip" data-title="<?php esc_attr_e( 'Number of solid layers to generate on bottom surfaces.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td> 
					</tr>

					<tr valign="top" class="printer_fff p3d-advanced">
						<th scope="row"><?php _e( 'Wall Thickness', '3dprint' ); ?></th>
						<td><input <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_wall_thickness[<?php echo $printer['id'];?>]" value="<?php echo $printer['wall_thickness'];?>" /><?php _e( 'mm', '3dprint' );?> <img class="tooltip" data-title="<?php esc_attr_e( 'Thickness of the outside shell in the horizontal direction.<br>This is used in combination with the nozzle size to define the number<br>of perimeter lines and the thickness of those perimeter lines.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td>
					</tr>

					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']!='cura') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Bottom/Top Thickness', '3dprint' ); ?></th>
						<td><input <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_bottom_top_thickness[<?php echo $printer['id'];?>]" value="<?php echo $printer['bottom_top_thickness'];?>" /><?php _e( 'mm', '3dprint' );?> <img class="tooltip" data-title="<?php esc_attr_e( 'This controls the thickness of the bottom and top layers, the amount of solid layers put down is calculated by the layer thickness and this value.<br>Having this value a multiple of the layer thickness makes sense. And keep it near your wall thickness to make an evenly strong part.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td>
					</tr>


					<tr class="printer_fff p3d-advanced" valign="top">
						<th scope="row"><?php _e( 'Nozzle Size', '3dprint' ); ?></th>
						<td><input <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_nozzle_size[<?php echo $printer['id'];?>]" value="<?php echo $printer['nozzle_size'];?>" /><?php _e( 'mm', '3dprint' );?> <img class="tooltip" data-title="<?php esc_attr_e( 'The nozzle size is very important, this is used to calculate the line width of the infill, and used to calculate the amount of outside wall lines and thickness for the wall thickness you entered in the print settings.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td>
					</tr>

					<tr class="printer_fff p3d-advanced" valign="top">
						<th scope="row"><?php _e( 'Infill Options', '3dprint' ); ?></th>
						<td>
							<select <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> autocomplete="off" name="3dp_printer_infills[<?php echo $printer['id'];?>][]" multiple="multiple" class="sumoselect">
								<?php 
									for ($j=0; $j<=10; $j++) {
										if (strlen($printer['infills'])>0 && in_array($j*10, explode(',',$printer['infills']))) $selected="selected"; else $selected="";
										echo '<option '.$selected.' value="'.($j*10).'">'.($j*10).'%';
									}
								?>
							</select>&nbsp;
							<?php _e( 'Default Infill:', '3dprint' ); ?>
							<select <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> name="3dp_printer_default_infill[<?php echo $printer['id'];?>]">
								<?php 
									for ($j=0; $j<=10; $j++) {
										if ((int)$printer['default_infill']==($j*10)) $selected="selected"; else $selected="";
										echo '<option '.$selected.' value="'.($j*10).'">'.($j*10).'%';
									}
								?>
		 					</select>
							<img class="tooltip" data-title="<?php esc_attr_e( 'This controls how densely filled the insides of your print will be. For a solid part use 100%, for an empty part use 0%. A value around 20% is usually enough.<br>This won\'t affect the outside of the print and only adjusts how strong the part becomes.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>

					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']!='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Infill Pattern', '3dprint' ); ?></th>
						<td>
							<select <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> autocomplete="off" name="3dp_printer_infill_pattern[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['infill_pattern']=='rectilinear' ) echo "selected";?> value="rectilinear"><?php _e('Rectilinear', '3dprint');?></option>
								<option <?php if ( $printer['infill_pattern']=='line' ) echo "selected";?> value="line"><?php _e('Line', '3dprint');?></option>
								<option <?php if ( $printer['infill_pattern']=='concentric' ) echo "selected";?> value="concentric"><?php _e('Concentric', '3dprint');?></option>
								<option <?php if ( $printer['infill_pattern']=='honeycomb' ) echo "selected";?> value="honeycomb"><?php _e('Honeycomb', '3dprint');?></option>
								<option <?php if ( $printer['infill_pattern']=='3dhoneycomb' ) echo "selected";?> value="3dhoneycomb"><?php _e('3D honeycomb', '3dprint');?></option>
								<option <?php if ( $printer['infill_pattern']=='hilbertcurve' ) echo "selected";?> value="hilbertcurve"><?php _e('Hilbert Curve', '3dprint');?></option>
								<option <?php if ( $printer['infill_pattern']=='archimedeanchords' ) echo "selected";?> value="archimedeanchords"><?php _e('Archimedean Chords', '3dprint');?></option>
								<option <?php if ( $printer['infill_pattern']=='octagramspiral' ) echo "selected";?> value="octagramspiral"><?php _e('Octagram Spiral', '3dprint');?></option>
		 					</select>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Fill pattern for general low-density infill.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>

					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']!='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Top/Bottom Infill Pattern', '3dprint' ); ?></th>
						<td>
							<select <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> autocomplete="off" name="3dp_printer_infill_pattern_top_bottom[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['infill_pattern_top_bottom']=='rectilinear' ) echo "selected";?> value="rectilinear"><?php _e('Rectilinear', '3dprint');?></option>
								<option <?php if ( $printer['infill_pattern_top_bottom']=='concentric' ) echo "selected";?> value="concentric"><?php _e('Concentric', '3dprint');?></option>
								<option <?php if ( $printer['infill_pattern_top_bottom']=='hilbertcurve' ) echo "selected";?> value="hilbertcurve"><?php _e('Hilbert Curve', '3dprint');?></option>
								<option <?php if ( $printer['infill_pattern_top_bottom']=='archimedeanchords' ) echo "selected";?> value="archimedeanchords"><?php _e('Archimedean Chords', '3dprint');?></option>
								<option <?php if ( $printer['infill_pattern_top_bottom']=='octagramspiral' ) echo "selected";?> value="octagramspiral"><?php _e('Octagram Spiral', '3dprint');?></option>
		 					</select>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Fill pattern for top/bottom infill. This only affects the external visible layer, and not its adjacent solid shells.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>





					<tr valign="top" class="printer_fff p3d-advanced">
						<th scope="row"><?php _e( 'Print Speed', '3dprint' ); ?></th>
						<td><input <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_speed[<?php echo $printer['id'];?>]" value="<?php echo $printer['speed'];?>" />
							<select <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> name="3dp_printer_speed_type[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['speed_type']=='mm3s' ) echo "selected";?> value="mm3s"><?php _e('mm3/s', '3dprint');?></option>
								<option <?php if ( $printer['speed_type']=='mms' ) echo "selected";?>  value="mms"><?php _e('mm/s', '3dprint');?></option>
		 					</select>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Speed at which printing happens. This is needed if you charge by hour. A well adjusted Ultimaker can reach 150mm/s, but for good quality prints you want to print slower. <br>Printing speed depends on a lot of factors. So you will be experimenting with optimal settings for this.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>

					</tr>

					<tr valign="top" class="printer_fff p3d-advanced">
						<th scope="row"><?php _e( 'Infill Speed', '3dprint' ); ?></th>
						<td><input <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_infill_speed[<?php echo $printer['id'];?>]" value="<?php echo $printer['infill_speed'];?>" /><?php _e( 'mm/s', '3dprint' ); ?>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Speed for printing the internal fill. Set to zero for auto.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>

					<tr valign="top" class="printer_fff p3d-advanced">
						<th scope="row"><?php _e( 'Travel Speed', '3dprint' ); ?></th>
						<td><input <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_travel_speed[<?php echo $printer['id'];?>]" value="<?php echo $printer['travel_speed'];?>" /><?php _e( 'mm/s', '3dprint' ); ?>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Speed at which travel moves are done, a well built Ultimaker can reach speeds of 250mm/s. But some machines might miss steps then.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>

					</tr>
					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']=='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Bottom Layer Speed', '3dprint' ); ?></th>
						<td><input <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_bottom_layer_speed[<?php echo $printer['id'];?>]" value="<?php echo $printer['bottom_layer_speed'];?>" /><?php _e( 'mm/s', '3dprint' ); ?>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Print speed for the bottom layer, you want to print the first layer slower so it sticks better to the printer bed.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>

					</tr>
					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']=='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Top/bottom Speed', '3dprint' ); ?></th>
						<td><input <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_solidarea_speed[<?php echo $printer['id'];?>]" value="<?php echo $printer['solidarea_speed'];?>" /><?php _e( 'mm/s', '3dprint' ); ?>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Speed at which top/bottom parts are printed. If set to 0 then the print speed is used for the infill. Printing the top/bottom faster can greatly reduce printing time, but this can negatively affect print quality.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>

					</tr>
					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']=='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Outer Shell Speed', '3dprint' ); ?></th>
						<td><input <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_outer_shell_speed[<?php echo $printer['id'];?>]" value="<?php echo $printer['outer_shell_speed'];?>" /><?php _e( 'mm/s', '3dprint' ); ?>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Speed at which outer shell is printed. If set to 0 then the print speed is used. Printing the outer shell at a lower speed improves the final skin quality. However, having a large difference between the inner shell speed and the outer shell speed will effect quality in a negative way<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>

					</tr>
					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']=='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Inner Shell Speed', '3dprint' ); ?></th>
						<td><input <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_inner_shell_speed[<?php echo $printer['id'];?>]" value="<?php echo $printer['inner_shell_speed'];?>" /><?php _e( 'mm/s', '3dprint' ); ?>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Speed at which inner shells are printed. If set to 0 then the print speed is used. Printing the inner shell faster then the outer shell will reduce printing time. It is good to set this somewhere in between the outer shell speed and the infill/printing speed.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>

					</tr>

					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']=='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Minimal layer time', '3dprint' ); ?></th>
						<td><input <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_cool_min_layer_time[<?php echo $printer['id'];?>]" value="<?php echo $printer['cool_min_layer_time'];?>" /><?php _e( 'sec', '3dprint' ); ?>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Minimum time spent in a layer, gives the layer time to cool down before the next layer is put on top. If the layer will be placed down too fast the printer will slow down to make sure it has spent at least this amount of seconds printing this layer.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>

					</tr>


					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']=='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Support Material', '3dprint' ); ?></th>
						<td>
							<select <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> autocomplete="off" name="3dp_printer_support[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['support']=='0' ) echo "selected";?> value="0"><?php _e('None', '3dprint');?></option>
								<option <?php if ( $printer['support']=='1' ) echo "selected";?> value="1"><?php _e('Touching Buildplate', '3dprint');?></option>
								<option <?php if ( $printer['support']=='2' ) echo "selected";?> value="2"><?php _e('Everywhere', '3dprint');?></option>
		 					</select>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Type of support structure build.<br>\'Touching buildplate\' is the most commonly used support setting.<br><br>None does not do any support.<br>Touching buildplate only creates support where the support structure will touch the build platform.<br>Everywhere creates support even on top of parts of the model.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>
					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']=='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Support Structure Type', '3dprint' ); ?></th>
						<td>
							<select <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> autocomplete="off" name="3dp_printer_support_type[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['support_type']=='0' ) echo "selected";?> value="0"><?php _e('Lines', '3dprint');?></option>
								<option <?php if ( $printer['support_type']=='1' ) echo "selected";?> value="1"><?php _e('Grid', '3dprint');?></option>
		 					</select>
							<img class="tooltip" data-title="<?php esc_attr_e( 'The type of support structure.<br>Grid is very strong and can come off in 1 piece, however, sometimes it is too strong.<br>Lines are single walled lines that break off one at a time. Which is more work to remove, but as it is less strong it does work better on tricky prints.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>


					<tr valign="top" class="printer_fff p3d-advanced">
						<th scope="row"><?php _e( 'Support Overhang Angle', '3dprint' ); ?></th>
						<td>
							<input <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_support_angle[<?php echo $printer['id'];?>]" value="<?php echo $printer['support_angle'];?>" />&deg; 
							<img class="tooltip" data-title="<?php esc_attr_e( 'The minimal angle that overhangs need to have to get support. With 90 degree being horizontal and 0 degree being vertical.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>

					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']!='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Support Pattern Spacing', '3dprint' ); ?></th>
						<td>
							<input type="text" name="3dp_printer_support_pattern_spacing[<?php echo $printer['id'];?>]" value="<?php echo $printer['support_pattern_spacing'];?>" /><?php _e('mm', '3dprint');?>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Spacing between support material lines.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>

					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']!='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( "Don't support bridges", '3dprint' ); ?></th>
						<td>
							<select <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> autocomplete="off" name="3dp_printer_support_bridges[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['support_bridges']=='0' ) echo "selected";?> value="0"><?php _e('Yes', '3dprint');?></option>
								<option <?php if ( $printer['support_bridges']=='1' ) echo "selected";?> value="1"><?php _e('No', '3dprint');?></option>
		 					</select>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Experimental option for preventing support material from being generated under bridged areas<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">

						</td>
					</tr>

					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']!='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Support Raft Layers', '3dprint' ); ?></th>
						<td>
							<input type="text" name="3dp_printer_support_raft_layers[<?php echo $printer['id'];?>]" value="<?php echo $printer['support_raft_layers'];?>" />
							<img class="tooltip" data-title="<?php esc_attr_e( 'The object will be raised by this number of layers, and support material will be generated under it.<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>
<!--					<tr valign="top" class="printer_fff p3d-advanced <?php if ($settings['slicer']!='slic3r') echo 'p3d-hidden'?>">-->
					<tr valign="top" class="printer_fff p3d-advanced p3d-hidden">
						<th scope="row"><?php _e( 'Contact Z Distance', '3dprint' ); ?></th>
						<td>
							<input type="text" name="3dp_printer_support_z_distance[<?php echo $printer['id'];?>]" value="<?php echo $printer['support_z_distance'];?>" /><?php _e('mm', '3dprint');?>
							<img class="tooltip" data-title="<?php esc_attr_e( 'The vertical distance between object and support material interface. Setting this to 0 will also prevent Slic3r from using bridge flow and speed for the first object layer<br><b>Analyse API required</b>', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>">
						</td>
					</tr>

					<tr valign="top" class="printer_fff printer_dlp p3d-advanced <?php if ($settings['slicer']!='slic3r') echo 'p3d-hidden'?>">
						<th scope="row"><?php _e( 'Brim Width', '3dprint' ); ?></th>
						<td>
							<input <?php if ($printer['type']=='fff' && $settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" name="3dp_printer_brim_width[<?php echo $printer['id'];?>]" value="<?php echo $printer['brim_width'];?>" />
							<img class="tooltip" data-title="<?php esc_attr_e( 'Horizontal width of the brim that will be printed around each object on the first layer.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td> 
					</tr>

					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<td colspan="2" align="center"><b><?php _e('Energy', '3dprnit');?></b></td>
					</tr>
					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<th scope="row"><?php _e('Electricity Tariff', '3dprint');?></th>
						<td>

							<input onkeyup="p3dCalculateKwh();" name="3dp_printer_power_tariff[<?php echo $printer['id'];?>]" <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" size="4" value="<?php echo $printer['power_tariff'];?>">
							<?php echo get_woocommerce_currency_symbol(); ?>/<?php _e('kWh', '3dprint');?>
						</td>
					</tr>
					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<th scope="row"><?php _e('Printer Power', '3dprint');?></th>
						<td>
							<input onkeyup="p3dCalculateKwh();" name="3dp_printer_power[<?php echo $printer['id'];?>]" <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" size="4" value="<?php echo $printer['printer_power'];?>">
							<?php _e('Watts', '3dprint');?>
						</td>
					</tr>
					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<th scope="row"><?php _e('Hourly Cost', '3dprint');?></th>
						<td>
							<input class="printer_kwh" type="text" size="3" name="3dp_printer_energy_hourly_cost[<?php echo $printer['id'];?>]" value="<?php echo $printer['printer_energy_hourly_cost'];?>">
							<?php echo get_woocommerce_currency_symbol(); ?>/<?php _e('hr', '3dprint');?>
						</td>
					</tr>

					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<td colspan="2" align="center"><b><?php _e('Depreciation', '3dprnit');?></b></td>
					</tr>
					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<th scope="row"><?php _e('Printer Purchase Price', '3dprint');?></th>
						<td>
							<input onkeyup="p3dCalculateDepreciation();" name="3dp_printer_purchase_price[<?php echo $printer['id'];?>]" <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" size="4" value="<?php echo $printer['printer_purchase_price'];?>">
							<?php echo get_woocommerce_currency_symbol(); ?>
						</td>
					</tr>
					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<th scope="row"><?php _e('Printer Lifetime', '3dprint');?></th>
						<td>
							<input onkeyup="p3dCalculateDepreciation();" name="3dp_printer_lifetime[<?php echo $printer['id'];?>]" <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" size="4" value="<?php echo $printer['printer_lifetime'];?>">
							<?php _e('years', '3dprint');?>
							<img class="tooltip" data-title="<?php esc_attr_e( 'The total time before the printer needs to be replaced.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td> 
						</td>
					</tr>
					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<th scope="row"><?php _e('Printer Daily Usage', '3dprint');?></th>
						<td>
							<input onkeyup="p3dCalculateDepreciation();" name="3dp_printer_daily_usage[<?php echo $printer['id'];?>]" <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" size="4" value="<?php echo $printer['printer_daily_usage'];?>">
							<?php _e('hours', '3dprint');?>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Average usage per day.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td> 
						</td>
					</tr>

<!--					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<th scope="row"><?php _e('Hours in Life', '3dprint');?></th>
						<td>
							<span class="printer_hours_in_life"></span>
							<?php _e('hours', '3dprint');?>
						</td>
					</tr>-->
					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<th scope="row"><?php _e('Depreciation', '3dprint');?></th>
						<td>
							<input class="printer_depreciation" type="text" size="3" name="3dp_printer_depreciation_hourly_cost[<?php echo $printer['id'];?>]" value="<?php echo $printer['printer_depreciation_hourly_cost'];?>">
							<?php echo get_woocommerce_currency_symbol(); ?>/<?php _e('hr','3dprint')?>
						</td>
					</tr>
					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<td colspan="2" align="center"><b><?php _e('Repairs', '3dprnit');?></b></td>
					</tr>

					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<th scope="row"><?php _e('Repair Cost', '3dprint');?></th>
						<td>
							<input onkeyup="p3dCalculateRepair();" name="3dp_printer_repair_cost[<?php echo $printer['id'];?>]" <?php if ($settings['api_analyse']!='on') echo 'readonly="readonly"';?> type="text" size="4" value="<?php echo $printer['printer_repair_cost'];?>">
							<?php _e('% of Purchase Price', '3dprint');?>
							<img class="tooltip" data-title="<?php esc_attr_e( 'Requires depreciation fields above to be set.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td> 
						</td>
					</tr>
					<tr valign="top" class="printer_fff printer_dlp p3d-advanced">
						<th scope="row"><?php _e('Repairs', '3dprint');?></th>
						<td>
							<input class="printer_repair" type="text" size="3" name="3dp_printer_repair_hourly_cost[<?php echo $printer['id'];?>]" value="<?php echo $printer['printer_repair_hourly_cost'];?>">
							<?php echo get_woocommerce_currency_symbol(); ?>/<?php _e('hr', '3dprint');?>
						</td>
					</tr>


					<tr valign="top">
						<th scope="row"><?php _e( 'Printing Cost', '3dprint' ); ?></th>
						<td>
							<input type="text" name="3dp_printer_price[<?php echo $printer['id'];?>]" value="<?php echo $printer['price'];?>" /><?php echo get_woocommerce_currency_symbol(); ?> <?php _e('per', '3dprint');?>
							<select class="printer_type" name="3dp_printer_price_type[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['price_type']=='box_volume' ) echo "selected";?> value="box_volume"><?php _e('1 cm3 of Bounding Box Volume', '3dprint');?></option>
								<option <?php if ( $printer['price_type']=='material_volume' ) echo "selected";?> value="material_volume"><?php _e('1 cm3 of Material Volume', '3dprint');?></option>
								<option <?php if ( $printer['price_type']=='removed_material_volume' ) echo "selected";?> value="removed_material_volume"><?php _e('1 cm3 of Removed Material Volume (bounding box volume - material volume)', '3dprint');?></option>
								<option <?php if ( $printer['price_type']=='gram' ) echo "selected";?> value="gram"><?php _e('1 gram of Material', '3dprint');?></option>
								<option <?php if ( $printer['price_type']=='fixed' ) echo "selected";?> value="fixed"><?php _e('Fixed Price', '3dprint');?></option>
								<option <?php if ( $printer['price_type']=='hour' ) echo "selected";?> value="hour"><?php _e('1 Hour (Analyse API required)', '3dprint');?></option>
								<option <?php if ( $printer['price_type']=='pct' ) echo "selected";?> value="pct"><?php _e('+% to total price', '3dprint');?></option>
							</select>
						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Extra Price', '3dprint' ); ?> 1</th>
						<td>
							<input type="text" name="3dp_printer_price1[<?php echo $printer['id'];?>]" value="<?php echo $printer['price1'];?>" /><?php echo get_woocommerce_currency_symbol(); ?> <?php _e('per', '3dprint');?>
							<select class="printer_type" name="3dp_printer_price_type1[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['price_type1']=='box_volume' ) echo "selected";?> value="box_volume"><?php _e('1 cm3 of Bounding Box Volume', '3dprint');?></option>
								<option <?php if ( $printer['price_type1']=='material_volume' ) echo "selected";?> value="material_volume"><?php _e('1 cm3 of Material Volume', '3dprint');?></option>
								<option <?php if ( $printer['price_type1']=='removed_material_volume' ) echo "selected";?> value="removed_material_volume"><?php _e('1 cm3 of Removed Material Volume (bounding box volume - material volume)', '3dprint');?></option>
								<option <?php if ( $printer['price_type1']=='gram' ) echo "selected";?> value="gram"><?php _e('1 gram of Material', '3dprint');?></option>
								<option <?php if ( $printer['price_type1']=='hour' ) echo "selected";?> value="hour"><?php _e('1 Hour (Analyse API required)', '3dprint');?></option>
								<option <?php if ( $printer['price_type1']=='fixed' ) echo "selected";?> value="fixed"><?php _e('Fixed Price', '3dprint');?></option>
								<option <?php if ( $printer['price_type1']=='pct' ) echo "selected";?> value="pct"><?php _e('+% to total price', '3dprint');?></option>
							</select>
						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Extra Price', '3dprint' ); ?> 2</th>
						<td>
							<input type="text" name="3dp_printer_price2[<?php echo $printer['id'];?>]" value="<?php echo $printer['price2'];?>" /><?php echo get_woocommerce_currency_symbol(); ?> <?php _e('per', '3dprint');?>
							<select class="printer_type" name="3dp_printer_price_type2[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['price_type2']=='box_volume' ) echo "selected";?> value="box_volume"><?php _e('1 cm3 of Bounding Box Volume', '3dprint');?></option>
								<option <?php if ( $printer['price_type2']=='material_volume' ) echo "selected";?> value="material_volume"><?php _e('1 cm3 of Material Volume', '3dprint');?></option>
								<option <?php if ( $printer['price_type2']=='removed_material_volume' ) echo "selected";?> value="removed_material_volume"><?php _e('1 cm3 of Removed Material Volume (bounding box volume - material volume)', '3dprint');?></option>
								<option <?php if ( $printer['price_type2']=='gram' ) echo "selected";?> value="gram"><?php _e('1 gram of Material', '3dprint');?></option>
								<option <?php if ( $printer['price_type2']=='hour' ) echo "selected";?> value="hour"><?php _e('1 Hour (Analyse API required)', '3dprint');?></option>
								<option <?php if ( $printer['price_type2']=='fixed' ) echo "selected";?> value="fixed"><?php _e('Fixed Price', '3dprint');?></option>
								<option <?php if ( $printer['price_type2']=='pct' ) echo "selected";?> value="pct"><?php _e('+% to total price', '3dprint');?></option>
							</select>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Extra Price', '3dprint' ); ?> 3</th>
						<td>
							<input type="text" name="3dp_printer_price3[<?php echo $printer['id'];?>]" value="<?php echo $printer['price3'];?>" /><?php echo get_woocommerce_currency_symbol(); ?> <?php _e('per', '3dprint');?>
							<select class="printer_type" name="3dp_printer_price_type3[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['price_type3']=='box_volume' ) echo "selected";?> value="box_volume"><?php _e('1 cm3 of Bounding Box Volume', '3dprint');?></option>
								<option <?php if ( $printer['price_type3']=='material_volume' ) echo "selected";?> value="material_volume"><?php _e('1 cm3 of Material Volume', '3dprint');?></option>
								<option <?php if ( $printer['price_type3']=='removed_material_volume' ) echo "selected";?> value="removed_material_volume"><?php _e('1 cm3 of Removed Material Volume (bounding box volume - material volume)', '3dprint');?></option>
								<option <?php if ( $printer['price_type3']=='gram' ) echo "selected";?> value="gram"><?php _e('1 gram of Material', '3dprint');?></option>
								<option <?php if ( $printer['price_type3']=='hour' ) echo "selected";?> value="hour"><?php _e('1 Hour (Analyse API required)', '3dprint');?></option>
								<option <?php if ( $printer['price_type3']=='fixed' ) echo "selected";?> value="fixed"><?php _e('Fixed Price', '3dprint');?></option>
								<option <?php if ( $printer['price_type3']=='pct' ) echo "selected";?> value="pct"><?php _e('+% to total price', '3dprint');?></option>
							</select>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Extra Price', '3dprint' ); ?> 4</th>
						<td>
							<input type="text" name="3dp_printer_price4[<?php echo $printer['id'];?>]" value="<?php echo $printer['price4'];?>" /><?php echo get_woocommerce_currency_symbol(); ?> <?php _e('per', '3dprint');?>
							<select class="printer_type" name="3dp_printer_price_type4[<?php echo $printer['id'];?>]">
								<option <?php if ( $printer['price_type4']=='box_volume' ) echo "selected";?> value="box_volume"><?php _e('1 cm3 of Bounding Box Volume', '3dprint');?></option>
								<option <?php if ( $printer['price_type4']=='material_volume' ) echo "selected";?> value="material_volume"><?php _e('1 cm3 of Material Volume', '3dprint');?></option>
								<option <?php if ( $printer['price_type4']=='removed_material_volume' ) echo "selected";?> value="removed_material_volume"><?php _e('1 cm3 of Removed Material Volume (bounding box volume - material volume)', '3dprint');?></option>
								<option <?php if ( $printer['price_type4']=='gram' ) echo "selected";?> value="gram"><?php _e('1 gram of Material', '3dprint');?></option>
								<option <?php if ( $printer['price_type4']=='hour' ) echo "selected";?> value="hour"><?php _e('1 Hour (Analyse API required)', '3dprint');?></option>
								<option <?php if ( $printer['price_type4']=='fixed' ) echo "selected";?> value="fixed"><?php _e('Fixed Price', '3dprint');?></option>
								<option <?php if ( $printer['price_type4']=='pct' ) echo "selected";?> value="pct"><?php _e('+% to total price', '3dprint');?></option>
							</select>
						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Materials', '3dprint' ); ?></th>
						<td>

							<select autocomplete="off" name="3dp_printer_materials[<?php echo $printer['id'];?>][]" multiple="multiple" class="sumoselect">
								<?php 
									foreach ($materials as $j => $material) {
										if (strlen($printer['materials'])>0 && in_array($materials[$j]['id'], explode(',',$printer['materials']))) $selected="selected"; else $selected="";
										echo '<option '.$selected.' value="'.$materials[$j]['id'].'">'.$materials[$j]['name'];
									}
								?>
							</select>
						</td>
					</tr>


					<tr valign="top">
						<th scope="row"><?php _e( 'Group Name', '3dprint' ); ?></th>
						<td><input type="text" name="3dp_printer_group_name[<?php echo $printer['id'];?>]" value="<?php echo $printer['group_name'];?>" /></td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Sort Order', '3dprint' ); ?></th>
						<td><input type="text" name="3dp_printer_sort_order[<?php echo $printer['id'];?>]" value="<?php echo (int)$printer['sort_order'];?>" /></td>
					</tr>


				</table>
				</div>
				</div>
				<button class="p3d-clone-button button-secondary" onclick="p3dAddPrinter(<?php echo $printer['id'];?>, <?php echo (int)p3d_get_max_id ('3dp_printers');?>);return false;"><?php _e('Clone', '3dprint');?></button>
				<button style="<?php  if (count( $printers )==1) echo 'display:none;';?>" class="p3d-remove-button button-secondary" onclick="p3dRemovePrinter(<?php echo $printer['id'];?>);return false;"><?php _e('Remove', '3dprint');?></button>
				<br style="clear:both">
<?php
			$i++;
		}
	}
?>
				<br style="clear:both">
				<button id="add_printer_button" class="button-secondary" onclick="p3dAddPrinter(false, <?php echo (int)p3d_get_max_id ('3dp_printers');?>);return false;"><?php _e( 'Add Printer', '3dprint' );?></button>
				<input type="hidden" name="action" value="update" />
				<input type="hidden" name="page_options" value="new_option_name,some_other_option,option_etc" />

				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e( 'Save Changes', '3dprint' ) ?>" />
				</p>
			</form>
		</div><!-- 3dp_tabs-1 -->
		<div id="3dp_tabs-2">
			<form method="post" action="admin.php?page=3dprint&cpage=1&p3d_section=p3d_materials#3dp_tabs-2" enctype="multipart/form-data">

<?php
	echo paginate_links( array(
        	'base' => add_query_arg( array('cpage' => '%#%', 'p3d_section' => 'p3d_materials') ).'#3dp_tabs-2',
	        'format' => '',
	        'prev_text' => __('&laquo;'),
	        'next_text' => __('&raquo;'),
	        'total' => ceil(p3d_get_option_total('p3d_materials') / $settings['items_per_page']),
	        'current' => ((isset($_GET['cpage']) && is_numeric($_GET['cpage']) && $_GET['p3d_section']=='p3d_materials') ? $_GET['cpage'] : 1)
	));
?>
<br>
<?php
	if ( is_array( $paginated_materials ) && count( $paginated_materials )>0 ) {
		$i=0;
		foreach ( $paginated_materials as $material ) {

?>

				<div class="p3d-expand">
				<h3><?php echo '#'.$material['id'].' <div class="group-color-sample" style="background-color:'.$material['color'].';"></div>&nbsp;'.$material['name'];?></h3>
				<div>
				<table id="material-<?php echo $material['id'];?>" class="form-table material">

				 	<tr>
						<td colspan="2"><span class="item_id"><?php echo "<b>ID #".$material['id']."</b>";?></span></td>
				 	</tr>

				 	<tr valign="top">
						<th scope="row"><?php _e( 'Material Name', '3dprint' );?></th>
						<td>
							<input type="text" name="3dp_material_name[<?php echo $material['id'];?>]" value="<?php echo $material['name'];?>" />&nbsp;
						</td>
					</tr>

					<tr valign="top">
						<th scope="row">
							<?php _e( 'Material Description', '3dprint' ); ?>
						</th>
						<td>
							<textarea name="3dp_material_description[<?php echo $material['id'];?>]" /><?php echo $material['description'];?></textarea>
						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Photo', '3dprint' );?></th>
						<td>
							<a href="<?php echo $material['photo'];?>"><img class="p3d-preview" src="<?php echo esc_url($material['photo']);?>"></a>
							<input type="text" name="3dp_material_photo[<?php echo $material['id'];?>]" value="<?php echo esc_url($material['photo']);?>" />
							<input type="file" name="3dp_material_photo_upload[<?php echo $material['id'];?>]" accept="image/*">
						</td>

					</tr>
					<tr>
						<th scope="row"><?php _e( 'Enabled', '3dprint' );?></th>
						<td>
							<select name="3dp_material_status[<?php echo $material['id'];?>]">
								<option <?php if ( $material['status']=='1' ) echo "selected";?> value="1"><?php _e('Yes', '3dprint');?></option>
								<option <?php if ( $material['status']=='0' ) echo "selected";?> value="0"><?php _e('No', '3dprint');?></option>
							</select>

						</td>
					</tr>

				 	<tr valign="top">
						<th scope="row"><?php _e( 'Material Type', '3dprint' );?></th>
						<td>
							<select class="select_material" name="3dp_material_type[<?php echo $material['id'];?>]" onchange="p3dSetMaterialType(this)">
								<option <?php if ( $material['type']=='filament' ) echo "selected";?> value="filament"><?php _e( 'Filament', '3dprint' );?>
								<option <?php if ( $material['type']=='other' ) echo "selected";?> value="other"><?php _e( 'Other', '3dprint' );?>
							</select>
						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Price', '3dprint' ); ?></th>
						<td>
							<input type="text" class="3dp_price" name="3dp_material_price[<?php echo $material['id'];?>]" value="<?php echo $material['price'];?>" /><?php echo get_woocommerce_currency_symbol(); ?> <?php _e('per', '3dprint');?>
							<select class="3dp_price_type"  name="3dp_material_price_type[<?php echo $material['id'];?>]">
								<option <?php if ( $material['price_type']=='cm3' ) echo "selected";?> value="cm3"><?php _e('1 cm3', '3dprint');?></option>
								<option <?php if ( $material['price_type']=='box_volume' ) echo "selected";?> value="box_volume"><?php _e('1 cm3 of Bounding Box Volume', '3dprint');?></option>
								<option <?php if ( $material['price_type']=='removed_material_volume' ) echo "selected";?> value="removed_material_volume"><?php _e('1 cm3 of Removed Material Volume (bounding box volume - material volume)', '3dprint');?></option>
								<option <?php if ( $material['price_type']=='gram' ) echo "selected";?> value="gram"><?php _e('1 gram', '3dprint');?></option>
								<option <?php if ( $material['price_type']=='hour' ) echo "selected";?> value="hour"><?php _e('1 Hour (Analyse API required)', '3dprint');?></option>
								<option <?php if ( $material['price_type']=='fixed' ) echo "selected";?> value="fixed"><?php _e('Fixed Price', '3dprint');?></option>
								<option <?php if ( $material['price_type']=='pct' ) echo "selected";?> value="pct"><?php _e('+% to total price', '3dprint');?></option>
							</select>
							<a class="material_filament" onclick="javascript:p3dCalculateFilamentPrice(this, '')" href="javascript:void(0)"><?php _e( 'Calculate', '3dprint' );?></a>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Extra Price', '3dprint' ); ?> 1</th>
						<td>
							<input type="text" class="3dp_price1" name="3dp_material_price1[<?php echo $material['id'];?>]" value="<?php echo $material['price1'];?>" /><?php echo get_woocommerce_currency_symbol(); ?> <?php _e('per', '3dprint');?>
							<select class="3dp_price_type1"  name="3dp_material_price_type1[<?php echo $material['id'];?>]">
								<option <?php if ( $material['price_type1']=='cm3' ) echo "selected";?> value="cm3"><?php _e('1 cm3', '3dprint');?></option>
								<option <?php if ( $material['price_type1']=='box_volume' ) echo "selected";?> value="box_volume"><?php _e('1 cm3 of Bounding Box Volume', '3dprint');?></option>
								<option <?php if ( $material['price_type1']=='removed_material_volume' ) echo "selected";?> value="removed_material_volume"><?php _e('1 cm3 of Removed Material Volume (bounding box volume - material volume)', '3dprint');?></option>
								<option <?php if ( $material['price_type1']=='gram' ) echo "selected";?> value="gram"><?php _e('1 gram', '3dprint');?></option>
								<option <?php if ( $material['price_type1']=='hour' ) echo "selected";?> value="hour"><?php _e('1 Hour (Analyse API required)', '3dprint');?></option>
								<option <?php if ( $material['price_type1']=='fixed' ) echo "selected";?> value="fixed"><?php _e('Fixed Price', '3dprint');?></option>
								<option <?php if ( $material['price_type1']=='pct' ) echo "selected";?> value="pct"><?php _e('+% to total price', '3dprint');?></option>
							</select>
							<a class="material_filament" onclick="javascript:p3dCalculateFilamentPrice(this, '1')" href="javascript:void(0)"><?php _e( 'Calculate', '3dprint' );?></a>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Extra Price', '3dprint' ); ?> 2</th>
						<td>
							<input type="text" class="3dp_price2" name="3dp_material_price2[<?php echo $material['id'];?>]" value="<?php echo $material['price2'];?>" /><?php echo get_woocommerce_currency_symbol(); ?> <?php _e('per', '3dprint');?>
							<select class="3dp_price_type2"  name="3dp_material_price_type2[<?php echo $material['id'];?>]">
								<option <?php if ( $material['price_type2']=='cm3' ) echo "selected";?> value="cm3"><?php _e('1 cm3', '3dprint');?></option>
								<option <?php if ( $material['price_type2']=='box_volume' ) echo "selected";?> value="box_volume"><?php _e('1 cm3 of Bounding Box Volume', '3dprint');?></option>
								<option <?php if ( $material['price_type2']=='removed_material_volume' ) echo "selected";?> value="removed_material_volume"><?php _e('1 cm3 of Removed Material Volume (bounding box volume - material volume)', '3dprint');?></option>
								<option <?php if ( $material['price_type2']=='gram' ) echo "selected";?> value="gram"><?php _e('1 gram', '3dprint');?></option>
								<option <?php if ( $material['price_type2']=='hour' ) echo "selected";?> value="hour"><?php _e('1 Hour (Analyse API required)', '3dprint');?></option>
								<option <?php if ( $material['price_type2']=='fixed' ) echo "selected";?> value="fixed"><?php _e('Fixed Price', '3dprint');?></option>
								<option <?php if ( $material['price_type2']=='pct' ) echo "selected";?> value="pct"><?php _e('+% to total price', '3dprint');?></option>
							</select>
							<a class="material_filament" onclick="javascript:p3dCalculateFilamentPrice(this, '2')" href="javascript:void(0)"><?php _e( 'Calculate', '3dprint' );?></a>
						</td>
					</tr>




					<tr valign="top">
						<th scope="row"><?php _e( 'Material Density', '3dprint' );?></th>
						<td>
							<input type="text" name="3dp_material_density[<?php echo $material['id'];?>]" value="<?php echo $material['density'];?>" /><?php _e( 'g/cm3', '3dprint' );?>
							<a class="material_filament" onclick="javascript:p3dCalculateFilamentDensity(this)" href="javascript:void(0)"><?php _e( 'Calculate', '3dprint' );?></a>
						</td>
					</tr>

					<tr class="material_filament" valign="top">
						<th scope="row"><?php _e( 'Filament Diameter', '3dprint' );?></th>
						<td><input type="text" class="3dp_diameter" name="3dp_material_diameter[<?php echo $material['id'];?>]" value="<?php echo $material['diameter'];?>" /><?php _e( 'mm', '3dprint' );?></td>
					</tr>

					<tr class="material_filament" valign="top">
						<th scope="row"><?php _e( 'Filament Length', '3dprint' );?></th>
						<td><input type="text" class="3dp_length" name="3dp_material_length[<?php echo $material['id'];?>]" value="<?php echo $material['length'];?>" /><?php _e( 'm', '3dprint' );?></td>
					</tr>

					<tr class="material_filament" valign="top">
						<th scope="row"><?php _e( 'Roll Weight', '3dprint' );?></th>
						<td><input type="text" class="3dp_weight" name="3dp_material_weight[<?php echo $material['id'];?>]" value="<?php echo $material['weight'];?>" /><?php _e( 'kg', '3dprint' );?></td>
					</tr>

					<tr class="material_filament" valign="top">
						<th scope="row"><?php _e( 'Roll Price', '3dprint' );?></th>
						<td><input type="text" class="3dp_roll_price" name="3dp_material_roll_price[<?php echo $material['id'];?>]" value="<?php echo $material['roll_price'];?>" /><?php echo get_woocommerce_currency_symbol(); ?></td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Material Color', '3dprint' );?></th>
						<td class="color_td"><input type="text" class="3dp_color_picker" name="3dp_material_color[<?php echo $material['id'];?>]" value="<?php echo $material['color'];?>" /></td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Material Shininess', '3dprint' );?></th>
						<td>
							<select name="3dp_material_shininess[<?php echo $material['id'];?>]">
								<option <?php if ( $material['shininess']=='plastic') echo "selected";?> value="plastic"><?php _e('Plastic', '3dprint');?></option>
								<option <?php if ( $material['shininess']=='wood' ) echo "selected";?> value="wood"><?php _e('Wood', '3dprint');?></option>
								<option <?php if ( $material['shininess']=='metal' ) echo "selected";?> value="metal"><?php _e('Metal', '3dprint');?></option>
							</select>
						</td>

					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Material Glow', '3dprint' );?></th>
						<td>
							<select name="3dp_material_glow[<?php echo $material['id'];?>]">
								<option <?php if ( $material['glow']=='0') echo "selected";?> value="0"><?php _e('No', '3dprint');?></option>
								<option <?php if ( $material['glow']=='1' ) echo "selected";?> value="1"><?php _e('Yes', '3dprint');?></option>
							</select>
						</td>

					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Material Transparency', '3dprint' );?></th>
						<td>
							<select name="3dp_material_transparency[<?php echo $material['id'];?>]">
								<option <?php if ( $material['transparency']=='opaque') echo "selected";?> value="opaque"><?php _e('Opaque', '3dprint');?></option>
								<option <?php if ( $material['transparency']=='resin' ) echo "selected";?> value="resin"><?php _e('Resin', '3dprint');?></option>
								<option <?php if ( $material['transparency']=='glass' ) echo "selected";?> value="glass"><?php _e('Glass', '3dprint');?></option>
							</select>
						</td>

					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Group Name', '3dprint' );?></th>
						<td><input type="text" name="3dp_material_group_name[<?php echo $material['id'];?>]" value="<?php echo $material['group_name'];?>" /></td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Sort Order', '3dprint' );?></th>
						<td><input type="text" name="3dp_material_sort_order[<?php echo $material['id'];?>]" value="<?php echo (int)$material['sort_order'];?>" /></td>
					</tr>
				</table>
				</div>
				</div>
				<button class="p3d-clone-button button-secondary" onclick="p3dAddMaterial(<?php echo $material['id'];?>, <?php echo (int)p3d_get_max_id ('3dp_materials');?>);return false;"><?php _e('Clone', '3dprint');?></button>
				<button style="<?php  if (count( $materials )==1) echo 'display:none;';?>" class="p3d-remove-button button-secondary" onclick="p3dRemoveMaterial(<?php echo $material['id'];?>);return false;"><?php _e('Remove', '3dprint');?></button>
				<br style="clear:both">
<?php
			$i++;
		}
	}
?>
				<br style="clear:both">
				<button id="add_material_button" class="button-secondary" onclick="p3dAddMaterial(false, <?php echo (int)p3d_get_max_id ('3dp_materials');?>);return false;"><?php _e( 'Add Material', '3dprint' );?></button>
				<input type="hidden" name="action" value="update" />
				<input type="hidden" name="page_options" value="new_option_name,some_other_option,option_etc" />

				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e( 'Save Changes', '3dprint' ) ?>" />
				</p>

			</form>

		</div><!-- 3dp_tabs-2 -->

		<div id="3dp_tabs-3">
			<form method="post" action="admin.php?page=3dprint&cpage=1&p3d_section=p3d_coatings#3dp_tabs-3" enctype="multipart/form-data">
<?php
	echo paginate_links( array(
        	'base' => add_query_arg( array('cpage' => '%#%', 'p3d_section' => 'p3d_coatings') ).'#3dp_tabs-3',
	        'format' => '',
	        'prev_text' => __('&laquo;'),
	        'next_text' => __('&raquo;'),
	        'total' => ceil(p3d_get_option_total('p3d_coatings') / $settings['items_per_page']),
	        'current' => ((isset($_GET['cpage']) && is_numeric($_GET['cpage']) && $_GET['p3d_section']=='p3d_coatings') ? $_GET['cpage'] : 1)
	));
?>
<br>
<?php
			if ( !is_array($paginated_coatings) || count( $paginated_coatings )==0 ) {
?>
				<table class="form-table coating">
					<tr>
						<td colspan="2"><hr></td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Coating Name', '3dprint' );?></th>
						<td><input type="text" name="3dp_coating_name[1]" value="" /></td>
					</tr>

					<tr valign="top">
						<th scope="row">
							<?php _e( 'Coating Description', '3dprint' ); ?>
						</th>
						<td>
							<textarea name="3dp_coating_description[1]" /></textarea>
						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Photo', '3dprint' );?></th>
						<td>
							<input type="text" name="3dp_coating_photo[1]" value="" />
							<input type="file" name="3dp_coating_photo_upload[1]" accept="image/*">
						</td>

					</tr>
					<tr>
						<th scope="row"><?php _e( 'Enabled', '3dprint' );?></th>
						<td>
							<select name="3dp_coating_status[1]">
								<option value="1"><?php _e('Yes', '3dprint');?></option>
								<option value="0"><?php _e('No', '3dprint');?></option>
							</select>

						</td>
					</tr>
	
					<tr valign="top">
						<th scope="row"><?php _e( 'Price', '3dprint' ); ?></th>
						<td>
							<input type="text" class="3dp_price" name="3dp_coating_price[1]" value="" /><?php echo get_woocommerce_currency_symbol(); ?> <?php _e('per', '3dprint');?> 
							<select name="3dp_coating_price_type[1]">
								<option value="cm2"><?php _e('cm2 of surface area', '3dprint');?></option>
								<option value="fixed"><?php _e('Fixed Price', '3dprint');?></option>
								<option value="pct"><?php _e('+% to total price', '3dprint');?></option>
							</select>

							
					 	</td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Extra Price', '3dprint' ); ?> 1</th>
						<td>
							<input type="text" class="3dp_price" name="3dp_coating_price1[1]" value="" /><?php echo get_woocommerce_currency_symbol(); ?> <?php _e('per', '3dprint');?> 
							<select name="3dp_coating_price_type1[1]">
								<option value="cm2"><?php _e('cm2 of surface area', '3dprint');?></option>
								<option value="fixed"><?php _e('Fixed Price', '3dprint');?></option>
								<option value="pct"><?php _e('+% to total price', '3dprint');?></option>
							</select>

							
					 	</td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Coating Color', '3dprint' );?></th>
						<td class="color_td"><input type="text" class="3dp_color_picker" name="3dp_coating_color[1]" value="" /></td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Coating Shininess', '3dprint' );?></th>
						<td>
							<select class="3dp_price_type"  name="3dp_coating_shininess[1]">
								<option value="none"><?php _e('None', '3dprint');?></option>
								<option value="plastic"><?php _e('Plastic', '3dprint');?></option>
								<option value="wood"><?php _e('Wood', '3dprint');?></option>
								<option value="metal"><?php _e('Metal', '3dprint');?></option>
							</select>
						</td>

					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Coating Glow', '3dprint' );?></th>
						<td>
							<select name="3dp_coating_glow[1]">
								<option value="0"><?php _e('No', '3dprint');?></option>
								<option value="1"><?php _e('Yes', '3dprint');?></option>
							</select>
						</td>

					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Coating Transparency', '3dprint' );?></th>
						<td>
							<select class="3dp_price_type"  name="3dp_coating_transparency[1]">
								<option value="none"><?php _e('None', '3dprint');?></option>
								<option value="opaque" checked><?php _e('Opaque', '3dprint');?></option>
							</select>
						</td>

					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Materials', '3dprint' ); ?></th>
						<td>
							<select autocomplete="off" name="3dp_coating_materials[1][]" multiple="multiple" class="sumoselect">
								<?php 
									foreach ($materials as $j => $material) {
										echo '<option value="'.$materials[$j]['id'].'">'.$materials[$j]['name'];
									}
								?>
							</select>
						</td>
					</tr>


					<tr valign="top">
						<th scope="row"><?php _e( 'Group Name', '3dprint' );?></th>
						<td><input type="text" name="3dp_coating_group_name[1]" value="" /></td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Sort Order', '3dprint' );?></th>
						<td><input type="text" name="3dp_coating_sort_order[1]" value="0" /></td>
					</tr>

				</table>
			<?php } ?>
<?php
	if ( is_array( $paginated_coatings ) && count( $paginated_coatings )>0 ) {
		$i=0;
		foreach ( $paginated_coatings as $coating ) {
?>

				<div class="p3d-expand">
				<h3><?php echo '#'.$coating['id'].' <div class="group-color-sample" style="background-color:'.$coating['color'].';"></div>&nbsp;'.$coating['name'];?></h3>
				<div>
				<table id="coating-<?php echo $coating['id'];?>" class="form-table coating">

				 	<tr>
						<td colspan="2"><span class="item_id"><?php echo "<b>ID #".$coating['id']."</b>";?></span></td>
				 	</tr>
				 	<tr valign="top">
					<th scope="row"><?php _e( 'Coating Name', '3dprint' );?></th>
						<td>
							<input type="text" name="3dp_coating_name[<?php echo $coating['id'];?>]" value="<?php echo $coating['name'];?>" />&nbsp;
						</td>
					</tr>
					<tr valign="top">
						<th scope="row">
							<?php _e( 'Coating Description', '3dprint' ); ?>
						</th>
						<td>
							<textarea name="3dp_coating_description[<?php echo $coating['id'];?>]" /><?php echo $coating['description'];?></textarea>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Photo', '3dprint' );?></th>
						<td>
							<a href="<?php echo $coating['photo'];?>"><img class="p3d-preview" src="<?php echo esc_url($coating['photo']);?>"></a>
							<input type="text" name="3dp_coating_photo[<?php echo $coating['id'];?>]" value="<?php echo esc_url($coating['photo']);?>" />
							<input type="file" name="3dp_coating_photo_upload[<?php echo $coating['id'];?>]" accept="image/*">
						</td>

					</tr>
					<tr>
						<th scope="row"><?php _e( 'Enabled', '3dprint' );?></th>
						<td>
							<select name="3dp_coating_status[<?php echo $coating['id'];?>]">
								<option <?php if ( $coating['status']=='1' ) echo "selected";?> value="1"><?php _e('Yes', '3dprint');?></option>
								<option <?php if ( $coating['status']=='0' ) echo "selected";?> value="0"><?php _e('No', '3dprint');?></option>
							</select>

						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Price', '3dprint' ); ?></th>
						<td>
							<input type="text" class="3dp_price" name="3dp_coating_price[<?php echo $coating['id'];?>]" value="<?php echo $coating['price'];?>" /><?php echo get_woocommerce_currency_symbol(); ?> <?php _e('per', '3dprint');?> 
							<select name="3dp_coating_price_type[<?php echo $coating['id'];?>]">
								<option <?php if ($coating['price_type']=='cm2') echo 'selected'; ?> value="cm2"><?php _e('cm2 of surface area', '3dprint');?></option>
								<option <?php if ($coating['price_type']=='fixed') echo 'selected'; ?> value="fixed"><?php _e('Fixed Price', '3dprint');?></option>
								<option <?php if ($coating['price_type']=='pct' ) echo "selected";?> value="pct"><?php _e('+% to total price', '3dprint');?></option>
							</select>

						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Extra Price', '3dprint' ); ?> 1</th>
						<td>
							<input type="text" class="3dp_price" name="3dp_coating_price1[<?php echo $coating['id'];?>]" value="<?php echo $coating['price1'];?>" /><?php echo get_woocommerce_currency_symbol(); ?> <?php _e('per', '3dprint');?> 
							<select name="3dp_coating_price_type1[<?php echo $coating['id'];?>]">
								<option <?php if ($coating['price_type1']=='cm2') echo 'selected'; ?> value="cm2"><?php _e('cm2 of surface area', '3dprint');?></option>
								<option <?php if ($coating['price_type1']=='fixed') echo 'selected'; ?> value="fixed"><?php _e('Fixed Price', '3dprint');?></option>
								<option <?php if ($coating['price_type1']=='pct' ) echo "selected";?> value="pct"><?php _e('+% to total price', '3dprint');?></option>
							</select>

						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Coating Color', '3dprint' );?></th>
						<td class="color_td"><input type="text" class="3dp_color_picker" name="3dp_coating_color[<?php echo $coating['id'];?>]" value="<?php echo $coating['color'];?>" /></td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Coating Shininess', '3dprint' );?></th>
						<td>
							<select class="3dp_price_type"  name="3dp_coating_shininess[<?php echo $coating['id'];?>]">
								<option <?php if ( $coating['shininess']=='none') echo "selected";?> value="none"><?php _e('None', '3dprint');?></option>
								<option <?php if ( $coating['shininess']=='plastic') echo "selected";?> value="plastic"><?php _e('Plastic', '3dprint');?></option>
								<option <?php if ( $coating['shininess']=='wood' ) echo "selected";?> value="wood"><?php _e('Wood', '3dprint');?></option>
								<option <?php if ( $coating['shininess']=='metal' ) echo "selected";?> value="metal"><?php _e('Metal', '3dprint');?></option>
							</select>
						</td>

					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Coating transparency', '3dprint' );?></th>
						<td>
							<select class="3dp_price_type"  name="3dp_coating_transparency[<?php echo $coating['id'];?>]">
								<option <?php if ( $coating['transparency']=='none') echo "selected";?> value="none"><?php _e('None', '3dprint');?></option>
								<option <?php if ( $coating['transparency']=='opaque') echo "selected";?> value="opaque"><?php _e('Opaque', '3dprint');?></option>
							</select>
						</td>

					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Coating Glow', '3dprint' );?></th>
						<td>
							<select name="3dp_coating_glow[<?php echo $coating['id'];?>]">
								<option <?php if ( $coating['glow']=='0') echo "selected";?> value="0"><?php _e('No', '3dprint');?></option>
								<option <?php if ( $coating['glow']=='1' ) echo "selected";?> value="1"><?php _e('Yes', '3dprint');?></option>
							</select>
						</td>
					</tr>



					<tr valign="top">
						<th scope="row"><?php _e( 'Materials', '3dprint' ); ?></th>
						<td>
							<select autocomplete="off" name="3dp_coating_materials[<?php echo $coating['id'];?>][]" multiple="multiple" class="sumoselect">
								<?php 
									foreach ($materials as $j => $material) {
										if (strlen($coating['materials'])>0 && in_array($materials[$j]['id'], explode(',',$coating['materials']))) $selected="selected"; else $selected="";
										echo '<option '.$selected.' value="'.$materials[$j]['id'].'">'.$materials[$j]['name'];
									}
								?>
							</select>
						</td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e( 'Group Name', '3dprint' );?></th>
						<td><input type="text" name="3dp_coating_group_name[<?php echo $coating['id'];?>]" value="<?php echo $coating['group_name'];?>" /></td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php _e( 'Sort Order', '3dprint' );?></th>
						<td><input type="text" name="3dp_coating_sort_order[<?php echo $coating['id'];?>]" value="<?php echo (int)$coating['sort_order'];?>" /></td>
					</tr>
				</table>
				</div>
				</div>
				<button class="p3d-clone-button button-secondary" onclick="p3dAddCoating(<?php echo $coating['id'];?>, <?php echo (int)p3d_get_max_id ('3dp_coatings');?>);return false;"><?php _e('Clone', '3dprint');?></button>
				<button class="p3d-remove-button button-secondary" onclick="p3dRemoveCoating(<?php echo $coating['id'];?>);return false;"><?php _e('Remove', '3dprint');?></button>
				<br style="clear:both">
<?php
			$i++;
		}
	}
?>
				<br style="clear:both">
				<button id="add_coating_button" class="button-secondary" onclick="p3dAddCoating(false, <?php echo (int)p3d_get_max_id ('3dp_coatings');?>);return false;"><?php _e( 'Add Coating', '3dprint' );?></button>
				<input type="hidden" name="action" value="update" />
				<input type="hidden" name="page_options" value="new_option_name,some_other_option,option_etc" />

				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e( 'Save Changes', '3dprint' ) ?>" />
				</p>

			</form>

		</div><!-- 3dp_tabs-3 -->


		<div id="3dp_tabs-4">
<?php
			if ($settings['use_ninjaforms']=='on') {
?>
			<p><?php echo sprintf(__('With NinjaForms you can also view your subscriptions <a href="%s">here</a>', '3dprint'), admin_url('edit.php?post_type=nf_sub'));?></p>
<?php
			}
?>
			<form method="post" action="admin.php?page=3dprint#3dp_tabs-4">
<?php
			if ( is_array( $price_requests ) && count( $price_requests )>0 ) {
?>
				<table class="form-table">
					<tr>
						<td>X</td>
						<td><?php _e( 'Product', '3dprint' );?></td>
						<td><?php _e( 'Customer', '3dprint' );?></td>
						<td><?php _e( 'Details', '3dprint' );?></td>
						<td><?php _e( 'Price', '3dprint' );?></td>
						<td><?php _e( 'Comment', '3dprint' );?></td>
					</tr>
<?php
		$db_printers=p3d_get_option( '3dp_printers' );
		$db_materials=p3d_get_option( '3dp_materials' );
		$db_coatings=p3d_get_option( '3dp_coatings' );
		

		foreach ( $price_requests as $product_key=>$price_request ) {

			list ( $product_id, $printer_id, $material_id, $coating_id, $infill, $unit, $scale, $email_address, $base64_filename ) = explode( '_', $product_key );

			if (!is_numeric($product_id)) { //new way
				$product_id = $price_request['product_id'];
				$printer_id = $price_request['printer_id'];
				$material_id = $price_request['material_id'];
				$coating_id = $price_request['coating_id'];
				$infill = $price_request['infill'];
				$unit = $price_request['unit'];
				$scale = $price_request['scale'];
				$email_address = $price_request['email_address'];
				$filename = $price_request['model_file'];
#print_r($price_requests[$product_key]);
#echo "$product_id, $printer_id, $material_id, $coating_id, $infill, $unit, $scale, $email_address, $filename";exit;
			}
			else {
				$filename=base64_decode( $base64_filename );
			}
			if(!get_permalink( $product_id )) continue; 

			if ( $price_request['price']=='' ) {
				$product = new WC_Product_Variable( $product_id );


				$attr_st='';

				foreach ( $price_request['attributes'] as $attr_key => $attr_value ) {
					if ( $attr_key=='attribute_pa_p3d_printer' ) {
						$attr_st.=__( "Printer" , '3dprint' )." : ".$price_request['printer']."<br>";
					}
					elseif ( $attr_key=='attribute_pa_p3d_infill') {
						$attr_st.=__( "Infill" , '3dprint' )." : ".$price_request['infill']."<br>";
					}
					elseif ( $attr_key=='attribute_pa_p3d_material' ) {
						$attr_st.=__( "Material" , '3dprint' )." : ".$price_request['material']."<br>";
					}
					elseif ( $attr_key=='attribute_pa_p3d_coating' && isset($price_request['coating'])) {
						$attr_st.=__( "Coating" , '3dprint' )." : ".$price_request['coating']."<br>";
					}

					elseif ( $attr_key=='attribute_pa_p3d_model' ) {
						$upload_dir = wp_upload_dir();

						$filepath = $upload_dir['basedir']."/p3d/$attr_value";
						$original_file = p3d_get_original($attr_value);

						$link = $upload_dir['baseurl'] ."/p3d/". rawurlencode($attr_value) ;

						$p3d_file_url_meta = get_post_meta($product_id, 'p3d_file_url'); $p3d_file_url=$p3d_file_url_meta[0];
						$p3d_product_price_type_meta = get_post_meta($product_id, 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];
						$original_link = $upload_dir['baseurl']."/p3d/".rawurlencode(p3d_basename($original_file));

						if (strlen($p3d_file_url)>0) {
							$original_link = $p3d_file_url;
							$p3d_original_file_url_meta = get_post_meta($product_id, 'p3d_original_file_url'); 
							if (strlen($p3d_original_file_url_meta[0])>0) $original_link=$p3d_original_file_url_meta[0];
			
							$attr_st.=__( 'Original file', '3dprint' ).' : <a target="_blank" href="'.$original_link.'">'.urldecode(urldecode(p3d_basename($original_link))).'</a> <br>';

						}
						elseif (file_exists($original_file) && p3d_basename($filepath) != p3d_basename($original_file)) {

							$original_link = $upload_dir['baseurl'] ."/p3d/". rawurlencode(p3d_basename($original_file)) ;
							$attr_st.=__( "Model" , '3dprint' )." : <a href='".$link."'>".p3d_basename( $attr_value )."</a><br>";
							$attr_st.=__( 'Original file', '3dprint' ).' : <a target="_blank" href="'.$original_link.'">'.urldecode(urldecode(p3d_basename($original_file))).'</a> <br>';

						}
						else {
							$attr_st.=__( "Model" , '3dprint' )." : <a href='".$link."'>".p3d_basename( $attr_value )."</a><br>";
						}




					}
					elseif ( $attr_key=='attribute_pa_p3d_unit' ) {
						$attr_st.=__( "Unit" , '3dprint' )." : ".__( $attr_value )."<br>";
					}
					else {

						$product_attributes=( $product->get_attributes() );
						$attribute_id=str_replace( 'attribute_', '', $attr_key );

						$attr_name=wc_attribute_label(str_replace('attribute_', '', $product_attributes[$attribute_id]['name'] ));
						$attr_value=p3d_attribute_slug_to_title($product_attributes[$attribute_id]['name'], $attr_value);
						$attr_st.= $attr_name." : $attr_value<br>";
						
					}
				}
				if (isset($price_request['estimated_price'])) {
					$attr_st.= __('Estimated Price:')."  ".wc_price($price_request['estimated_price'])."<br>";
				}
				$attr_st.= __('Dimensions', '3dprint')."  : ".$price_request['scale_x']." &times; ".$price_request['scale_y']." &times; ".$price_request['scale_z']." ".__('cm', '3dprint')."<br>";
				$attr_st.= __('Resize Scale', '3dprint').':'.$scale.'<br>';
				if ( version_compare( $woocommerce->version, '3.0.0', '<' ) ) { 
					$product_name = $product->post->post_title;
				}
				else {
					$product_name =  $product->get_name();
				}

				echo '
				<tr>
					<td>
						<a class="remove_request" href="javascript:void(0);" onclick="p3dRemoveRequest(\''.$product_key.'\');return false;">
							<img alt="'.__( 'Remove Request', '3dprint' ).'" title="'.__( 'Remove Request', '3dprint' ).'" src="'.plugins_url( '3dprint/images/remove.png' ).'">
						</a>
					</td>
					<td>'.$product_name.'</td>
					<td>
						'.__( 'Email:', '3dprint' ).' '.$price_request['email'].'<br>
						'.__( 'Comment:', '3dprint' ).' '.$price_request['request_comment'].'
					</td>
					<td>'.$attr_st.'</td>
					<td><span style="color:red;">*</span> <input name="p3d_buynow['.$product_key.']" type="text">'.get_woocommerce_currency_symbol().'</td>
					<td><textarea name="p3d_comments['.$product_key.']" style="width:250px;height:100px;" placeholder="'.__('Leave a comment.', '3dprint').'"></textarea></td>
				</tr>';
			}
		}
?>
				</table>
<?php
	}

?>
				<input type="hidden" name="action" value="update" />
				<input type="hidden" name="page_options" value="new_option_name,some_other_option,option_etc" />

				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e( 'Email Quotes', '3dprint' ) ?>" />
				</p>
			</form>
		</div><!-- 3dp_tabs-4 -->
		<div id="3dp_tabs-5">
			<form method="post" action="admin.php?page=3dprint#3dp_tabs-5">
				<p><b><?php _e('E-mail to admin', '3dprint'); ?>:</b></p>
				<?php
				if ($settings['use_ninjaforms']=='on') {
				?>
					<p style="color:red;"><?php echo sprintf(__('NinjaForms is being used. Please configure this e-mail on the <a href="%s">appropriate NinjaForms page.</a>', '3dprint'), admin_url( "admin.php?page=ninja-forms&form_id=$form_id" ));?>&nbsp;</p>
				<?php
				}
				?>
				<p><?php _e('Available shortcodes', '3dprint'); ?>:<?php echo implode(', ', array('[customer_email]','[product_id]','[printer_name]','[material_name]','[coating_name]','[infill]','[model_file]','[original_model_file]','[unit]','[resize_scale]','[dimensions]','[estimated_price]','[custom_attributes]','[customer_comments]','[price_requests_link]'));?></p>

				<p><?php _e('From', '3dprint');?>:<input type="text" size="50" name="3dp_email_templates[admin_email_from]" value="<?php echo esc_attr($current_templates['admin_email_from']);?>" />&nbsp;<i>Name Surname &#x3C;me@example.net&#x3E;</i>
					<img class="tooltip" data-title="<?php esc_attr_e( 'Please note that if you put a different domain name the email may go to spam.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td> 
				</p>

				<p><?php _e('Subject', '3dprint');?>:<input type="text" size="50" name="3dp_email_templates[admin_email_subject]" value="<?php echo esc_attr($current_templates['admin_email_subject']);?>" /></p>


				<?php wp_editor($current_templates['admin_email_body'], 'admin_email_body', array('textarea_name' => '3dp_email_templates[admin_email_body]') ); ?>

				<p><b><?php _e('E-mail to client', '3dprint'); ?>:</b></p>
				<p><?php _e('Available shortcodes', '3dprint'); ?>:<?php echo implode(', ', array('[printer_name]', '[infill]', '[material_name]', '[coating_name]', '[model_file]', '[dimensions]', '[custom_attributes]', '[price]', '[buy_link]', '[admin_comments]'));?></p>

				<p><?php _e('From', '3dprint');?>:<input type="text" size="50" name="3dp_email_templates[client_email_from]" value="<?php echo esc_attr($current_templates['client_email_from']);?>" />&nbsp;<i>Name Surname &#x3C;me@example.net&#x3E;</i>
					<img class="tooltip" data-title="<?php esc_attr_e( 'Please note that if you put a different domain name the email may go to spam.', '3dprint' );?>" src="<?php echo plugins_url( '3dprint/images/question.png' ); ?>"></td> 
				</p>

				<p><?php _e('Subject', '3dprint');?>:<input type="text" size="50" name="3dp_email_templates[client_email_subject]" value="<?php echo esc_attr($current_templates['client_email_subject']);?>" /></p>
				<?php wp_editor($current_templates['client_email_body'], 'client_email_body', array('textarea_name' => '3dp_email_templates[client_email_body]') ); ?>


				<input type="hidden" name="action" value="update" />
				<input type="hidden" name="page_options" value="new_option_name,some_other_option,option_etc" />

				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e( 'Save', '3dprint' ) ?>" />
				</p>
			</form>
		</div><!-- 3dp_tabs-5 -->


	</div><!-- 3dp_tabs -->
</div> <!-- wrap -->
<?php

}
?>