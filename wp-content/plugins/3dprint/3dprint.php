<?php
/*
 * Plugin Name: 3DPrint
 * Description: A plugin for selling 3D printing services.
 * Author: Sergey Burkov
 * Text Domain: 3dprint
 * Plugin URI: http://www.wp3dprinting.com/2015/07/29/changelog/
 * WC requires at least: 2.6
 * WC tested up to: 3.4
 * Version: 2.3.3.7
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

define('P3D_VERSION', '2.3.3.7');

global $wpdb;
if ( !function_exists( 'get_home_path' ) ) {
	require_once ABSPATH . '/wp-admin/includes/file.php';
}

include 'includes/3dprint-functions.php';
include 'includes/3dprint-model-functions.php';
require 'includes/ext/plugin-update-checker/plugin-update-checker.php';

$p3d_update_checker = PucFactory::buildUpdateChecker('http://srv1.wp3dprinting.com/wp-update-server/?action=get_metadata&slug=3dprint', __FILE__, '3dprint' );
$p3d_update_checker->addQueryArgFilter('p3d_filter_update_checks');

if ( is_admin() ) {
	add_action( 'admin_enqueue_scripts', 'p3d_enqueue_scripts_backend' );

	add_action( 'wp_ajax_p3d_handle_upload', 'p3d_handle_upload' );
	add_action( 'wp_ajax_nopriv_p3d_handle_upload', 'p3d_handle_upload' );

	add_action( 'wp_ajax_p3d_handle_repair', 'p3d_handle_repair' );
	add_action( 'wp_ajax_nopriv_p3d_handle_repair', 'p3d_handle_repair' );

	add_action( 'wp_ajax_p3d_handle_repair_check', 'p3d_handle_repair_check' );
	add_action( 'wp_ajax_nopriv_p3d_handle_repair_check', 'p3d_handle_repair_check' );

	add_action( 'wp_ajax_p3d_handle_process', 'p3d_handle_process' );
	add_action( 'wp_ajax_nopriv_p3d_handle_process', 'p3d_handle_process' );

	add_action( 'wp_ajax_p3d_handle_process_check', 'p3d_handle_process_check' );
	add_action( 'wp_ajax_nopriv_p3d_handle_process_check', 'p3d_handle_process_check' );

	add_action( 'wp_ajax_p3d_handle_analyse', 'p3d_handle_analyse' );
	add_action( 'wp_ajax_nopriv_p3d_handle_analyse', 'p3d_handle_analyse' );

	add_action( 'wp_ajax_p3d_handle_analyse_check', 'p3d_handle_analyse_check' );
	add_action( 'wp_ajax_nopriv_p3d_handle_analyse_check', 'p3d_handle_analyse_check' );

	add_action( 'wp_ajax_p3d_handle_triangulate', 'p3d_handle_triangulate' );
	add_action( 'wp_ajax_nopriv_p3d_handle_triangulate', 'p3d_handle_triangulate' );

        add_action( 'wp_ajax_p3d_handle_shortcode_generation', 'p3d_handle_shortcode_generation' );

	include 'includes/3dprint-admin.php';
}
else {
	add_action( 'wp_enqueue_scripts', 'p3d_enqueue_scripts_frontend' );
}


register_activation_hook( __FILE__, 'p3d_activate' );
register_deactivation_hook( __FILE__, 'p3d_deactivate' );

add_action('init', 'p3d_check_installation');
function p3d_check_installation() {

	if ( ! function_exists( 'get_plugin_data' ) ) {
		require_once ABSPATH . 'wp-admin/includes/plugin.php';
	}
	$p3d_plugin_data = get_plugin_data(  __FILE__ );
	$p3d_current_version = get_option('3dp_version');

	if (!empty($p3d_current_version) && version_compare($p3d_current_version, $p3d_plugin_data['Version'], '<')) {
		p3d_check_install();
	}
}

?>