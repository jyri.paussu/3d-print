<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce;
$settings = get_option('3dp_settings');
//workaround for shortcode pages
$p3d_file_url = get_post_meta($product->get_id(), 'p3d_file_url', true); 
$p3d_product_price_type = get_post_meta($product->get_id(), 'p3d_product_price_type', true); 
$p3d_product_display_mode = get_post_meta($product->get_id(), 'p3d_product_display_mode', true); 
$p3d_product_pricing = get_post_meta($product->get_id(), 'p3d_product_pricing', true); 


$upload_dir = wp_upload_dir();
$upload_url = $upload_dir['baseurl'].'/p3d/';
$upload_url = str_replace('http:','',$upload_url);
$upload_url = str_replace('https:','',$upload_url);
$p3d_mtl='';

if (strlen($p3d_file_url)>0) {

	if (strtolower(p3d_extension($p3d_file_url))=='obj') {
		$upload_dir = wp_upload_dir();
		$targetDir = dirname($upload_dir['basedir']).'/'.dirname(substr($p3d_file_url, strpos($p3d_file_url, 'uploads/'))).'/';
		$p3d_mtl=p3d_get_mtl($targetDir.p3d_basename($p3d_file_url));

		if (!file_exists(trim($targetDir.$p3d_mtl))) $p3d_mtl='' ;
	}
	$upload_url = dirname($upload_dir['baseurl']).'/'.dirname(substr($p3d_file_url, strpos($p3d_file_url, 'uploads/'))).'/';

	$p3d_file_url = str_replace('http:','',$p3d_file_url);
	$p3d_file_url = str_replace('https:','',$p3d_file_url);
}
$upload_url = str_replace('http:','',$upload_url);
$upload_url = str_replace('https:','',$upload_url);
$min_price = $product->get_price();

$p3d_product_pricing = get_post_meta($product->get_id(), 'p3d_product_pricing', true);
if ($p3d_product_pricing!='') {
	$settings['pricing'] = $p3d_product_pricing;
}
if ( $settings['pricing']=='request_estimate') $estimated_price = __( '<b>Estimated Price:</b>', '3dprint' );
else $estimated_price='';

if( version_compare( $woocommerce->version, '3.0.0', ">=" ) ) {
	$display_price = wc_get_price_to_display($product);
}
else {
	$display_price = $product->get_display_price();
}
?>
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
	<p class="price"><?php echo $estimated_price;?><span class="amount"><?php #echo $product->get_price(); ?></span></p>
	<input type="hidden" id="p3d_file_url" value="<?php echo $p3d_file_url;?>">
	<input type="hidden" id="p3d_mtl" value="<?php echo $p3d_mtl;?>">
	<input type="hidden" id="p3d_product_price_type" value="<?php echo $p3d_product_price_type;?>">
	<input type="hidden" id="p3d_product_display_mode" value="<?php echo $p3d_product_display_mode;?>">
	<input type="hidden" id="p3d_upload_url" value="<?php echo $upload_url;?>">
	<input type="hidden" id="p3d_product_id" value="<?php echo $product->get_id();?>">
	<input type="hidden" id="p3d_min_price" value="<?php echo $min_price;?>">
	<meta itemprop="price" content="<?php echo esc_attr( $display_price ); ?>" />
	<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

</div>