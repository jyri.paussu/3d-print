<div <?php if ($settings['show_materials']!='on') echo 'style="display:none;"';?> class="p3d-info">
	<fieldset id="material_fieldset" class="p3d-fieldset" >
		<legend id="p3d-material-name"><?php _e( 'Material', '3dprint' );?></legend>
		<ul class="p3d-bxslider p3d-list">
<?php
		$group_name=''; 
		for ( $i=0;$i<count( $db_materials );$i++ ) {
			if ( !is_array($attributes['pa_p3d_material']) ) continue;
			if ( in_array( $db_materials[$i]['id'], $attributes['pa_p3d_material'] ) || in_array( 'all', $attributes['pa_p3d_material'] ) ) {
				if (!empty($db_materials[$i]['group_name']) && $group_name!=$db_materials[$i]['group_name']) {
					echo '<li style="background:none;"><a href="javascript:void(0);">'.__($db_materials[$i]['group_name'],'3dprint').'</a><ul>';
					$group_name = $db_materials[$i]['group_name'];
				}
				echo '<li class="p3d-tooltip" data-tooltip-content="#p3d-tooltip-material-'.$db_materials[$i]['id'].'" data-color=\''.$db_materials[$i]['color'].'\' data-shininess=\''.$db_materials[$i]['shininess'].'\' data-glow=\''.$db_materials[$i]['glow'].'\' data-transparency=\''.$db_materials[$i]['transparency'].'\' data-name="'.esc_attr( $db_materials[$i]['name'] ).'" onclick="p3dSelectFilament(this);"><input id="p3d_material_'.$db_materials[$i]['id'].'" class="p3d-control" style="display:none;" autocomplete="off" type="radio" data-id="'. $db_materials[$i]['id'].'"  data-diameter="'.esc_attr( $db_materials[$i]['diameter'] ).'" data-density="'.esc_attr( $db_materials[$i]['density'] ).'" data-price="'.esc_attr($db_materials[$i]['price']).'" data-price_type="'.$db_materials[$i]['price_type'].'" data-price1="'.esc_attr($db_materials[$i]['price1']).'" data-price_type1="'.$db_materials[$i]['price_type1'].'" data-price1="'.esc_attr($db_printers[$i]['price1']).'" data-price2="'.esc_attr($db_materials[$i]['price2']).'" data-price_type2="'.$db_materials[$i]['price_type2'].'" data-color=\''.$db_materials[$i]['color'].'\' data-name="'.esc_attr( $db_materials[$i]['name'] ).'" name="product_filament" >';
				if ($db_materials[$i]['photo']!='') {
					echo 	'<div class="p3d-photo-sample" style="background-image:url(\''.$db_materials[$i]['photo'].'\');"></div>';
				}
				else {
					echo 	'<div style="background-color:'.$db_materials[$i]['color'].'" class="p3d-photo-sample"></div>';
				}
				echo 	__($db_materials[$i]['name'],'3dprint');
				echo '</li>';
				if (!empty($db_materials[$i]['group_name']) && (isset($db_materials[$i+1]) && $db_materials[$i+1]['group_name']!=$group_name) || $i==(count($db_materials)-1)) {
					echo '</ul></li>';
				}
			}
		}
?>
		</ul>
	</fieldset>
</div>
