<?php
/**
 * Variable product add to cart
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product, $post, $wpdb, $woocommerce;
require_once ABSPATH . 'wp-admin/includes/file.php';
$settings=p3d_get_option( '3dp_settings' );
$price = get_post_meta( get_the_ID(), '_regular_price' );


if( version_compare( $woocommerce->version, '3.0.0', ">=" ) ) {
	$fixed_atts=$product->get_attributes();
	foreach ($fixed_atts as $fixed_att_key => $fixed_att) {
		if (strstr($fixed_att_key, 'pa_p3d_')) {
			$new_attributes=array();
			foreach($fixed_att->get_terms() as $term) {
				$new_attributes[]=$term->name;
			}
			$attributes[$fixed_att_key]=$new_attributes;
		}
	}
}


if ( isset( $_POST['action'] ) && $_POST['action']=='request_price' && (!isset($_POST['add-to-cart']) || $_POST['add-to-cart']=='' ) ) {
#var_dump(isset($_POST['action']));
#var_dump(!isset($_POST['add-to-cart']));
#var_dump($_POST['add-to-cart']=='');
#var_dump((!isset($_POST['add-to-cart']) || $_POST['add-to-cart']==''));
#print_r($_POST);
	unset($_POST['attribute_pa_p3d_scale']);
	$product_id=(int)$_POST['product_id'];
	$printer_id=(int)$_POST['attribute_pa_p3d_printer'];
	$material_id=(int)$_POST['attribute_pa_p3d_material'];
	$scale=(float)$_POST['p3d_resize_scale'];

	if (is_numeric((int)$_POST['attribute_pa_p3d_infill']))
		$infill=(int)(int)$_POST['attribute_pa_p3d_infill'];
	else
		$infill='';
	if (is_numeric($_POST['attribute_pa_p3d_coating']))
		$coating_id=(int)$_POST['attribute_pa_p3d_coating'];
	else
		$coating_id='';
	if ($_POST['attribute_pa_p3d_unit']=='inch')
		$unit='inch';
	else
		$unit='mm';

	//$model_file=sanitize_file_name( p3d_basename( $_POST['attribute_pa_p3d_model'] ) );
	$model_file = p3d_basename( $_POST['attribute_pa_p3d_model'] );
	$email_address = sanitize_email( $_POST['email_address'] );
	$request_comment = sanitize_text_field( $_POST['request_comment'] );
	$resized_file_path = '';
	$custom_attributes = '';


	$db_printers=p3d_get_option( '3dp_printers' );
	$db_materials=p3d_get_option( '3dp_materials' );
	$db_coatings=p3d_get_option( '3dp_coatings' );
	$settings=p3d_get_option( '3dp_settings' );

	$thumbnail_data=$_REQUEST['p3d_thumb'];
	$thumbnail_url=p3d_save_thumbnail( $thumbnail_data, $model_file );

	$p3d_file_url_meta = get_post_meta($product_id, 'p3d_file_url'); $p3d_file_url = $p3d_file_url_meta[0];
	$p3d_product_price_type_meta = get_post_meta($product_id, 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];

	if ($scale!=1 || $unit!='mm') {
		$upload_dir = wp_upload_dir();
		$file_path=$upload_dir['basedir'].'/p3d/'.p3d_basename( $model_file );

		if (strlen($p3d_file_url)>0) {
			$uploads = dirname($upload_dir['basedir']).'/'.dirname(substr($p3d_file_url, strpos($p3d_file_url, 'uploads/'))).'/';
			$basename = p3d_basename( $p3d_file_url );
			$file_path = $uploads . $basename;
		}

		$path_parts = pathinfo($file_path);
		p3d_get_model_stats( $file_path, $unit, $scale, $printer_id, $infill );
		$resized_file_path = $upload_dir['basedir'].'/p3d/'.$path_parts['filename'].'_resized.'.$path_parts['extension'];
	
		if (file_exists($resized_file_path)) {
			$model_file=p3d_basename($resized_file_path);
			copy($upload_dir['basedir'].'/p3d/'.$path_parts['filename'].'.'.$path_parts['extension'].'.png', $resized_file_path.'.png');
		}
	}

	$error=false;
	$upload_dir = wp_upload_dir();




	if ( $p3d_file_url=='' && (strlen( $model_file )==0 || !file_exists( $upload_dir['basedir'].'/p3d/'.$model_file ) || strlen( $printer_id )==0 || strlen( $material_id )==0 )) {
		$error=true;
		$email_status_message='<span class="p3d-mail-error">'.__( 'Please upload your model and select all options.' , '3dprint').'</span>';
	}
	if ( empty( $email_address ) ) {
		$error=true;
		$email_status_message='<span class="p3d-mail-error">'.__( 'Please enter valid email address.' , '3dprint').'</span>';
	}
	if ( !$error ) {
		#$product_key=$product_id.'_'.$printer_id.'_'.$material_id.'_'.$coating_id.'_'.$infill.'_'.$unit.'_'.$scale.'_'.$email_address.'_'.base64_encode( $model_file );

		$product_key = p3d_generate_request_key($product_id, $printer_id, $material_id, $coating_id, $infill, $unit, $scale, $email_address, $model_file);

		$p3d_price_requests=(array)p3d_get_option( '3dp_price_requests' );
		$current_templates = get_option( '3dp_email_templates' );
		$template_body = $current_templates['admin_email_body'];
		$template_subject = $current_templates['admin_email_subject'];
		$from = $current_templates['admin_email_from'];
		$message='';


		if (!is_array($p3d_price_requests)) $p3d_price_requests = array();

		$p3d_price_requests[$product_key]['product_id'] = $product_id;
		$p3d_price_requests[$product_key]['material_id'] = $material_id;
		$p3d_price_requests[$product_key]['coating_id'] = $coating_id;
		$p3d_price_requests[$product_key]['unit'] = $unit;
		$p3d_price_requests[$product_key]['scale'] = $scale;
		$p3d_price_requests[$product_key]['email_address'] = $email_address;
		$p3d_price_requests[$product_key]['model_file'] = $model_file;
		$p3d_price_requests[$product_key]['printer'] = $db_printers[$printer_id]['name'];
		$p3d_price_requests[$product_key]['material'] = $db_materials[$material_id]['name'];
		$p3d_price_requests[$product_key]['thumbnail_url'] = $thumbnail_url;

		if (is_numeric($coating_id))
			$p3d_price_requests[$product_key]['coating'] = $db_coatings[$coating_id]['name'];
		if (is_numeric($infill))
			$p3d_price_requests[$product_key]['infill'] = "$infill %";
		foreach ( $_POST as $key => $value ) {
			if ( strpos( $key, 'attribute_' )===0 ) {
				if ( !strstr( $key, 'p3d_' ) ) {
					$email_attrs[$key]=$value;
				}

				$p3d_price_requests[$product_key]['attributes'][$key]=$value;
			}

		}
		if (file_exists($resized_file_path)) {
			$p3d_price_requests[$product_key]['attributes']['attribute_pa_p3d_model']=p3d_basename($resized_file_path);
		}

		//$p3d_price_requests[$product_key]['attributes']['attribute_unit']=$_POST['p3d_unit'];

		$p3d_price_requests[$product_key]['price']='';
		$p3d_price_requests[$product_key]['estimated_price']=(float)$_POST['p3d_estimated_price'];
		$p3d_price_requests[$product_key]['email']=$email_address;
		$p3d_price_requests[$product_key]['request_comment']=$request_comment;

#		$p3d_price_requests[$product_key]['scale']=(float)$_POST['resize_scale'];

		$p3d_price_requests[$product_key]['scale_x']=(float)$_POST['p3d_dim_x'];
		$p3d_price_requests[$product_key]['scale_y']=(float)$_POST['p3d_dim_y'];
		$p3d_price_requests[$product_key]['scale_z']=(float)$_POST['p3d_dim_z'];
		$dimensions=$p3d_price_requests[$product_key]['scale_x']." &times; ".$p3d_price_requests[$product_key]['scale_y']." &times; ".$p3d_price_requests[$product_key]['scale_z']." ".__('cm', '3dprint')."<br>";

		update_option( "3dp_price_requests", $p3d_price_requests );

		// $request_comment
		$upload_dir = wp_upload_dir();
		$link = $upload_dir['baseurl'].'/p3d/'.rawurlencode($model_file);


		if (is_numeric($infill) && $settings['api_analyse']=='on') $message.=__( "Infill" , '3dprint' ).": $infill % <br>";

		$filepath = $upload_dir['basedir']."/p3d/".rawurlencode($model_file);
		$original_file = p3d_get_original($filepath);

		$original_link = $upload_dir['baseurl']."/p3d/".rawurlencode(p3d_basename($original_file));
		if (strlen($p3d_file_url)>0) {
			$original_link = $p3d_file_url;
			$p3d_original_file_url_meta = get_post_meta($product_id, 'p3d_original_file_url'); 
			if (strlen($p3d_original_file_url_meta[0])>0) $original_link=$p3d_original_file_url_meta[0];
		}

		elseif (file_exists($original_file) && p3d_basename($filepath) != p3d_basename($original_file)) {
			$original_link = $upload_dir['baseurl']."/p3d/".rawurlencode(p3d_basename($original_file));
			$model_link="<a href='".$link."'>".$model_file."</a>";
			$original_link='<a target="_blank" href="'.$original_link.'">'.urldecode(urldecode(p3d_basename($original_file))).'</a>';
		}
		else {
			$model_link="<a href='".$link."'>".$model_file."</a>";
			$original_link="";
		}

		if ( isset( $email_attrs ) && count( $email_attrs ) ) {
			foreach ( $email_attrs as $key=>$value ) {
				$attr_name=wc_attribute_label(str_replace('attribute_', '', $key ));
				$attr_value=p3d_attribute_slug_to_title($key, $value);
				$custom_attributes.="$attr_name: $attr_value<br>";
			}
		}

		$replace_from = array('[customer_email]','[product_id]','[printer_name]','[material_name]','[coating_name]','[infill]','[model_file]','[original_model_file]','[unit]','[resize_scale]','[dimensions]','[estimated_price]','[custom_attributes]','[customer_comments]','[price_requests_link]');
		$replace_to = array($email_address, $product_id, $db_printers[$printer_id]['name'], $db_materials[$material_id]['name'], 
				   (isset($db_coatings[$coating_id]['name']) ? $db_coatings[$coating_id]['name'] : ''), (isset($infill) ? "$infill %" : ''), $model_link, $original_link, $unit, $scale, $dimensions,
				   wc_price($p3d_price_requests[$product_key]['estimated_price']), $custom_attributes, $request_comment, "<a href='".admin_url( 'admin.php?page=3dprint#3dp_tabs-4' )."'>".admin_url( 'admin.php?page=3dprint#3dp_tabs-4' )."</a>");
		$subject=str_ireplace($replace_from, $replace_to, $template_subject);
		$body=str_ireplace($replace_from, $replace_to, $template_body);



		$headers = array();
		$headers[] = "From: $from";
		$headers[] = 'Content-Type: text/html; charset=UTF-8';


		if ( wp_mail( $settings['email_address'], $subject, $body, $headers ) )
			$email_status_message='<span class="p3d-mail-success">'.__( 'Store owner has been notified about your request. You\'ll receive the email with the price shortly.' , '3dprint' ).'</span>';
		else
			$email_status_message='<span class="p3d-mail-error">'.__( 'Could not send the email. Please try again later.' , '3dprint' ).'</span>';

		do_action('3dprint_request_price');
	}
}


if ( isset( $_GET['attribute_pa_p3d_printer'] ) ) {
	$p3dprinter=explode( '.', $_GET['attribute_pa_p3d_printer'] );
	$printer_id=(int)$p3dprinter[0];
	echo '<input type="hidden" name="get_printer_id" value="'.$printer_id.'">';
}

if ( isset( $_GET['attribute_pa_p3d_material'] ) ) {
	$p3dmaterial=explode( '.', $_GET['attribute_pa_p3d_material'] );
	$material_id=(int)$p3dmaterial[0];
	echo '<input type="hidden" name="get_material_id" value="'.$material_id.'">';
}

if ( isset( $_GET['attribute_pa_p3d_coating'] ) ) {
	$p3dcoating=explode( '.', $_GET['attribute_pa_p3d_coating'] );
	$coating_id=(int)$p3dcoating[0];
	echo '<input type="hidden" name="get_coating_id" value="'.$coating_id.'">';
}

if ( isset( $_GET['attribute_pa_p3d_infill'] ) ) {
	$p3dinfill=(str_replace('%', '', $_GET['attribute_pa_p3d_infill']));
	echo '<input type="hidden" name="get_infill" value="'.$p3dinfill.'">';
}

if ( isset( $_GET['attribute_pa_p3d_unit'] ) ) {
	if ( $_GET['attribute_pa_p3d_unit']=='mm' ) $unit='mm';
	elseif ( $_GET['attribute_pa_p3d_unit']=='inch' ) $unit='inch';
	echo '<input type="hidden" name="get_product_unit" value="'.$unit.'">';
}

if ( isset( $_GET['attribute_pa_p3d_model'] ) ) {
	$p3dmodel=$_GET['attribute_pa_p3d_model'];
	echo '<input type="hidden" name="get_product_model" value="'.htmlspecialchars( strip_tags( urldecode($p3dmodel) ) ).'">';
}


$p3d_file_url_meta = get_post_meta(get_the_ID(), 'p3d_file_url'); $p3d_file_url = $p3d_file_url_meta[0];
$p3d_product_price_type_meta = get_post_meta(get_the_ID(), 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];

$p3d_product_pricing = get_post_meta(get_the_ID(), 'p3d_product_pricing', true);
if ($p3d_product_pricing!='') {
	$settings['pricing']=$p3d_product_pricing;
}

if (strlen($p3d_file_url)>0 && $p3d_product_price_type=='fixed') $settings['pricing']='checkout';

?>


<input type="hidden" id="base_price" value="<?php echo $price[0];?>">
<input type="hidden" id="p3d_product_id" value="<?php echo get_the_ID();?>">



<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
<form action="" style="margin-bottom:0px;" class="variations_form cart" method="post" enctype='multipart/form-data' data-product_id="<?php echo $post->ID; ?>" data-product_variations="<?php echo esc_attr( json_encode( $available_variations ) ) ?>">
	<input type="hidden" id="p3d-post-id" name="p3d_post_id" value="<?php echo $post->ID;?>">
	<input type="hidden" id="p3d-thumb" name="p3d_thumb" value="">
	<input type="hidden" id="p3d_estimated_price" name="p3d_estimated_price" value="">
	<input type="hidden" id="p3d-resize-scale" name="p3d_resize_scale" value="1">
	<input type="hidden" id="p3d-dim-x" name="p3d_dim_x" value="">
	<input type="hidden" id="p3d-dim-y" name="p3d_dim_y" value="">
	<input type="hidden" id="p3d-dim-z" name="p3d_dim_z" value="">

	<input type="hidden" id="p3d-rotation-x" name="p3d_rotation_x" value="">
	<input type="hidden" id="p3d-rotation-y" name="p3d_rotation_y" value="">
	<input type="hidden" id="p3d-rotation-z" name="p3d_rotation_z" value="">
	<input type="hidden" id="p3d-file-original" name="file_original" value="">

<?php 
	if ( !empty( $email_status_message ) ) {
		echo '<div class="p3d-info">'.$email_status_message.'</div>';
	}
?>


	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
	<div id="p3d-quote-loading" class="p3d-info">
		<img alt="Loading price" src="<?php echo esc_url($settings['ajax_loader']); ?>">
	</div>
	<div id="add-cart-wrapper">
		<div id="add-cart-container" style="visibility:hidden;">
			<?php do_action( 'woocommerce_before_single_variation' ); ?>

			<div class="single_variation"></div>
<?php
		if ( $settings['pricing']=='checkout' ) {
?>
			<div class="variations_button">
				<?php woocommerce_quantity_input(); ?>
				<button type="submit" class="single_add_to_cart_button button alt"><?php echo $product->single_add_to_cart_text(); ?></button>
			</div>
<?php
		}
?>


			<div <?php #if ( $settings['pricing']=='checkout' )  echo 'style="display:none;"'; ?> style="display:none;" id="p3d-request-form" class="variations_button p3d-info">
<?php
				$current_user = wp_get_current_user();

				if ($current_user->data->ID) {
					$current_user_email = $current_user->data->user_email;
				}
				else $current_user_email = '';
				if ($settings['use_ninjaforms']=='on') {
					if (strlen($settings['ninjaforms_shortcode'])==0) {
						_e('Please set the NinjaForms shortcode','3dprint');
					}
					else	
						echo do_shortcode($settings['ninjaforms_shortcode']);
				}
				else {
?>

                                <input type="hidden" value="request_price" name="action">
				<input class="price-request-field" type="text" value="<?php echo $current_user_email;?>" placeholder="<?php _e( 'Enter Your E-mail', '3dprint' );?>" name="email_address">
				<input class="price-request-field" type="text" value="" placeholder="<?php _e( 'Leave a comment', '3dprint' );?>" name="request_comment"><br>
				<button style="float:left;" type="submit" class="button alt"><?php _e( 'Request a Quote', '3dprint' ); ?></button>
<?php
				}
?>



			</div>


<?php
		if ( $settings['pricing']=='checkout' ) {
?>
			<input id="checkout-add-to-cart" type="hidden" name="add-to-cart" value="<?php echo $product->get_id(); ?>" />
<?php
		}
?>


			<input type="hidden" name="product_id" value="<?php echo esc_attr( $post->ID ); ?>" />
			<input type="hidden" name="variation_id" class="variation_id" value="<?php echo $available_variations[0]['variation_id']?>" />

			<?php do_action( 'woocommerce_after_single_variation' ); ?>
		</div>
	</div>
		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<?php if ( ! empty( $available_variations ) ) : ?>
		<table class="variations" cellspacing="0">
			<tbody>

				<?php $loop = 0; foreach ( $attributes as $name => $options ) : $loop++; ?>

					        <?php
				if ( strstr( $name, '_p3d_' ) ) {
?>
					<tr style="display:none;">
						<td><input id="<?php echo esc_attr( sanitize_title( $name ) ); ?>" name="attribute_<?php echo sanitize_title( $name ); ?>" type="hidden" autocomplete="off" data-attribute_name="attribute_<?php echo sanitize_title( $name ); ?>"></td>
						<?php

				}
				else { //normal attribute
?>
					<tr>
						<td class="label"><label for="<?php echo sanitize_title( $name ); ?>"><?php echo wc_attribute_label( $name ); ?></label></td>
						<td class="value">
						<?php
						$p3d_attr_prices=p3d_get_option('3dp_attr_prices');
						if (isset($p3d_attr_prices[sanitize_title($name)]) && !empty($p3d_attr_prices[sanitize_title($name)])) {
							$onchange='onchange="p3dGetStats();"';
							$woo_class='class="woo_attribute"';
						}
						else {
							$onchange='';
							$woo_class='';
						}
						?>
						<select id="<?php echo esc_attr( sanitize_title( $name ) ); ?>" name="attribute_<?php echo sanitize_title( $name ); ?>" data-attribute_name="attribute_<?php echo sanitize_title( $name ); ?>" <?php echo $woo_class;?> <?php echo $onchange;?> >
							<option value=""><?php echo __( 'Choose an option', 'woocommerce' ) ?>&hellip;</option>
							<?php
				if ( is_array( $options ) ) {

					if ( isset( $_REQUEST[ 'attribute_' . sanitize_title( $name ) ] ) ) {
						$selected_value = $_REQUEST[ 'attribute_' . sanitize_title( $name ) ];
					} elseif ( isset( $selected_attributes[ sanitize_title( $name ) ] ) ) {
						$selected_value = $selected_attributes[ sanitize_title( $name ) ];
					} else {
						$selected_value = '';
					}
						// Get terms if this is a taxonomy - ordered

					if ( taxonomy_exists( $name ) ) {

						$terms = wc_get_product_terms( $post->ID, $name, array( 'fields' => 'all' ) );
	
						foreach ( $terms as $term ) {

							if ( ! in_array( $term->slug, $options ) ) {
								continue;
							}
							$data_price=$data_price_type='';

							if (isset($p3d_attr_prices[sanitize_title($name)][$term->slug])) {
								$data_price='data-price="'.$p3d_attr_prices[sanitize_title($name)][$term->slug]['price'].'"';
								$data_price_type='data-price-type="'.$p3d_attr_prices[sanitize_title($name)][$term->slug]['price_type'].'"';

								if ($p3d_attr_prices[sanitize_title($name)][$term->slug]['price_type']=='pct') {
									$data_pct_type='data-pct-type="'.$p3d_attr_prices[sanitize_title($name)][$term->slug]['pct_type'].'"';
								}
								else {
									$data_pct_type='';
								}
							}

							echo '<option '.$data_price.' '.$data_price_type.' '.$data_pct_type.' value="' . esc_attr( $term->slug ) . '" ' . selected( sanitize_title( $selected_value ), sanitize_title( $term->slug ), false ) . '>' . apply_filters( 'woocommerce_variation_option_name', $term->name ) . '</option>';

						}
	
					} else {

						foreach ( $options as $option ) {

							echo '<option value="' . esc_attr( sanitize_title( $option ) ) . '" ' . selected( sanitize_title( $selected_value ), sanitize_title( $option ), false ) . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option ) ) . '</option>';
						}

					}
				}
?>
						</select>
<?php
	if ( sizeof( $attributes ) === $loop ) {
		echo '<a class="reset_variations" href="#reset">' . __( 'Clear selection', 'woocommerce' ) . '</a>';
	}
	?>
						</td>

						<?php
}
?>
					</tr>
		        <?php endforeach;?>

			</tbody>
		</table>


	<?php else : ?>

		<p class="stock out-of-stock"><?php _e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>

	<?php endif; ?>
<?php do_action( '3dprint_add_to_cart_form' ); ?>
</form>

<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>





<?php
$attributes['pa_p3d_printer']=array_map('p3d_process_attr', $attributes['pa_p3d_printer']);
$attributes['pa_p3d_material']=array_map('p3d_process_attr', $attributes['pa_p3d_material']);
$attributes['pa_p3d_coating']=array_map('p3d_process_attr', $attributes['pa_p3d_coating']);
$attributes['pa_p3d_infill']=array_map('p3d_process_attr', $attributes['pa_p3d_infill']);


$db_printers=p3d_get_option( '3dp_printers' );
$db_materials=p3d_get_option( '3dp_materials' );
$db_coatings=p3d_get_option( '3dp_coatings' );


$assigned_materials = p3d_get_assigned_materials($db_printers, $db_materials);

foreach ($db_materials as $key => $material) {
	if (!in_array($material['id'], $assigned_materials)) unset($db_materials[$key]);
}




if (count(p3d_get_enabled($db_printers))) $db_printers = p3d_sort_by_group_order(p3d_get_enabled($db_printers)); else $db_printers = array();
if (count(p3d_get_enabled($db_materials))) $db_materials = p3d_sort_by_group_order(p3d_get_enabled($db_materials)); else $db_materials = array();
if (count(p3d_get_enabled($db_coatings))) $db_coatings = p3d_sort_by_group_order(p3d_get_enabled($db_coatings)); else $db_coatings = array();


$db_printers_new = array();
if (!in_array( 'all', $attributes['pa_p3d_printer'] )) {
	for ( $i=0;$i<count( $db_printers );$i++ ) {
		if ( in_array( $db_printers[$i]['id'], $attributes['pa_p3d_printer'] ) ) {	
			$db_printers_new[]=$db_printers[$i];
		}
	}
	$db_printers=$db_printers_new;
}


$db_materials_new = array();
#print_r($attributes['pa_p3d_material']);
if (!in_array( 'all', $attributes['pa_p3d_material'] )) {
	for ( $i=0;$i<count( $db_materials );$i++ ) {
		if ( in_array( $db_materials[$i]['id'], $attributes['pa_p3d_material'] ) ) {	
			$db_materials_new[]=$db_materials[$i];
		}
	}
	$db_materials=$db_materials_new;
}
#print_r($db_materials);
$db_coatings_new = array();
if (!in_array( 'all', $attributes['pa_p3d_coating'] )) {
	for ( $i=0;$i<count( $db_coatings );$i++ ) {
		if ( in_array( $db_coatings[$i]['id'], $attributes['pa_p3d_coating'] ) ) {	
			$db_coatings_new[]=$db_coatings[$i];
		}
	}
	$db_coatings=$db_coatings_new;
}


$p3d_file_url_meta = get_post_meta($product->get_id(), 'p3d_file_url'); $p3d_file_url = $p3d_file_url_meta[0];
$p3d_product_price_type_meta = get_post_meta($product->get_id(), 'p3d_product_price_type'); $p3d_product_price_type = $p3d_product_price_type_meta[0];
$fixed_price = false;
if (strlen($p3d_file_url)>0 && $p3d_product_price_type=='fixed') $fixed_price = true;

//prepare photos and descriptions
echo '<div class="tooltip_templates">';
for ( $i=0;$i<count( $db_printers );$i++ ) {
	if (strlen($db_printers[$i]['description']) || strlen($db_printers[$i]['photo'])) {
		echo '<div class="p3d-tooltip-info" id="p3d-tooltip-printer-'.$db_printers[$i]['id'].'">';
		if (strlen($db_materials[$i]['description']) == 0) $image_class = 'p3d-tooltip-image-full'; else $image_class = '';
		if (strlen($db_printers[$i]['photo']))
			echo '<div class="p3d-tooltip-image '.$image_class.'"><img src="'.esc_url($db_printers[$i]['photo']).'"></div>';
		if (strlen($db_printers[$i]['description']))
			echo '<div class="p3d-tooltip-description">'.esc_html(stripslashes($db_printers[$i]['description'])).'</div>';
		echo '</div>';
	}
}

for ( $i=0;$i<count( $db_materials );$i++ ) {
	if (strlen($db_materials[$i]['description']) || strlen($db_materials[$i]['photo'])) {
		echo '<div class="p3d-tooltip-info" id="p3d-tooltip-material-'.$db_materials[$i]['id'].'">';
		if (strlen($db_materials[$i]['description']) == 0) $image_class = 'p3d-tooltip-image-full'; else $image_class = '';
		if (strlen($db_materials[$i]['photo']))
			echo '<div class="p3d-tooltip-image '.$image_class.'"><img src="'.esc_url($db_materials[$i]['photo']).'"></div>';
		if (strlen($db_materials[$i]['description']))
			echo '<div class="p3d-tooltip-description">'.esc_html(stripslashes($db_materials[$i]['description'])).'</div>';
		echo '</div>';
	}
}

for ( $i=0;$i<count( $db_coatings );$i++ ) {
	if (strlen($db_coatings[$i]['description']) || strlen($db_coatings[$i]['photo'])) {
		echo '<div class="p3d-tooltip-info" id="p3d-tooltip-coating-'.$db_coatings[$i]['id'].'">';
		if (strlen($db_materials[$i]['description']) == 0) $image_class = 'p3d-tooltip-image-full'; else $image_class = '';
		if (strlen($db_coatings[$i]['photo']))
			echo '<div class="p3d-tooltip-image '.$image_class.'"><img src="'.esc_url($db_coatings[$i]['photo']).'"></div>';
		if (strlen($db_coatings[$i]['description']))
			echo '<div class="p3d-tooltip-description">'.esc_html(stripslashes($db_coatings[$i]['description'])).'</div>';
		echo '</div>';
	}
}
echo '</div>';


//choose the layout
switch ($settings['materials_layout']) {
	case 'lists':
		include('template_material_list.php');
	break;
	case 'dropdowns':
		include('template_material_dropdown.php');
	break;
	case 'colors':
		include('template_material_colors.php');
	break;
	case 'slider':
		include('template_material_slider.php');
	break;
	case 'group_slider':
		include('template_material_group_slider.php');
	break;
	default:
		include('template_material_dropdown.php');
	break;
}

switch ($settings['coatings_layout']) {

	case 'lists':
		include('template_coating_list.php');
	break;
	case 'dropdowns':
		include('template_coating_dropdown.php');
	break;
	case 'colors':
		include('template_coating_colors.php');
	break;
	case 'slider':
		include('template_coating_slider.php');
	break;
	case 'group_slider':
		include('template_coating_group_slider.php');
	break;
	default:
		include('template_coating_dropdown.php');
	break;
}

switch ($settings['printers_layout']) {
	case 'lists':
		include('template_printer_list.php');
	break;
	case 'dropdowns':
		include('template_printer_dropdown.php');
	break;
	case 'slider':
		include('template_printer_slider.php');
	break;
	case 'group_slider':
		include('template_printer_group_slider.php');
	break;
	default:
		include('template_printer_dropdown.php');
	break;
}

switch ($settings['infills_layout']) {
	case 'lists':
		include('template_infill_list.php');
	break;
	case 'dropdowns':
		include('template_infill_dropdown.php');
	break;
	default:
		include('template_infill_dropdown.php');
	break;
}
?>