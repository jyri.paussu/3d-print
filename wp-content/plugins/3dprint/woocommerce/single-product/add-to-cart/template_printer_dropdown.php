<nav <?php if ($settings['show_printers']!='on') echo 'style="display:none;"';?> class="applePie p3d-info">
	<div style="display:none;" class="menubtn"><?php _e( 'Printer', '3dprint' );?></div>
	<ul class="nav">
		<li class="p3d-dropdown-li"><a id="p3d-printer-name" href="javascript:void(0)"><?php _e( 'Printer', '3dprint' );?> : <?php echo $db_printers[0]['name'];?></a>
			<ul>
	        
<?php
		$group_name=''; 
		for ( $i=0;$i<count( $db_printers );$i++ ) {
			if ( !is_array($attributes['pa_p3d_printer']) ) continue;
			if ( is_array($attributes['pa_p3d_printer']) && in_array( $db_printers[$i]['id'], $attributes['pa_p3d_printer'] ) || in_array( 'all', $attributes['pa_p3d_printer'] ) ) {
				if (!empty($db_printers[$i]['group_name']) && $group_name!=$db_printers[$i]['group_name']) {
					echo '<li><a href="javascript:void(0);">'.__($db_printers[$i]['group_name'],'3dprint').'</a><ul>';
					$group_name = $db_printers[$i]['group_name'];
				}
				echo '<li class="p3d-tooltip" data-tooltip-content="#p3d-tooltip-printer-'.$db_printers[$i]['id'].'" onclick="p3dSelectPrinter(this);" data-name="'.esc_attr( $db_printers[$i]['name'] ).'"><input style="display:none;" id="p3d_printer_'.$db_printers[$i]['id'].'" class="p3d-control" autocomplete="off" data-name="'.esc_attr( $db_printers[$i]['name'] ).'" data-type="'.esc_attr( $db_printers[$i]['type'] ).'" data-full_color="'.esc_attr( $db_printers[$i]['full_color'] ).'" data-platform_shape="'.esc_attr( $db_printers[$i]['platform_shape'] ).'" data-diameter="'.$db_printers[$i]['diameter'].'" data-width="'.$db_printers[$i]['width'].'" data-length="'.$db_printers[$i]['length'].'" data-height="'.$db_printers[$i]['height'].'" data-min_side="'.$db_printers[$i]['min_side'].'" data-id="'.$db_printers[$i]['id'].'" data-layer-height="'.$db_printers[$i]['layer_height'].'" data-time-per-layer="'.$db_printers[$i]['time_per_layer'].'" data-wall-thickness="'.$db_printers[$i]['wall_thickness'].'" data-nozzle-size="'.$db_printers[$i]['nozzle_size'].'" data-infills="'.($db_printers[$i]['type']=='fff' ? $db_printers[$i]['infills'] : '').'" data-default-infill="'.($db_printers[$i]['type']=='fff' ? $db_printers[$i]['default_infill'] : '').'" data-materials="'.$db_printers[$i]['materials'].'" data-price="'.esc_attr($db_printers[$i]['price']).'" data-price_type="'.$db_printers[$i]['price_type'].'" data-price_type1="'.$db_printers[$i]['price_type1'].'" data-price1="'.esc_attr($db_printers[$i]['price1']).'" data-price2="'.esc_attr($db_printers[$i]['price2']).'" data-price_type2="'.$db_printers[$i]['price_type2'].'" data-price3="'.esc_attr($db_printers[$i]['price3']).'" data-price_type3="'.$db_printers[$i]['price_type3'].'" data-price4="'.esc_attr($db_printers[$i]['price4']).'" data-price_type4="'.$db_printers[$i]['price_type4'].'" data-speed="'.esc_attr($db_printers[$i]['speed']).'" data-speed_type="'.$db_printers[$i]['speed_type'].'" data-travel_speed="'.esc_attr($db_printers[$i]['travel_speed']).'" data-support="'.$db_printers[$i]['support'].'" data-support_type="'.$db_printers[$i]['support_type'].'" data-support_angle="'.$db_printers[$i]['support_angle'].'" data-printer_energy_hourly_cost="'.$db_printers[$i]['printer_energy_hourly_cost'].'" data-printer_depreciation_hourly_cost="'.$db_printers[$i]['printer_depreciation_hourly_cost'].'" data-printer_repair_hourly_cost="'.$db_printers[$i]['printer_repair_hourly_cost'].'" type="radio" name="product_printer"><a class="p3d-dropdown-item" href="javascript:void(0)">'.__($db_printers[$i]['name'], '3dprint').'</a></li>';
				if (!empty($db_printers[$i]['group_name']) && (isset($db_printers[$i+1]) && $db_printers[$i+1]['group_name']!=$group_name) || $i==(count($db_printers)-1)) {
					echo '</ul></li>';
				}
			}
		}
?>
			</ul>
		</li>
	</ul>

</nav>
