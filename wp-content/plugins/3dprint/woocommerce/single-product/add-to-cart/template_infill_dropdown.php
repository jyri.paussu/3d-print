<nav id="infill-info" <?php if ($settings['show_infills']!='on' || $fixed_price) echo 'style="display:none;"';?> class="applePie p3d-info">
	<div style="display:none;" class="menubtn"><?php _e( 'Infill', '3dprint' );?></div>
	<ul class="nav">
		<li class="p3d-dropdown-li"><a id="p3d-infill-name" href="javascript:void(0)"><?php _e( 'Infill', '3dprint' );?></a>
			<ul>
<?php
			for ( $i=0;$i<=10;$i++ ) {
				if ( in_array( $i*10, $attributes['pa_p3d_infill'] ) || in_array( 'all', $attributes['pa_p3d_infill'] ) ) {
					echo '<li data-name="'.($i*10).'%" onclick="p3dSelectInfill(this);"><input style="display:none;" id="p3d_infill_'.($i*10).'" data-name="'.($i*10).'%" class="p3d-control p3d-infill-dropdown" autocomplete="off" type="radio" data-id="'. ($i*10).'" name="product_infill"><a class="p3d-dropdown-item" href="javascript:void(0)">'.($i*10).'%</a></li>';
				}
			}
?>
			</ul>
		</li>
	</ul>
</nav>


