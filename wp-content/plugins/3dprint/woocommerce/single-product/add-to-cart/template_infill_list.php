<div id="infill-info" <?php if ($settings['show_infills']!='on' || $fixed_price) echo 'style="display:none;"';?> class="p3d-info">
	<fieldset id="infill_fieldset" class="p3d-fieldset">
		<legend id="p3d-infill-name"><?php _e( 'Infill', '3dprint' );?></legend>
		<ul class="p3d-list">
<?php
			for ( $i=0;$i<=10;$i++ ) {
				if ( in_array( $i*10, $attributes['pa_p3d_infill'] ) || in_array( 'all', $attributes['pa_p3d_infill'] ) ) {
					echo '<li data-name="'.($i*10).'%" onclick="p3dSelectInfill(this);"><input id="p3d_infill_'.($i*10).'" data-name="'.($i*10).'%" class="p3d-control" autocomplete="off" type="radio" data-id="'. ($i*10).'" name="product_infill"><a class="p3d-dropdown-item" href="javascript:void(0)">'.($i*10).'%</a></li>';
				}
			}
?>
		</ul>
	</fieldset>
</div>