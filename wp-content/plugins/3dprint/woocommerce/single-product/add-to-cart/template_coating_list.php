<?php

if (is_array($attributes['pa_p3d_coating']) && is_array($db_coatings) && count($db_coatings)>0) {
?>
	<div <?php if ($settings['show_coatings']!='on') echo 'style="display:none;"';?> class="p3d-info">
		<fieldset id="coating_fieldset" class="p3d-fieldset">
			<legend id="p3d-coating-name"><?php _e( 'Coating', '3dprint' );?></legend>
			<ul class="p3d-list">
	<?php
			$group_name=''; 
			for ( $i=0;$i<count( $db_coatings );$i++ ) {
				if ( !is_array($attributes['pa_p3d_coating']) ) continue;
				if ( in_array( $db_coatings[$i]['id'], $attributes['pa_p3d_coating'] ) || in_array( 'all', $attributes['pa_p3d_coating'] ) ) {
					if (!empty($db_coatings[$i]['group_name']) && $group_name!=$db_coatings[$i]['group_name']) {
						echo '<li style="background:none;"><a href="javascript:void(0);">'.__($db_coatings[$i]['group_name'],'3dprint').'</a><ul>';
						$group_name = $db_coatings[$i]['group_name'];
					}

					echo '<li class="p3d-tooltip" data-tooltip-content="#p3d-tooltip-coating-'.$db_coatings[$i]['id'].'" data-color=\''.$db_coatings[$i]['color'].'\' data-shininess=\''.$db_coatings[$i]['shininess'].'\' data-glow=\''.$db_coatings[$i]['glow'].'\'  data-transparency=\''.$db_coatings[$i]['transparency'].'\' data-name="'.esc_attr( $db_coatings[$i]['name'] ).'" onclick="p3dSelectCoating(this);"><input id="p3d_coating_'.$db_coatings[$i]['id'].'" class="p3d-control" autocomplete="off" type="radio" data-id="'.$db_coatings[$i]['id'].'" data-materials="'.$db_coatings[$i]['materials'].'" data-color=\''.$db_coatings[$i]['color'].'\' data-name="'.esc_attr( $db_coatings[$i]['name'] ).'" data-price="'.esc_attr($db_coatings[$i]['price']).'" data-price_type="'.$db_coatings[$i]['price_type'].'" data-price1="'.esc_attr($db_coatings[$i]['price1']).'" data-price_type1="'.$db_coatings[$i]['price_type1'].'" name="product_coating" ><div style="background-color:'.$db_coatings[$i]['color'].'" class="color-sample"></div>'.__($db_coatings[$i]['name'], '3dprint').'</li>';
					if (!empty($db_coatings[$i]['group_name']) && (isset($db_coatings[$i+1]) && $db_coatings[$i+1]['group_name']!=$group_name) || $i==(count($db_coatings)-1)) {
						echo '</ul></li>';
					}
				}
			}
	?>
			</ul>
		</fieldset>
	</div>
<?php
}
?>
