<?php
/**
 * Order again button
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-again.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$p3d_items=0;
$nonp3d_items=0;
$order_items = $order->get_items();
foreach ($order_items as $order_item) {
	if (p3d_is_p3d($order_item['product_id'])) {
		$p3d_items++;
		if ( method_exists( $product, 'get_product' ) ) {
			$product = $order_item->get_product();
			$permalink = $product->get_permalink( $order_item );
		}
		else {
			$permalink = get_permalink( $order_item['product_id'] );
		}
	}
	else $nonp3d_items++;
}
?>
<?php
if ($p3d_items==0) {
?>
<p class="order-again">
	<a href="<?php echo esc_url( wp_nonce_url( add_query_arg( 'order_again', $order->get_id() ) , 'woocommerce-order_again' ) ); ?>" class="button"><?php _e( 'Order again', 'woocommerce' ); ?></a>
</p>
<?php
}
else if ($p3d_items==1 && $nonp3d_items==0) {
?>
<p class="order-again">
	<a href="<?php echo esc_url( $permalink ); ?>" class="button"><?php _e( 'Order again', 'woocommerce' ); ?></a>
</p>
<?php
}
else {
?>
<p>
	<?php _e('If you wish to reorder please add products to the cart one by one.', '3dprint');?>
</p>
<?php
}
?>