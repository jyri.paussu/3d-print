<?php
//if uninstall not called from WordPress exit
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) 
    exit();

function p3d_rmdir($dir) {
	if (is_dir($dir)) {
		$objects = scandir($dir);
		foreach ($objects as $object) {
			if ($object != "." && $object != "..") {
				if (filetype($dir."/".$object) == "dir") p3d_rmdir($dir."/".$object); else unlink($dir."/".$object);
			}
		}
		reset($objects);
		rmdir($dir);
	}
} 

$upload_dir = wp_upload_dir();
p3d_rmdir($upload_dir['basedir'].'/p3d/');

delete_option( '3dp_settings' );
delete_option( '3dp_printers' );
delete_option( '3dp_materials' );
delete_option( '3dp_cache' );
delete_option( '3dp_triangulation_cache' );
delete_option( '3dp_attr_prices' );
delete_option( '3dp_servers' );
delete_option( '3dp_price_requests' );
delete_option( '3dp_version' );



global $wpdb;
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}p3d_printers" );
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}p3d_materials" );
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}p3d_coatings" );

$wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_printer' ) );
$wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_material' ) );
$wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_coating' ) );
$wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_model' ) );
$wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_unit' ) );
$wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name = '%s'", 'p3d_infill' ) );



?>