<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define( 'WP_MEMORY_LIMIT', '1024M' );

//define('WP_DEBUG', true);
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'b$*wTH]t2Xo1Q@~7pXBsx#!EKaY2;<Ip)CV9n,),.Le|k;h=Mp)*5*:#q8`52N&|');
define('SECURE_AUTH_KEY',  '6!U)O)X+Df#w/0(ITW!`@5PRl[|S9]l8enx[6OKeU3Q9~nK1x#YmQCF^oZe,sF_;');
define('LOGGED_IN_KEY',    '7q9@&y$r#~d8$!bCUBBe~Y.k)5`,r<pl;i29,7=xPG+Xqh*4}h4}Y79Gb;6vnV&%');
define('NONCE_KEY',        '4l|P*B+`Ih]_TbFUlyEC%3W&To}xIT-+j4iou<+HZC<%5uZunFIKL5:v8?|yPxY>');
define('AUTH_SALT',        'seu~-H>c![z5(N$|=TCqkZCiZ+lcG&82Bw;[]))*#j[qa3imHaA$s+9<Luy`/)9G');
define('SECURE_AUTH_SALT', '#e{~:6SL.9<~S@BGxDxxY<7Q:#iT?rhL+m=t5.zO7<L}J4X^FDgyF<`ayRd>{?RT');
define('LOGGED_IN_SALT',   '2U9.o%O9)5_(:5iGcO[OVN1}*}eqI<4XBUGFo![`z*|%jd%h``FVM%,prv-RYTY~');
define('NONCE_SALT',       '**2}a[D_n.}XagmI v%d:zflNGFs)1av{!zjlIW;_()zG&1ynGc6F rGh!Le5gj^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
